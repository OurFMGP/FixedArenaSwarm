using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour
{
    [Header("Gold")]
    [SerializeField]
    private bool gold;

    [SerializeField]
    private int goldValue;

    [Header("Bomb")]
    [SerializeField]
    private bool bomb;

    [SerializeField]
    private float bombDamage;

    [SerializeField]
    private float bossBombDamagePercent;

    [Header("Magnet")]
    [SerializeField]
    private bool magnet;

    //Allows the player to collect gold depending on its value
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            if (gold)
            {
                PlayerStats.Instance.gold += goldValue * PlayerStats.Instance.currentGoldMultiplier;

                Destroy(gameObject);
            }
        }
    }
}
