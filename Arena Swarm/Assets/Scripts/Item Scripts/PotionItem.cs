using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PotionItem : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            PlayerUIManager playerUIManager = FindObjectOfType<PlayerUIManager>();
            if (playerUIManager != null)
            {
                // Handle the logic for the player picking up the health potion.
                Destroy(gameObject); // Destroy the health potion item.
                playerUIManager.EnableHealButton(); // Call the method to enable the UI button.
            }
        }
    }
}