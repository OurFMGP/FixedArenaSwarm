using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ItemSpawner : MonoBehaviour
{
    public GameObject[] itemPrefabs;
    public int maxItemsToSpawn = 10;

    public float minX; // Variable for minimum x position
    public float maxX; // Variable for maximum x position
    public float minY; // Variable for minimum y position
    public float maxY; // Variable for maximum y position

    private List<GameObject> spawnedItems = new List<GameObject>(); // Keep track of spawned items

    public Timer timer; // Reference to the Timer script
    private float lastSpawnTime = 0; // The time when items were last spawned
    private float spawnInterval = 2 * 60; // Interval to spawn items (2 minutes)

    void Start()
    {
        SpawnItems(maxItemsToSpawn);
    }

    void Update()
    {
        if (timer.timer - lastSpawnTime >= spawnInterval)
        {
            lastSpawnTime = timer.timer; // Update the last spawn time

            int currentItemCount = spawnedItems.RemoveAll(item => item == null); // Remove null references and get the current item count

            if (currentItemCount < maxItemsToSpawn)
            {
                int itemsToSpawn = Random.Range(1, maxItemsToSpawn - currentItemCount + 1); // Calculate random number of items to spawn
                SpawnItems(itemsToSpawn);
            }
        }
    }

    void SpawnItems(int count)
    {
        for (int i = 0; i < count; i++)
        {
            // Randomly select an item prefab
            GameObject itemPrefab = itemPrefabs[Random.Range(0, itemPrefabs.Length)];

            // Calculate a random spawn position
            Vector3 randomSpawnPosition = GetRandomSpawnPosition();

            // Instantiate the item at the random position
            GameObject item = Instantiate(itemPrefab, randomSpawnPosition, Quaternion.identity);

            // Add the spawned item to the list
            spawnedItems.Add(item);
        }
    }

    Vector3 GetRandomSpawnPosition()
    {
        float x = Random.Range(minX, maxX);
        float y = Random.Range(minY, maxY);

        return new Vector3(x, y, 0); // Assuming you want to spawn the items at z = 0
    }
}