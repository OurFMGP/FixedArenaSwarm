using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sniper : BulletManager
{
	[Header("Level 3")]
	public bool canCrit;

	public float critDamageMulti;

	[SerializeField]
	private float originalCritDamageMulti;

	public float critChance;

	[SerializeField]
	private float originalCritChance;

	//Similer to the original except if we can crit we run a randomiser to see if we can deal additional damage
	protected override void TriggerEnter2D(Collider2D collider)
	{
		if (collider.CompareTag("BulletCollider"))
        {
			pierce -= 1;

			float newDamage = damage;

			if (canCrit)
			{
				float randomNum = Random.value;

				if (randomNum <= critChance)
				{
					newDamage = damage * (1f + critDamageMulti);
				}
			}

			collider.gameObject.SendMessageUpwards("TakeGunDamage", newDamage * PlayerStats.Instance.currentDamage);

			if (pierce <= 0)
			{
				Destroy(gameObject);
			}
		}
	}

	//resets all crit based things and calles to reset values in bulletManager
	public override void ResetValues()
    {
		canCrit = false;

		critDamageMulti = originalCritDamageMulti;

		critChance = originalCritChance;

        base.ResetValues();
    }
}
