using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Retical : MonoBehaviour
{
    [Header("References")]
    public List<GameObject> currentGuns = new List<GameObject>();

    //Allows us to get the mouse position based on where it is on the screen in which we aim our gun towards it
    void Update()
    {
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        transform.position = mousePos;

        foreach (GameObject gun in currentGuns)
        {
            Vector2 direction = (mousePos - (Vector2)gun.transform.position).normalized;

            gun.transform.right = -direction;
        }      
    }
}
