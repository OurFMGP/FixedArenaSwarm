using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BulletManager : MonoBehaviour
{
	[Header("Fields")]
	public float damage;

	[SerializeField]
	protected float originalDamage;

	public float speed;

	[SerializeField]
	protected float originalSpeed;

	public int pierce;

	[SerializeField]
	protected int originalPirece;

	public Vector2 size;

	public float lifeTimeWait;

	[SerializeField]
	protected float originalTimeWait;

	[SerializeField]
	protected float lifeTimeCounter;

	private void Start()
	{
		transform.localScale = size;
	}

	private void Update()
	{
		if (!PlayerStats.Instance.paused)
        {
			LifeCounter();
		}
	}

	//Keeps track of how long the bullet has been alive for and also applys speed to the rigidbodys velocity
	protected virtual void LifeCounter()
    {
		lifeTimeCounter += Time.deltaTime;

		if (lifeTimeCounter >= lifeTimeWait)
		{
			Destroy(gameObject);
		}

		GetComponent<Rigidbody2D>().velocity = -transform.right * speed;
	}

	//Pn trigger enter if its not the player we call a method
	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.gameObject.tag != "Player")
		{
			TriggerEnter2D(collision);
		}
	}

	//If the collider is what we are comparing we apply damage to that enemy and if the pierce is 0 or less we destroy the bullet
	protected virtual void TriggerEnter2D(Collider2D collider)
    {
		if (collider.CompareTag("BulletCollider"))
        {
			pierce -= 1;

			collider.gameObject.SendMessageUpwards("TakeGunDamage", damage * PlayerStats.Instance.currentDamage);

			if (pierce <= 0)
			{
				Destroy(gameObject);
			}
		}
	}

	//We rest our values to the stored ones when called
	public virtual void ResetValues()
	{
		damage = originalDamage;

		speed = originalSpeed;

		pierce = originalPirece;

		lifeTimeWait = originalTimeWait;
	}
}
