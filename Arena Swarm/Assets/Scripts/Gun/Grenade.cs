using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//We inherit from bulletManager
public class Grenade : BulletManager
{
    [SerializeField]
    private GameObject explosive;

    //Similer to the original except we call another method when life counter is 0
    protected override void LifeCounter()
    {
        lifeTimeCounter += Time.deltaTime;

        if (lifeTimeCounter >= lifeTimeWait)
        {
            ExplosiveSpawn();

            Destroy(gameObject);
        }

        GetComponent<Rigidbody2D>().velocity = -transform.right * speed;
    }

    //Similer to the original except we call another method when pierce is 0
    protected override void TriggerEnter2D(Collider2D collider)
    {
        pierce -= 1;

        if (collider.CompareTag("BulletCollider"))
        {
            collider.gameObject.SendMessageUpwards("TakeGunDamage", damage * PlayerStats.Instance.currentDamage);

            if (pierce <= 0)
            {
                ExplosiveSpawn();

                Destroy(gameObject);
            }
        }
    }

    //Spawns an explsive clone and call the method BOOM
    private void ExplosiveSpawn()
    {
        GameObject newExplosive = Instantiate(explosive, transform.position, Quaternion.LookRotation(transform.forward));

        newExplosive.GetComponent<Explosion>().BOOM();
    }
}
