using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//We inherit from bulletManager
public class Shotgun : BulletManager
{
	[Header("Awaken")]
	public bool splitShot;

	[SerializeField]
	private float splitDamageReduce;

	private bool hitTarget;

	[SerializeField]
	private GameObject rightSplit;

	[SerializeField]
	private GameObject leftSplit;

	//Similer to the original except we call another method when life counter is 0 while we have splitShot and hit target is false
	protected override void LifeCounter()
    {
		lifeTimeCounter += Time.deltaTime;

		if (lifeTimeCounter >= lifeTimeWait)
		{
			if (splitShot && hitTarget == false)
			{
				ShotgunSplit();
			}

			Destroy(gameObject);
		}

		GetComponent<Rigidbody2D>().velocity = -transform.right * speed;
	}

	//Similer to the original except we call another method when pierce is 0 and while we have split shot
	protected override void TriggerEnter2D(Collider2D collider)
    {
		if (collider.CompareTag("BulletCollider"))
        {
			pierce -= 1;

			collider.gameObject.SendMessageUpwards("TakeGunDamage", damage * PlayerStats.Instance.currentDamage);

			hitTarget = true;

			if (pierce <= 0)
			{
				if (splitShot)
				{
					ShotgunSplit();
				}

				Destroy(gameObject);
			}
		}
	}

	//This allows us to split our bullet only once by spawning a clone of this bullet in two differnt locations
    protected void ShotgunSplit()
	{
		splitShot = false;

		lifeTimeCounter = 0;

		pierce = 1;

		damage = damage * (1f - splitDamageReduce);

		Instantiate(gameObject, rightSplit.transform.position, rightSplit.transform.rotation);

		Instantiate(gameObject, leftSplit.transform.position, leftSplit.transform.rotation);
	}

	//resets the splitshot variable in the shotgun bullet then we call reset values within bulletmanager
	public override void ResetValues()
    {
		splitShot = false;

        base.ResetValues();
    }
}
