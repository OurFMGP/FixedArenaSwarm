using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//We inherit from bulletManager
public class DeagleMagic : BulletManager
{
    [Header("Awaken")]
    [SerializeField]
    private float damageReduce;

    [SerializeField]
    private int pierceReduce;

    [SerializeField]
    private float sizeReduce;

    [SerializeField]
    private float lifeTimeIncreace;

    //We set reduce our current values when we are activated
    public void SetAwakenDeagle()
    {
        damage = damage * (1f - damageReduce);

        pierce -= pierceReduce;

        size = size * (1f - sizeReduce);

        lifeTimeWait = lifeTimeWait * (1f + lifeTimeIncreace);
    }
}
