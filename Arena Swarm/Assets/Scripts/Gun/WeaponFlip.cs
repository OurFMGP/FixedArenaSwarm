using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponFlip : MonoBehaviour
{
    [Header("Reference")]
    [SerializeField]
    private GameObject flippedSprite;

    [SerializeField]
    private SpriteRenderer spriteRenderer;

    [SerializeField]
    private GameObject barrels;

    private Vector3 barrelOriginal;

    [Header("Fields")]
    [SerializeField]
    private float zTrueRotation;

    [SerializeField]
    private float barrelYIncease;

    private bool doOnce;

    private void Start()
    {
        barrelOriginal = barrels.transform.localPosition;
    }

    //We want to keep the weapon upright the whole time as otherwise it looks odd. When we want to flip based on when the gun will be upside down we turn off the current renderer,
    //then we activate the now right side up gameobject with a differnt renderer. Since the bullets will be offset to the gun we adjust the invisable barrel in line with the gun barrel.
    void Update()
    {
        zTrueRotation = transform.rotation.eulerAngles.z;

        if (zTrueRotation > 98 && zTrueRotation < 275)
        {
            if (!doOnce)
            {
                spriteRenderer.enabled = false;

                flippedSprite.SetActive(true);

                barrels.transform.localPosition = barrelOriginal;

                barrels.transform.localPosition = new Vector3(barrels.transform.localPosition.x, barrels.transform.localPosition.y + barrelYIncease, barrels.transform.localPosition.z);

                doOnce = true;
            }
        }
        else
        {
            if (doOnce)
            {
                spriteRenderer.enabled = true;

                flippedSprite.SetActive(false);

                barrels.transform.localPosition = barrelOriginal;

                doOnce = false;
            }
        }
    }
}
