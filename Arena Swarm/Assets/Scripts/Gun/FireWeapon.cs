using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class FireWeapon : MonoBehaviour
{
    [Header("Ammo")]
    public GameObject currentBullet;

    public List<GameObject> spawnSpot = new List<GameObject>();

    [Header("Fields")]
    [SerializeField]
    private float fireRate;

    public float actualFireRate;

    [SerializeField]
    private float fireCounter;

    [Serializable]
    public struct SniperAwaken
    {
        public static bool sniperAwaken;

        public bool tempFireStop;

        public int burstAmmount;

        [HideInInspector]
        public int increment;

        public float timeBetweenShots;
    }

    [SerializeField]
    private SniperAwaken sniperAwaken;

    [Serializable]
    public struct DeagleAwaken
    {
        public static bool doNotStart;

        public float fireRateReduce;
    }

    [SerializeField]
    private DeagleAwaken deagleAwaken;

    [Serializable]
    public struct M4Awaken
    {
        public static bool m4Awaken;

        public GameObject grenade;

        public float grenadeFireRate;

        public float grenadeActualFireRate;

        public float grenadeFireCounter;
    }

    public M4Awaken m4Awaken;

    //if the if statement is false we set our weapon firerate and our grenade firerate
    private void Start()
    {
        if (!DeagleAwaken.doNotStart)
        {
            if (fireRate != 0)
            {
                actualFireRate = 1.0f / fireRate;

                m4Awaken.grenadeActualFireRate = 1.0f / m4Awaken.grenadeFireRate;
            }
            else
            {
                actualFireRate = 0.01f;

                m4Awaken.grenadeActualFireRate = 0.01f;
            }           
        }
    }

    //if we are not paused and not sniper temp fire stop we increase our fire counter, when our firecounter is more an the actual fire rate we fire (or sniper fire if available)
    //If m4 awaken is active we also increase the grenade fire counter in which we fire that if its more than the actual grenade fire rate
    void Update()
    {
        if (!PlayerStats.Instance.paused)
        {
            if (!sniperAwaken.tempFireStop)
            {
                if (fireCounter < actualFireRate)
                {
                    fireCounter += Time.deltaTime;
                }

                if (fireCounter >= actualFireRate * PlayerStats.Instance.currentFireRate)
                {
                    if (SniperAwaken.sniperAwaken)
                    {
                        sniperAwaken.tempFireStop = true;

                        AwakenSniperFire();
                    }
                    else
                    {
                        Fire();
                    }
                }
            }

            if (M4Awaken.m4Awaken == true)
            {
                if (m4Awaken.grenadeFireCounter < m4Awaken.grenadeActualFireRate)
                {
                    m4Awaken.grenadeFireCounter += Time.deltaTime;
                }

                if (m4Awaken.grenadeFireCounter >= m4Awaken.grenadeActualFireRate)
                {
                    M4GrenadeFire();
                }
            }
        }      
    }

    //We clone the bullet based on how many barrels we have
    private void Fire()
    {
        fireCounter = 0;

        foreach (GameObject barrels in spawnSpot)
        {
            Instantiate(currentBullet, barrels.transform.position, gameObject.transform.rotation);
        }
    }

    //We clone the grenade based on how many barrels we have
    private void M4GrenadeFire()
    {
        m4Awaken.grenadeFireCounter = 0;

        foreach (GameObject barrels in spawnSpot)
        {
            Instantiate(m4Awaken.grenade, barrels.transform.position, gameObject.transform.rotation);
        }
    }

    private void AwakenSniperFire()
    {
        StartCoroutine(SniperAwakenBurst());
    }

    //we fire the snipers awaken bullet using a while loop with an increment and burst ammount, this allows us to fire bullets in a quick succession then stop. This is to simulate a 
    //burst fire weapon. after this pause we reset the values to do it again.
    private IEnumerator SniperAwakenBurst()
    {
        while (true)
        {
            if (sniperAwaken.increment < sniperAwaken.burstAmmount)
            {
                foreach (GameObject barrels in spawnSpot)
                {
                    Instantiate(currentBullet, barrels.transform.position, gameObject.transform.rotation);
                }

                sniperAwaken.increment++;

                yield return new WaitForSeconds(sniperAwaken.timeBetweenShots);
            }
            else
            {
                break;
            }
        }

        sniperAwaken.increment = 0;

        fireCounter = 0;

        sniperAwaken.tempFireStop = false;
    }

    //This is to make the deagle fire initially at the same time as the other deagles when they are active
    public void DeagleReset()
    {
        fireCounter = actualFireRate;
    }

    //Since most stats are tied with the bullet itself we only need to make the fire rate equal to all other active deagles at this time being
    public  void DeagleMagicAkimbo(float copiedFireRate)
    {
        actualFireRate = copiedFireRate;

        actualFireRate = actualFireRate * (1f + deagleAwaken.fireRateReduce);
    }
}
