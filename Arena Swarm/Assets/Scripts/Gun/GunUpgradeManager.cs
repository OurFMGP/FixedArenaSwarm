using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GunUpgradeManager : MonoBehaviour
{
    [Header("References")]
    [SerializeField]
    protected FireWeapon fireWeapon;

    [SerializeField]
    protected List<FireWeapon> deagleFireWeapon = new List<FireWeapon>();

    [SerializeField]
    protected BulletManager bulletManager;

    [SerializeField]
    protected Shotgun shotgun;

    [SerializeField]
    protected Sniper sniper;

    [SerializeField]
    protected Deagle deagle;

    [SerializeField]
    protected DeagleMagic deagleMagic;

    [SerializeField]
    protected List<GameObject> deagleAkimbos = new List<GameObject>();

    [SerializeField]
    protected M4 m4;

    [Header("Fields")]
    public string currentGun;

    public static bool ablityBeingUsed;

    //We set our current gun, fire weapon and bullet manager to the passed over variables
    public void SetWeapon(GameObject modifyGun, GameObject modifyAmmo)
    {
        currentGun = modifyGun.name;

        fireWeapon = modifyGun.GetComponentInChildren<FireWeapon>();

        bulletManager = modifyAmmo.GetComponent<BulletManager>();
    }

    //We set our weapons like the one above but in this case we use array and lists due to the ammount of deagles you can get
    public void SetDeagle(List<GameObject> modifyGun, GameObject modifyAmmo)
    {
        currentGun = modifyGun[0].name;

        foreach (GameObject fireWeapon in modifyGun)
        {
            deagleFireWeapon.Add(fireWeapon.GetComponentInChildren<FireWeapon>());
        }

        bulletManager = modifyAmmo.GetComponent<BulletManager>();
    }
}
