using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GunAbility : GunUpgradeManager
{
    public List<GameObject> weaponBars;
    public Image weaponBar;
    public Image circularWeaponBar;
    public float circularPercentange;
    private const float circularFillAmount = 0.75f;
    
    [Header("GunAbility References")]
    [SerializeField]
    private GunUpgrades gunUpgrades;

    [Header("AblityGuage")]
    [SerializeField]
    private float maxAbilityGauge;

    [SerializeField]
    private bool startWithAbility;

    [SerializeField]
    private float abilityTimeLeft;

    [Header("Shotgun")]
    [SerializeField]
    private float shGaugeAdjuster;

    [SerializeField]
    private float shAbilityTime;

    [SerializeField]
    private float shFireRateBoost;

    [SerializeField]
    private float shDamageBoost;

    [SerializeField]
    private int shPierceBoost;

    [Header("Sniper")]
    [SerializeField]
    private float snGaugeAdjuster;

    [SerializeField]
    private float snAbilityTime;

    [SerializeField]
    private float snFireRateReduce;

    [SerializeField]
    private float snDamageLevel3CritBoost;

    [SerializeField]
    private float snSpeedBoost;

    [SerializeField]
    private float snCritChanceBoost;

    [SerializeField]
    private int snPierceBoost;

    [Header("Deagle")]
    [SerializeField]
    private float deGaugeAdjuster;

    [SerializeField]
    private float deAbilityTime;

    [SerializeField]
    private float deNotLevel3Multi;

    [SerializeField]
    private float deNotLevel6Multi;

    [SerializeField]
    private float deFireRateBoost;

    [HideInInspector]
    public float deFireRateMagicOriginal; 

    [SerializeField]
    private float deDamageBoost;

    [HideInInspector]
    public float deDamageMagicOriginal; 

    [SerializeField]
    private float deSpeedBoost;

    [SerializeField]
    private float deSpeedMagicOriginal; 

    [SerializeField]
    private int dePierceBoost;

    [Header("M4")]
    [SerializeField]
    private Explosion m4Explosion;

    [SerializeField]
    private Explosion m4GrenadeExplosion;

    [SerializeField]
    private float m4GaugeAdjuster;

    [SerializeField]
    private float m4AbilityTime;

    [SerializeField]
    private float m4NotLevel3Multi;

    [SerializeField]
    private float m4NotLevel6Multi;

    [SerializeField]
    private float m4FireRateBoost;

    [SerializeField]
    private float m4GrenadeRateBoost;

    private float m4GrenadeRateBoostOriginal;

    [SerializeField]
    private float m4DamageBoost;

    [SerializeField]
    private float m4ExplosionRadiusBoost;

    [HideInInspector]
    public float m4ExplosionRadiusBoostOriginal;

    [SerializeField]
    private int m4PierceBoost;

    [Header("Universal Storage")]
    public float fireRateOriginal;

    public float damageOriginal;

    public float speedOriginal;

    [HideInInspector]
    public int levelOnAbility;

    [HideInInspector]
    public bool doNotUpdate;

    void Start()
    {
        SetActiveWeaponBar(PlayerStats.Instance.selectedSpriteIndex);
    }

    //We change the max ability gauge based on the weapon as some are stronger than others and require mores points
    public void SetMaxAbilityGauge()
    {
        if (currentGun == "Shotgun")
        {
            maxAbilityGauge *= shGaugeAdjuster;
        }
        else if (currentGun == "Sniper")
        {
            maxAbilityGauge *= snGaugeAdjuster;
        }
        else if (currentGun == "Deagle")
        {
            maxAbilityGauge *= deGaugeAdjuster;
        }
        else
        {
            maxAbilityGauge *= m4GaugeAdjuster;
        }

        if (startWithAbility)
        {
            PlayerStats.Instance.currentAbilityGauge = maxAbilityGauge;
        }
    }

    public void SetActiveWeaponBar(int characterIndex)
    {
        // Deactivate all health bars
        foreach (GameObject weaponBarObject in weaponBars)
        {
            weaponBarObject.SetActive(false);
        }

        // Activate the health bar that corresponds to the character
        GameObject selectedWeaponBarObject = weaponBars[characterIndex];
        selectedWeaponBarObject.SetActive(true);
    }

    //If the ability guage is more than the max we set it to the max, we have a switch when calling the ability chooser in which we are ethier using the ability or not. This is decided
    //by activating it with left click or deactivating it when the time reachers 0
    private void Update()
    {
        if (!PlayerStats.Instance.paused)
        {
            if (PlayerStats.Instance.currentAbilityGauge > maxAbilityGauge)
            {
                PlayerStats.Instance.currentAbilityGauge = maxAbilityGauge;
            }

            if (PlayerStats.Instance.currentAbilityGauge >= maxAbilityGauge && Input.GetKeyDown(KeyCode.Mouse0))
            {
                AbilityChooserSwitch(true);
            }

            if (abilityTimeLeft > 0)
            {
                abilityTimeLeft -= Time.deltaTime;

                if (abilityTimeLeft <= 0)
                {
                    AbilityChooserSwitch(false);
                }
            }
        }

        CircularFill();
        BarFill();
    }

    //Update the circular fill amount based on the weapons ability gauge
    void CircularFill()
    {
        float weaponPercentage = PlayerStats.Instance.currentAbilityGauge / maxAbilityGauge; // Calculate the weapon percentage
        float circularFill = weaponPercentage / circularPercentange; // Calculate the circular fill amount

        circularFill *= circularFillAmount; // Scale the circular fill amount
        circularFill = Mathf.Clamp(circularFill, 0, circularFillAmount); //Locks images fill amount between 0 and 0.75
        circularWeaponBar.fillAmount = circularFill; // Update the circular weapon bar
    }

    //Update the bar fill amount based on the weapons ability gauge
    void BarFill()
    {
        float circularAmount = circularPercentange * maxAbilityGauge; // Calculate the circular amount

        float weapon = PlayerStats.Instance.currentAbilityGauge - circularAmount; // Calculate the weapon ability gauge
        float barTotalWeapon = maxAbilityGauge - circularAmount; // Calculate the total weapon ability gauge
        weaponBar.fillAmount = weapon / barTotalWeapon; // Update the weapon bar
    }

    //Tells us if we are using the ability and in both cases of the starting if sets ability value to 0, calles the differnt abilitys based on the weapon used
    public void AbilityChooserSwitch(bool switchBool)
    {
        if (switchBool == true)
        {
            PlayerStats.Instance.currentAbilityGauge = 0;

            ablityBeingUsed = true;
        }
        else
        {
            if (!doNotUpdate)
            {
                PlayerStats.Instance.currentAbilityGauge = 0;
            }
        }

        if (currentGun == "Shotgun")
        {
            ShotgunAblity(switchBool);
        }
        else if (currentGun == "Sniper")
        {
            SniperAblity(switchBool);    
        }
        else if (currentGun == "Deagle")
        {
            DeagleAbility(switchBool);
        }
        else
        {
            M4Ability(switchBool);
        }
    }

    //We apply these stats to the weapon and store its original values in some cases when using the ability otherwise we set the weapons stats to its original value
    private void ShotgunAblity(bool switchBool)
    {
        if (switchBool == true)
        {
            abilityTimeLeft = shAbilityTime;

            fireRateOriginal = fireWeapon.actualFireRate;

            fireWeapon.actualFireRate = fireWeapon.actualFireRate * (1f - shFireRateBoost);

            damageOriginal = shotgun.damage;

            shotgun.damage = shotgun.damage * (1f + shDamageBoost);

            shotgun.pierce += shPierceBoost;
        }
        else
        {
            fireWeapon.actualFireRate = fireRateOriginal;

            shotgun.damage = damageOriginal;

            shotgun.pierce -= shPierceBoost;

            ablityBeingUsed = false;
        }
    }

    //We apply these stats to the weapon and store its original values in some cases when using the ability otherwise we set the weapons stats to its original value. We apply differnt 
    //stats based on the snipers level this is to attempt to balence the weapon against others at all levels taking into consideration the gauge ammount required.
    private void SniperAblity(bool switchBool)
    {
        if (switchBool)
        {
            if (!doNotUpdate)
            {
                abilityTimeLeft = snAbilityTime;
            }

            if (gunUpgrades.upgradeLevel == 6)
            {
                levelOnAbility = 6;

                fireRateOriginal = fireWeapon.actualFireRate;

                fireWeapon.actualFireRate = fireWeapon.actualFireRate * (1f + snFireRateReduce);

                sniper.critChance += snCritChanceBoost;
            }
            else if (gunUpgrades.upgradeLevel >= 3)
            {
                levelOnAbility = 3;

                sniper.critDamageMulti += snDamageLevel3CritBoost;

                sniper.critChance += snCritChanceBoost;
            }
            else if (gunUpgrades.upgradeLevel >= 0)
            {
                sniper.canCrit = true;
            }

            speedOriginal = sniper.speed;

            sniper.speed = sniper.speed * (1f + snSpeedBoost);

            sniper.pierce += snPierceBoost;
        }
        else
        {
            if (levelOnAbility == 6)
            {
                fireWeapon.actualFireRate = fireRateOriginal;

                sniper.critChance -= snCritChanceBoost;
            }
            else if (levelOnAbility >= 3)
            {
                sniper.critDamageMulti -= snDamageLevel3CritBoost;

                sniper.critChance -= snCritChanceBoost;
            }
            else if (levelOnAbility >= 0)
            {
                sniper.canCrit = false;
            }

            sniper.speed = speedOriginal;

            sniper.pierce -= snPierceBoost;

            ablityBeingUsed = false;
        }
    }

    //We apply these stats to the weapon and store its original values in some cases when using the ability otherwise we set the weapons stats to its original value. We apply differnt 
    //stats based on the snipers level this is to attempt to balence the weapon against others at all levels taking into consideration the gauge ammount required.
    private void DeagleAbility(bool switchBool)
    {
        if (switchBool)
        {
            float currentMulti = 1;

            if (!doNotUpdate)
            {
                abilityTimeLeft = shAbilityTime;
            }

            fireRateOriginal = deagleFireWeapon[0].actualFireRate;

            if (gunUpgrades.upgradeLevel == 6)
            {
                levelOnAbility = 6;

                deFireRateMagicOriginal = deagleFireWeapon[2].actualFireRate;

                deDamageMagicOriginal = deagleMagic.damage;

                deagleMagic.damage = deagleMagic.damage * (1f + deDamageBoost);

                deSpeedMagicOriginal = deagleMagic.speed;

                deagleMagic.speed = deagleMagic.speed * (1f + deSpeedBoost);

                deagleMagic.pierce += dePierceBoost;
            }
            else if (gunUpgrades.upgradeLevel >= 3)
            {
                levelOnAbility = 3;

                currentMulti = deNotLevel6Multi;
            }
            else
            {
                currentMulti = deNotLevel3Multi;
            }

            foreach (FireWeapon deagleFire in deagleFireWeapon)
            {
                deagleFire.actualFireRate = deagleFire.actualFireRate * (1f - deFireRateBoost * currentMulti);
            }

            damageOriginal = deagle.damage;

            deagle.damage = deagle.damage * (1f + deDamageBoost * currentMulti);

            speedOriginal = deagle.speed;

            deagle.speed = deagle.speed * (1f + deSpeedBoost);

            deagle.pierce += dePierceBoost;
        }
        else
        {
            if (levelOnAbility == 6)
            {
                deagleFireWeapon[2].actualFireRate = deFireRateMagicOriginal;

                deagleFireWeapon[3].actualFireRate = deFireRateMagicOriginal;

                deagleMagic.damage = deDamageMagicOriginal;

                deagleMagic.speed = deSpeedMagicOriginal;

                deagleMagic.pierce -= dePierceBoost;
            }

            deagleFireWeapon[0].actualFireRate = fireRateOriginal;

            deagleFireWeapon[1].actualFireRate = fireRateOriginal;

            deagle.damage = damageOriginal;         

            deagle.speed = speedOriginal;

            deagle.pierce -= dePierceBoost;

            ablityBeingUsed = false;
        }
    }

    //We apply these stats to the weapon and store its original values in some cases when using the ability otherwise we set the weapons stats to its original value. We apply differnt 
    //stats based on the snipers level this is to attempt to balence the weapon against others at all levels taking into consideration the gauge ammount required.
    private void M4Ability(bool switchBool)
    {
        if (switchBool)
        {
            float currentMulti = 1;

            abilityTimeLeft = m4AbilityTime;

            if (gunUpgrades.upgradeLevel == 6)
            {
                levelOnAbility = 6;

                m4GrenadeRateBoostOriginal = fireWeapon.m4Awaken.grenadeActualFireRate;

                fireWeapon.m4Awaken.grenadeActualFireRate = fireWeapon.m4Awaken.grenadeActualFireRate * (1f - m4GrenadeRateBoost);

                m4ExplosionRadiusBoostOriginal = m4Explosion.explosionRadius;

                m4Explosion.explosionRadius = m4Explosion.explosionRadius * (1f + m4ExplosionRadiusBoost);
            }
            else if (gunUpgrades.upgradeLevel >= 3)
            {
                levelOnAbility = 3;

                m4ExplosionRadiusBoostOriginal = m4Explosion.explosionRadius;

                m4Explosion.explosionRadius = m4Explosion.explosionRadius * (1f + m4ExplosionRadiusBoost);

                currentMulti = m4NotLevel6Multi;
            }
            else
            {
                currentMulti = m4NotLevel3Multi;
            }

            fireRateOriginal = fireWeapon.actualFireRate;

            fireWeapon.actualFireRate = fireWeapon.actualFireRate * (1f - m4FireRateBoost * currentMulti);

            damageOriginal = m4.damage;

            m4.damage = m4.damage * (1f + m4DamageBoost * currentMulti);

            m4.pierce += m4PierceBoost;
        }
        else
        {
            if (levelOnAbility == 6)
            {
                fireWeapon.m4Awaken.grenadeActualFireRate = m4GrenadeRateBoostOriginal;

                m4Explosion.explosionRadius = m4ExplosionRadiusBoostOriginal;
            }
            else if (levelOnAbility >= 3)
            {
                m4Explosion.explosionRadius = m4ExplosionRadiusBoostOriginal;
            }

            fireWeapon.actualFireRate = fireRateOriginal;

            m4.damage = damageOriginal;

            m4.pierce -= m4PierceBoost;

            ablityBeingUsed = false;
        }
    }

    //This is to reset prefab values that are kept over play test scenes
    private void OnApplicationQuit()
    {
        m4Explosion.ResetValues();

        m4GrenadeExplosion.ResetValues();
    }
}
