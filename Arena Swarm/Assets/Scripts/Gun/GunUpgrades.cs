using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//We inherit from GunUpgradeManager
public class GunUpgrades : GunUpgradeManager
{
    [Header("References")]
    [SerializeField]
    private GunAbility gunAbility;

    [Header("Shotgun")]
    [SerializeField]
    private float shLevel1Damage;

    [SerializeField]
    private float shLevel2LifeTime;

    [SerializeField]
    private float shLevel2FireRate;

    [SerializeField]
    private List<GameObject> shLevel3AddPellets = new List<GameObject>();

    [SerializeField]
    private float shLevel4Damage;

    [SerializeField]
    private float shLevel4FireRate;

    [SerializeField]
    private int shLevel5Pierce;

    [SerializeField]
    private float shLevel5LifeTime;

    [Header("Sniper")]
    [SerializeField]
    private float snLevel1Damage;

    [SerializeField]
    private int snLeve2Pierce;

    [SerializeField]
    private float snLevel2FireRate;

    [SerializeField]
    private float snLevel4Damage;

    [SerializeField]
    private float snLevel5Damage;

    [SerializeField]
    private float snLevel6FireRateNerf;

    [Header("Deagle")]
    [SerializeField]
    private float deLevel1Damage;

    [SerializeField]
    private float deLevel2FireRate;

    [SerializeField]
    private float deLevel4Damage;

    [SerializeField]
    private int deLevel4Pierce;

    [SerializeField]
    private float deLevel5Damage;

    [SerializeField]
    private float deLevel5FireRate;

    [Header("M4")]
    [SerializeField]
    private float m4Level1Damage;

    [SerializeField]
    private float m4Level2FireRate;

    [SerializeField]
    private float m4Level4Damage;

    [SerializeField]
    private float m4Level4FireRate;

    [SerializeField]
    private int m4Level5Pierce;

    [Header("Fields")]
    public int upgradeLevel;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
           // UpgradeChooser();
        }
    }

    //If this gets called we upgrade our weapon based on the one equipted
    public void UpgradeChooser()
    {
        upgradeLevel++;

        if (currentGun == "Shotgun")
        {
            ShotgunUpgrade();
        }
        else if (currentGun == "Sniper")
        {
            SniperUpgrade();
        }
        else if (currentGun == "Deagle")
        {
            DeagleUpgrade();
        }
        else
        {
            M4Upgrade();
        }
    }

    //We upgrade this weapons stats based on variables. Key points to note is on upgrade level 3 we add more spawnspots which allows the shotgun to shoot more pellets at one time.
    //At level 6 we also activate a variable for a bullet effect. If an ability is being used we store the weapons stats based on its upgrade level.
    private void ShotgunUpgrade()
    {
        switch (upgradeLevel)
        {
            case 1:
                shotgun.damage = shotgun.damage * (1f + shLevel1Damage);
                break;

            case 2:
                shotgun.lifeTimeWait = shotgun.lifeTimeWait * (1f + shLevel2LifeTime);

                fireWeapon.actualFireRate = fireWeapon.actualFireRate * (1f - shLevel2FireRate);
                break;

            case 3:
                fireWeapon.spawnSpot.AddRange(shLevel3AddPellets);
                break;

            case 4:
                shotgun.damage = shotgun.damage * (1f + shLevel4Damage);

                fireWeapon.actualFireRate = fireWeapon.actualFireRate * (1f - shLevel4FireRate);
                break;

            case 5:
                shotgun.pierce += shLevel5Pierce;

                shotgun.lifeTimeWait = shotgun.lifeTimeWait * (1f + shLevel5LifeTime);
                break;

            case 6:
                shotgun.splitShot = true;
                break;

            default:
                upgradeLevel = 6;
                break;             
        }    
        
        if (ablityBeingUsed)
        {
            switch (upgradeLevel)
            {
                case 1:
                    gunAbility.damageOriginal = gunAbility.damageOriginal * (1f + shLevel1Damage);
                    break;

                case 2:
                    gunAbility.fireRateOriginal = gunAbility.fireRateOriginal * (1f - shLevel2FireRate);
                    break;

                case 4:
                    gunAbility.damageOriginal = gunAbility.damageOriginal * (1f + shLevel4Damage);

                    gunAbility.fireRateOriginal = gunAbility.fireRateOriginal * (1f - shLevel4FireRate);
                    break;

                default:
                    break;
            }
        }
    }

    //We upgrade this weapons stats based on variables. Key points to note is on upgrade level 3 activate a bool to allow crits and at 6 we activate a bool to allow burst fire.
    //If an ability is being used we store the weapons stats based on its upgrade level.
    private void SniperUpgrade()
    {
        switch (upgradeLevel)
        {
            case 1:
                sniper.damage = sniper.damage * (1f + snLevel1Damage);
                break;

            case 2:
                sniper.pierce += snLeve2Pierce;

                fireWeapon.actualFireRate = fireWeapon.actualFireRate * (1f - snLevel2FireRate);
                break;

            case 3:
                sniper.canCrit = true;
                break;

            case 4:
                sniper.damage = sniper.damage * (1f + snLevel4Damage);
                break;

            case 5:
                sniper.damage = sniper.damage * (1f + snLevel5Damage);
                break;

            case 6:
                FireWeapon.SniperAwaken.sniperAwaken = true;

                fireWeapon.actualFireRate = fireWeapon.actualFireRate * (1f + snLevel6FireRateNerf);
                break;

            default:
                upgradeLevel = 6;
                break;
        }

        if (ablityBeingUsed)
        {
            switch (upgradeLevel)
            {
                case 1:
                    gunAbility.damageOriginal = gunAbility.damageOriginal * (1f + snLevel1Damage);
                    break;

                case 2:
                    gunAbility.fireRateOriginal = gunAbility.fireRateOriginal * (1f - snLevel2FireRate);
                    break;

                case 4:
                    gunAbility.damageOriginal = gunAbility.damageOriginal * (1f + snLevel4Damage);
                    break;

                case 5:
                    gunAbility.damageOriginal = gunAbility.damageOriginal * (1f + snLevel5Damage);
                    break;

                default:
                    break;
            }

            if (gunAbility.levelOnAbility < 6 && upgradeLevel == 6)
            {
                gunAbility.doNotUpdate = true;

                gunAbility.AbilityChooserSwitch(false);

                gunAbility.AbilityChooserSwitch(true);

                gunAbility.doNotUpdate = false;
            }
        }
    }

    //We upgrade this weapons stats based on variables. Key points to note is on upgrade level 3 we add more of the weapon and at 6 we set up magic deagles for use
    //If an ability is being used we store the weapons stats based on its upgrade level.
    private void DeagleUpgrade()
    {
        switch (upgradeLevel)
        {
            case 1:
                deagle.damage = deagle.damage * (1f + deLevel1Damage);
                break;

            case 2:
                deagleFireWeapon[0].actualFireRate = deagleFireWeapon[0].actualFireRate * (1f - deLevel2FireRate);
                
                break;

            case 3:
                FireWeapon.DeagleAwaken.doNotStart = true;

                deagleAkimbos[0].SetActive(true);

                deagleFireWeapon[1].actualFireRate = deagleFireWeapon[0].actualFireRate;

                foreach (FireWeapon deagleFire in deagleFireWeapon)
                {
                    deagleFire.DeagleReset();
                }
                break;

            case 4:
                deagle.damage = deagle.damage * (1f + deLevel4Damage);

                deagle.pierce += deLevel4Pierce;
                break;

            case 5:
                deagle.damage = deagle.damage * (1f + deLevel5Damage);

                foreach (FireWeapon deagleFire in deagleFireWeapon)
                {
                    deagleFire.actualFireRate = deagleFire.actualFireRate * (1f - deLevel5FireRate);
                }
                break;

            case 6:
                ConfigureMagicDeagles();

                foreach (FireWeapon deagleFire in deagleFireWeapon)
                {
                    deagleFire.DeagleReset();
                }
                break;

            default:
                upgradeLevel = 6;
                break;
        }

       
        if (ablityBeingUsed)
        {           
           switch (upgradeLevel)
           {
               case 1:
                   gunAbility.damageOriginal = gunAbility.damageOriginal * (1f + deLevel1Damage);
                   break;

               case 2:
                   gunAbility.fireRateOriginal = gunAbility.fireRateOriginal * (1f - deLevel2FireRate);

                   break;

               case 4:
                   gunAbility.damageOriginal = gunAbility.damageOriginal * (1f + deLevel4Damage);  
                   break;

               case 5:
                   gunAbility.damageOriginal = gunAbility.damageOriginal * (1f + deLevel5Damage);

                   gunAbility.fireRateOriginal = gunAbility.fireRateOriginal * (1f - deLevel5FireRate);
                   break;
               default:
                   break;
           }            
        }
    }

    //We upgrade this weapons stats based on variables. Key points to note is on upgrade level 3 we activate explosive bullets and at 6 we set up the m4 grenade launcher
    //If an ability is being used we store the weapons stats based on its upgrade level.
    private void M4Upgrade()
    {
        switch (upgradeLevel)
        {
            case 1:
                m4.damage = m4.damage * (1f + m4Level1Damage);
                break;

            case 2:
                fireWeapon.actualFireRate = fireWeapon.actualFireRate * (1f - m4Level2FireRate);
                break;

            case 3:
                m4.explosiveBullets = true;
                break;

            case 4:
                m4.damage = m4.damage * (1f + m4Level4Damage);

                fireWeapon.actualFireRate = fireWeapon.actualFireRate * (1f - m4Level4FireRate);
                break;

            case 5:
                m4.pierce += m4Level5Pierce;
                break;

            case 6:
                FireWeapon.M4Awaken.m4Awaken = true;
                break;
            default:
                upgradeLevel = 6;
                break;
        }

        if (ablityBeingUsed)
        {
            switch (upgradeLevel)
            {
                case 1:
                    gunAbility.damageOriginal = gunAbility.damageOriginal * (1f + m4Level1Damage);
                    break;

                case 2:
                    gunAbility.fireRateOriginal = gunAbility.fireRateOriginal * (1f - m4Level2FireRate);
                    break;

                case 4:
                    gunAbility.damageOriginal = gunAbility.damageOriginal * (1f + m4Level4Damage);

                    gunAbility.fireRateOriginal = gunAbility.fireRateOriginal * (1f - m4Level4FireRate);
                    break;

                default:
                    break;
            }

            if (gunAbility.levelOnAbility < 6 && upgradeLevel == 6)
            {
                gunAbility.doNotUpdate = true;

                gunAbility.AbilityChooserSwitch(false);

                gunAbility.AbilityChooserSwitch(true);

                gunAbility.doNotUpdate = false;
            }
        }
    }

    //We have this configure setup as when we level up our weapon during an ability we must setup some values so the weapon scales accordingly, this is especially the case with the deagles
    //due to the other deagles stats are based on the original. So we must take extra steps to make sure everything is how we want them. Even if the ability is not being used when we 
    //get to level 6 we must still take messures to set all the deagles 
    private void ConfigureMagicDeagles()
    {
        if (ablityBeingUsed)
        {
            if (gunAbility.levelOnAbility < 6 && upgradeLevel == 6)
            {
                gunAbility.doNotUpdate = true;

                gunAbility.AbilityChooserSwitch(false);

                deagleMagic.damage = deagle.damage;

                deagleMagic.pierce = deagle.pierce;

                deagleMagic.lifeTimeWait = deagle.lifeTimeWait;

                deagleAkimbos[1].SetActive(true);

                deagleAkimbos[2].SetActive(true);

                deagleFireWeapon[2].DeagleMagicAkimbo(deagleFireWeapon[0].actualFireRate);

                deagleFireWeapon[3].DeagleMagicAkimbo(deagleFireWeapon[0].actualFireRate);

                deagleMagic.SetAwakenDeagle();

                gunAbility.AbilityChooserSwitch(true);

                gunAbility.doNotUpdate = false;
            }
        }
        else
        {
            deagleMagic.damage = deagle.damage;

            deagleMagic.pierce = deagle.pierce;

            deagleMagic.lifeTimeWait = deagle.lifeTimeWait;

            deagleAkimbos[1].SetActive(true);

            deagleAkimbos[2].SetActive(true);

            deagleFireWeapon[2].DeagleMagicAkimbo(deagleFireWeapon[0].actualFireRate);

            deagleFireWeapon[3].DeagleMagicAkimbo(deagleFireWeapon[0].actualFireRate);

            deagleMagic.SetAwakenDeagle();
        }       
    }

    //We call the reseting of the bullet stats
    private void OnApplicationQuit()
    {
        bulletManager.ResetValues();

        if (currentGun == "Deagle")
        {
            deagleMagic.ResetValues();
        }
    }
}
