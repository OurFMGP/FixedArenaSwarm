using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//We inherit from bulletManager
public class M4 : BulletManager
{
    [Header("Level 3")]
    public bool explosiveBullets;

    [SerializeField]
    private GameObject explosive;

    [SerializeField]
    private float explosiveDamageReducer;

    //Similer to the original except we call another method when life counter is 0 while we have explosive bullets
    protected override void LifeCounter()
    {
        lifeTimeCounter += Time.deltaTime;

        if (lifeTimeCounter >= lifeTimeWait)
        {
            if (explosiveBullets)
            {
                ExplosiveSpawn();
            }

            Destroy(gameObject);
        }

        GetComponent<Rigidbody2D>().velocity = -transform.right * speed;
    }

    //Similer to the original except we call another method when pierce is 0  while we have explosive bullets
    protected override void TriggerEnter2D(Collider2D collider)
    {
        if (collider.CompareTag("BulletCollider"))
        {
            pierce -= 1;

            collider.gameObject.SendMessageUpwards("TakeGunDamage", damage * PlayerStats.Instance.currentDamage);

            if (pierce <= 0)
            {
                if (explosiveBullets)
                {
                    ExplosiveSpawn();
                }

                Destroy(gameObject);
            }
        }
    }

    //Spawns an explsive clone and calls the method SETBOOM
    private void ExplosiveSpawn()
    {
        GameObject newExplosive = Instantiate(explosive, transform.position, Quaternion.LookRotation(transform.forward));

        newExplosive.GetComponent<Explosion>().SETBOOM(damage * (1f - explosiveDamageReducer));
    }

    //resets the explosive variable in the m4 bullet then we call reset values within bulletmanager
    public override void ResetValues()
    {
        explosiveBullets = false;

        base.ResetValues();
    }
}
