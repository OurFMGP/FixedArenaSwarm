using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunSystem : MonoBehaviour
{
    [Header("Reference")]
    [SerializeField]
    private Retical retical;

    [SerializeField]
    private GunUpgrades gunUpgrades;

    [SerializeField]
    private GunAbility gunAbility;

    [Header("Weapons")]
    [SerializeField]
    private GameObject shotgun;

    [SerializeField]
    private GameObject sniper;

    [SerializeField]
    private List<GameObject> deagles = new List<GameObject>();

    [SerializeField]
    private GameObject M4;

    [SerializeField]
    private GameObject currentGun;

    [SerializeField]
    private GameObject currentAmmo;

    [Header("Fields")]
    [SerializeField]
    private bool usingShotgun;

    [SerializeField]
    private bool usingSniper;

    [SerializeField]
    private bool usingDeagle;

    [SerializeField]
    private bool usingM4;

    [HideInInspector]
    public int weaponNumber;

    //We set all weapons to false and we only set active the weapon we have stated using a bool. We add the current gun we are using to a retical so that the weapons know where to aim.
    //We also set current ammo that we will be firing as well as calling a method to set our ability. We also set the weapon we are using in the upgrade manager and ability manager
    void Start()
    {
        shotgun.SetActive(false);
        sniper.SetActive(false);

        foreach (GameObject deagles in deagles)
        {
            deagles.SetActive(false);
        }

        M4.SetActive(false);

        //Based on the character chosen we use the apropiate weapon
        if (PlayerStats.Instance.selectedSpriteIndex == 0)
        {
            usingShotgun = true;
        }
        else if (PlayerStats.Instance.selectedSpriteIndex == 1)
        {
            usingSniper = true;
        }
        else if (PlayerStats.Instance.selectedSpriteIndex == 2)
        {
            usingDeagle = true;
        }
        else
        {
            usingM4 = true;
        }

        if (usingShotgun)
        {
            currentGun = shotgun;
            shotgun.SetActive(true);

            weaponNumber = 1;
        }
        else if (usingSniper)
        {
            currentGun = sniper;
            sniper.SetActive(true);

            weaponNumber = 2;
        }
        else if (usingDeagle)
        {
            currentGun = deagles[0];
            deagles[0].SetActive(true);

            retical.currentGuns.AddRange(deagles);

            weaponNumber = 3;
        }
        else
        {
            currentGun = M4;
            M4.SetActive(true);

            weaponNumber = 4;
        }

        retical.currentGuns.Add(currentGun);

        currentAmmo = currentGun.GetComponentInChildren<FireWeapon>().currentBullet;

        if (!usingDeagle)
        {
            gunUpgrades.SetWeapon(currentGun, currentAmmo);
            gunAbility.SetWeapon(currentGun, currentAmmo);
        }
        else
        {
            gunUpgrades.SetDeagle(deagles, currentAmmo);
            gunAbility.SetDeagle(deagles, currentAmmo);
        }

        gunAbility.SetMaxAbilityGauge();
    }
}
