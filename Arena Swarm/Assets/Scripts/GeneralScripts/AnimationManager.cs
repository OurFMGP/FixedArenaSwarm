using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationManager : MonoBehaviour
{
    [Header("References")]
    public Animator animator;

    private string currentState;

    public void ChangeAnimationState(string newState)
    {
        if (currentState == newState) return;       //Stops the same animimation from interupting itself

        animator.Play(newState);        //Plays the animation based on state name

        currentState = newState;
    }

    public void AnimationEnded()
    {
        ChangeAnimationState("Nothing");        //Stops Attack animations from looping when we dont want them too
    }
}
