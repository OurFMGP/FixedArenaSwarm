using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateAround : MonoBehaviour
{
    [SerializeField]
    private GameObject player;

    public float rotateSpeed;

    //Rotating around the player based on rotate speed and deltaTime
    void Update()
    {
        transform.RotateAround(player.transform.position, -transform.forward, 1 * rotateSpeed * Time.deltaTime);
    }
}
