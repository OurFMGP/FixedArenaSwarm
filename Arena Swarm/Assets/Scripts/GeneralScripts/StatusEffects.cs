using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatusEffects : MonoBehaviour
{
    [Header("References")]
    [SerializeField]
    private EnemyHealth enemyHealth;

    [SerializeField]
    private SpriteRenderer spriteRenderer;

    [Header("Radiation")]
    public bool radiation;

    [SerializeField]
    private float radDamagePerSecondPercent;

    [SerializeField]
    private float radiationDuration;

    [Header("Midas")]
    public bool midasTouched;

    [SerializeField]
    private float midasDuration;

    [SerializeField]
    private int midasGoldGain;

    [SerializeField]
    private bool applyOnce;

    [Header("Fire")]
    public bool fire;

    [SerializeField]
    private float fireDamagePerSecondPercent;

    [SerializeField]
    private float fireDuration;

    [Header("Field")]
    [SerializeField]
    private float longestEffectDuration = 0;

    //We pass the radiation duration if the passed duration is the highest recorded we set the sprites color to green then start the Coroutine
    private void ApplyRadiation(float duration)
    {
        if (!radiation)
        {
            if (duration > longestEffectDuration)
            {
                longestEffectDuration = duration;

                spriteRenderer.color = Color.green;
            }

            radiation = true;

            radiationDuration = duration;

            StartCoroutine(EffectDuration());
        }
    }

    //We pass the midas duration if the passed duration is the highest recorded we set the sprites color to yellow then start the Coroutine
    private void ApplyMidasTouch(float duration)
    {
        if (!midasTouched)
        {
            if (duration > longestEffectDuration)
            {
                longestEffectDuration = duration;

                spriteRenderer.color = Color.yellow;
            }

            midasTouched = true;

            midasDuration = duration;

            StartCoroutine(EffectDuration());
        }
    }

    //We pass the fire duration if the passed duration is the highest recorded we set the sprites color to fire then start the Coroutine
    private void ApplyFire(float duration)
    {
        if (!fire)
        {
            if (duration > longestEffectDuration)
            {
                longestEffectDuration = duration;

                spriteRenderer.color = Color.red;
            }

            fire = true;

            fireDuration = duration;

            StartCoroutine(EffectDuration());
        }
    }

    //We keep the while loop for as long as the longest duration. we call and deal radiation and fire damage based on input data, if any of the status value is 0 or less we make that status
    //false. for midas touch we add extra gold on kill value while active otherwise we take it away. if all status are false we set the sprite to white
    private IEnumerator EffectDuration()
    {
        while (longestEffectDuration > 0)
        {
            yield return new WaitForSeconds(1);

            if (radiation)
            {
                radiationDuration--;

                enemyHealth.TakeStatusDamage(radDamagePerSecondPercent * PlayerStats.Instance.currentDamage);

                if (radiationDuration <= 0)
                {
                    radiation = false;
                }
            }

            if (midasTouched)
            {
                midasDuration--;

                if (!applyOnce)
                {
                    enemyHealth.goldGainOnKill += midasGoldGain;

                    applyOnce = true;
                }

                if (midasDuration <= 0)
                {
                    midasTouched = false;

                    enemyHealth.goldGainOnKill -= midasGoldGain;

                    applyOnce = false;
                }
            }

            if (fire)
            {
                fireDuration--;

                enemyHealth.TakeStatusDamage(fireDamagePerSecondPercent * PlayerStats.Instance.currentDamage);

                if (fireDuration <= 0)
                {
                    fire = false;
                }
            }

            longestEffectDuration--;

            yield return null;
        }

        spriteRenderer.color = Color.white;
    }
}
