using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UICamFix : MonoBehaviour
{
    [Header("References")]
    [SerializeField]
    private Camera uICamera;

    //The UI camera doesnt work as intented unless you turn it off then on again, which needs to be done again when parent camera is inactive at anytime it seems.
    private void OnEnable()
    {
        uICamera.enabled = false;

        uICamera.enabled = true;
    }
}
