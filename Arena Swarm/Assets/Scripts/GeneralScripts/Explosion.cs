using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
	[Header("References")]
	[SerializeField]
	private GameObject sphere;

	[Header("Explosion")]
	[SerializeField]
	private bool enemyExplosion;

	public float explosionRadius;

	[SerializeField]
	private float originalExplosionRadius;

	public float damage;

	[SerializeField]
	private float originalDamage;

	[SerializeField]
	private float explosionLife;

	[SerializeField]
	private float explosionTimeLimit;

	[Header("Status Effects")]
	public bool applyRadiation;

	[SerializeField]
	private float radDuration;	

	//M4 passed damage, gets all colliders in the radius and if you match the tag apply damage
	public void SETBOOM(float passedDamage)
    {
		damage = passedDamage;

		Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, explosionRadius);

		//sphere.transform.localScale = new Vector2(explosionRadius * 2, explosionRadius * 2);	//This is to show the explosion radius when there want a custom sprite

		sphere.SetActive(true);

		foreach (Collider2D collider in colliders)
		{
			float damageAmount = damage;

			if (enemyExplosion == true)
			{
				if (collider.CompareTag("Player"))
				{
					collider.GetComponent<Collider2D>().gameObject.SendMessageUpwards("TakeGunDamage", damageAmount, SendMessageOptions.DontRequireReceiver);
				}
			}
			else
			{
				if (collider.CompareTag("BulletCollider"))
				{
					collider.GetComponent<Collider2D>().gameObject.SendMessageUpwards("TakeGunDamage", damageAmount * PlayerStats.Instance.currentDamage, SendMessageOptions.DontRequireReceiver);
				}
			}
		}
	}

	// gets all colliders in the radius and if you match the tag apply damage as well as radiation if true
	public void BOOM()
	{
		// An array of nearby colliders
		Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, explosionRadius);

		//sphere.transform.localScale = new Vector2(explosionRadius * 2, explosionRadius * 2);	//This is to show the explosion radius when there want a custom sprite

		sphere.SetActive(true);

		foreach (Collider2D collider in colliders)
		{
			float damageAmount = damage;

			float newRadDuration = 0;

			if (applyRadiation)
            {
				newRadDuration = radDuration;
			}

			if (enemyExplosion == true)
			{
				if (collider.gameObject.tag == "Player")
				{
					collider.GetComponent<Collider2D>().gameObject.SendMessageUpwards("TakeGunDamage", damageAmount, SendMessageOptions.DontRequireReceiver);
				}
			}
			else
			{
				if (collider.CompareTag("BulletCollider"))
				{
					collider.GetComponent<Collider2D>().gameObject.SendMessageUpwards("TakeGunDamage", damageAmount * PlayerStats.Instance.currentDamage, SendMessageOptions.DontRequireReceiver);

					if (applyRadiation)
                    {
						collider.GetComponent<Collider2D>().gameObject.SendMessageUpwards("ApplyRadiation", newRadDuration, SendMessageOptions.DontRequireReceiver);
					}
				}
			}
		}
	}

	//When we destroy the explosion
	private void Update()
	{
		explosionLife += Time.deltaTime;

		if (explosionLife >= explosionTimeLimit)
		{
			Destroy(transform.gameObject);
		}
	}

	//Reset adjusted prefab values
    public void ResetValues()
    {
		explosionRadius = originalExplosionRadius;

		damage = originalDamage;

		applyRadiation = false;
    }
}
