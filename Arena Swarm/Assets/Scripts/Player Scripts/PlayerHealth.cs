using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    public Image healthBar; // Health bar image
    public Image circularHealthBar; // Circular health bar image
    public float circularPercentange; //how much of the whole HP bar the circular portion takes up
    private const float circularFillAmount = 0.75f; //how much of the circular portion of the HP bar is used
    public List<GameObject> healthBars; // List of health bar images
    public GameObject gameOverPanel; // Reference to the UI panel for game over.
    public bool invunrable;
    [SerializeField]
    private float invunrableTime;

    [SerializeField]
    private float invunrableCounter;

    private void Start()
    {
        // Set the active health bar
        SetActiveHealthBar(PlayerStats.Instance.selectedSpriteIndex);
        // Initialize current health to max health at the start.
        PlayerStats.Instance.currentHealth = PlayerStats.Instance.maxHealth;
    }

    public void SetActiveHealthBar(int characterIndex)
    {
        // Deactivate all health bars
        foreach (GameObject healthBarObject in healthBars)
        {
            healthBarObject.SetActive(false);
        }

        // Activate the health bar that corresponds to the character
        GameObject selectedHealthBarObject = healthBars[characterIndex];
        selectedHealthBarObject.SetActive(true);
    }

    private void Update()
    {
        // Methods to update the health bar
        CircularFill();
        BarFill();

        if (invunrable == true && !PlayerStats.Instance.paused)
        {
            if (invunrableCounter < invunrableTime)
            {
                invunrableCounter += Time.deltaTime;
            }
            else
            {
                invunrable = false;

                invunrableCounter = 0;
            }
        }
    }

    // Method to update the circular portion of the health bar
    void CircularFill()
    {
        float healthPercentage = PlayerStats.Instance.currentHealth / PlayerStats.Instance.maxHealth; // Calculate the health percentage
        float circularFill = healthPercentage / circularPercentange; // Calculate the circular fill amount

        circularFill *= circularFillAmount; // Scale the circular fill amount
        circularFill = Mathf.Clamp(circularFill, 0, circularFillAmount); //Locks images fill amount between 0 and 0.75
        circularHealthBar.fillAmount = circularFill; // Update the circular health bar
    }

    // Method to update the bar portion of the health bar
    void BarFill()
    {
        float circularAmount = circularPercentange * PlayerStats.Instance.maxHealth; // Calculate the circular amount

        float health = PlayerStats.Instance.currentHealth - circularAmount; // Calculate the health
        float barTotalHealth = PlayerStats.Instance.maxHealth - circularAmount; // Calculate the total health
        healthBar.fillAmount = health / barTotalHealth; // Update the health bar
    }

    // Update the player's health by the specified amount.
    public void ModifyHealth(float amount)
    {
        if (!invunrable && !PlayerStats.Instance.paused)
        {
            PlayerStats.Instance.currentHealth += amount;

            // Ensure that current health stays within the bounds of 0 and maxHealth.
            PlayerStats.Instance.currentHealth = Mathf.Clamp(PlayerStats.Instance.currentHealth, 0, PlayerStats.Instance.maxHealth);

            // Check if the player has reached zero health (you can add additional logic here).
            if (PlayerStats.Instance.currentHealth <= 0)
            {
                // Handle player's death or any other actions you want to take.
                Debug.Log("Player has died.");

                GameOver();
            }

            invunrable = true;
        }
    }

    // Method to pause the game and display game over UI.
    private void GameOver()
    {
        // Pause the game.
        Time.timeScale = 0f;

        // Display the game over UI panel.
        gameOverPanel.SetActive(true);
    }

    public void Heal(int amount)
    {
        PlayerStats.Instance.currentHealth += amount;
        PlayerStats.Instance.currentHealth = Mathf.Clamp(PlayerStats.Instance.currentHealth, 0, PlayerStats.Instance.maxHealth);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("EnemyBullet"))
        {
            // Get the EnemyBullet script from the enemy GameObject.
            EnemyBullet enemyBullet = collision.collider.GetComponent<EnemyBullet>();

            // Check if the enemyBullet script is not null.
            if (enemyBullet != null)
            {
                // Inflict damage on the player.
                TakeDamage(enemyBullet.playerDamage);
            }

            Destroy(enemyBullet.gameObject);
        }
    }

    // Public method to allow external scripts to inflict damage on the player.
    public void TakeDamage(int damageAmount)
    {
        // Reduce the player's health by the specified amount.
        ModifyHealth(-damageAmount);
    }
}
