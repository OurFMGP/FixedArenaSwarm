using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using UnityEngine.Animations;

public class PlayerMovement : MonoBehaviour
{
    public bool moving;

    public Vector2 moveInput;
    private Rigidbody2D rb;
    private PlayerControls controls;
    public Animator animator;
    public RuntimeAnimatorController[] animatorControllers; // Array of Animator Controllers
    public SpriteRenderer spriteRenderer;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        controls = new PlayerControls();
        PlayerStats.Instance.currentMoveSpeed = PlayerStats.Instance.maxMoveSpeed;
    }

    void Start()
    {
        // Set the sprite of the player character to the selected sprite
        spriteRenderer.sprite = PlayerStats.Instance.selectedSprite;
        // Set the Animator Controller of the Animator component to the corresponding Animator Controller
        animator.runtimeAnimatorController = animatorControllers[PlayerStats.Instance.selectedSpriteIndex];
    }

    private void OnEnable()
    {
        // Enable the input action
        controls.Player.Movement.performed += OnMovePerformed;
        controls.Player.Movement.canceled += OnMoveCanceled;
        controls.Player.Enable();
    }

    private void OnDisable()
    {
        // Disable the input action
        controls.Player.Movement.performed -= OnMovePerformed;
        controls.Player.Movement.canceled -= OnMoveCanceled;
        controls.Player.Disable();
    }

    private void OnMovePerformed(InputAction.CallbackContext context)
    {
        moveInput = context.ReadValue<Vector2>();

        moving = true;

        if (moving == true)
        {
            animator.SetBool("moving", true);
        }
        else
        {
            animator.SetBool("moving", false);
        }
    }

    private void OnMoveCanceled(InputAction.CallbackContext context)
    {
        moveInput = Vector2.zero;

        moving = false;

        if (moving == false)
        {
            animator.SetBool("moving", false);
        }
    }

    private void FixedUpdate()
    {
        if (!PlayerStats.Instance.paused)
        {
            // Apply movement based on input
            Vector2 moveVelocity = moveInput.normalized * PlayerStats.Instance.currentMoveSpeed;
            rb.velocity = moveVelocity;
        }
        else
        {
            rb.velocity = Vector2.zero;
        }
    }
}
