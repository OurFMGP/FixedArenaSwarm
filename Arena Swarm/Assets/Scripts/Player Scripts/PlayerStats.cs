using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerStats : MonoBehaviour
{   
    public static PlayerStats Instance { get; private set; }

    [SerializeField]
    private SkillLevelRandom skillLevelRandom;
    public Sprite selectedSprite; // The sprite of the selected character
    public Sprite selectedWeapon; // Sprite for the selected weapon
    public Sprite selectedExpIcon; // Sprite for the selected experience icon
    public RuntimeAnimatorController selectedAnimatorController; // The Animator Controller of the selected character
    public int selectedSpriteIndex; // The index of the selected sprite and animator controller
    public int level = 1; // Initial player level.
    public int gold;
    public float currentAbilityGauge;
    public float experience = 0;
    public int skillPoints  = 0;
    public int totalSkillPointsSpent = 0;
    public Dictionary<string, int> unlockedSkills = new Dictionary<string, int>(); // Skill name and level
    // Define the base experience required to reach the next level.
    public int baseExperiencePerLevel; // Adjust this value as needed.

    public bool paused;

    [Header("Player Attributes")]
    public float maxHealth; // Maximum health of the player.
    public float currentHealth; // Current health of the player.
    public float initialHealth;
    public float maxMoveSpeed; // Maximum move speed of the player.
    public float currentMoveSpeed; // Current move speed of the player.
    public float initialMoveSpeed;
    public float maxDamage; // Maximum damage of the player.
    public float currentDamage; // Current damage of the player.
    public float initialDamage;
    public float maxFireRate; // Maximum fire rate of the player.
    public float currentFireRate; // Current fire rate of the player.
    public float initialFireRate;
    public float maxBossDamageMultiplier; // Maximum boss damage multiplier of the player.
    public float currentBossDamageMultiplier; // Current boss damage multiplier of the player.
    public float initialBossDamageMultiplier;
    public float maxCooldownReduction; // Maximum cooldown reduction of the player.
    public float currentCooldownReduction; // Current cooldown reduction of the player.
    public float initialCooldownReduction;
    public float maxExperienceMultiplier; // Maximum experience multiplier of the player.
    public float currentExperienceMultiplier; // Current experience multiplier of the player.
    public float initialExperienceMultiplier;
    public int maxGoldMultiplier; // Maximum gold multiplier of the player.
    public int currentGoldMultiplier; // Current gold multiplier of the player.
    public int initialGoldMultiplier;
    public float maxSpawnDamageMultiplier;
    public float currentSpawnDamageMultiplier;
    public float initialSpawnDamageMultiplier;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        // Set the initial value for each stat
        currentHealth = initialHealth;
        currentMoveSpeed = initialMoveSpeed;
        currentDamage = initialDamage;
        currentFireRate = initialFireRate;
        currentBossDamageMultiplier = initialBossDamageMultiplier;
        currentCooldownReduction = initialCooldownReduction;
        currentExperienceMultiplier = initialExperienceMultiplier;
        currentGoldMultiplier = initialGoldMultiplier;
        currentSpawnDamageMultiplier = initialSpawnDamageMultiplier;
    }

    private void Update()
    {
        if(experience >= 1)
        {
            skillLevelRandom = FindAnyObjectByType<SkillLevelRandom>();
        }

        if (paused)
        {
            Cursor.visible = true;
        }
        else
        {
            Cursor.visible = false;
        }
    }

    // Call this function when the player earns experience points.
    public void EarnExperience(float amount)
    {
        experience += amount;
        // Check if the player has leveled up.
        CheckForLevelUp();
        PlayerUIManager playerUIManager = FindObjectOfType<PlayerUIManager>();
        if (playerUIManager != null)
        {
            playerUIManager.UpdateExpUI();
        }
    }

    // Check for level up based on the experience points.
    private void CheckForLevelUp()
    {
        int requiredExperienceForNextLevel = CalculateRequiredExperienceForNextLevel();

        while (experience >= requiredExperienceForNextLevel)
        {
            level++;
            skillPoints++; // Grant skill points upon leveling up.
            experience -= requiredExperienceForNextLevel;

            // Calculate the required experience for the next level.
            requiredExperienceForNextLevel = CalculateRequiredExperienceForNextLevel();

            if (skillLevelRandom != null)
            {
                skillLevelRandom.AddSkillPoints(1);
            }

        }
        PlayerUIManager playerUIManager = FindObjectOfType<PlayerUIManager>();
        if (playerUIManager != null)
        {
            playerUIManager.UpdateExpUI();
        }
    }

    // Calculate the required experience points for the next level.
    public int CalculateRequiredExperienceForNextLevel()
    {
        int requiredExperience = baseExperiencePerLevel * level;
        return requiredExperience;
    }
}