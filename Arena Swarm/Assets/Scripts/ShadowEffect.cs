using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class ShadowEffect : MonoBehaviour
{
    public Vector3 Offset = new Vector3(-0.1f, -0.1f);
    public Material Material;
    
    GameObject _shadow;

    [SerializeField]
    private Vector3 shadowSize;
    // Start is called before the first frame update
    void Start()
    {
        _shadow = new GameObject("Shadow");
        _shadow.transform.parent = transform;


        _shadow.transform.localPosition = Offset;
        _shadow.transform.localRotation = Quaternion.identity;

        SpriteRenderer renderer = GetComponent<SpriteRenderer> ();
        SpriteRenderer sr = _shadow.AddComponent<SpriteRenderer>();
        sr.sprite = renderer.sprite;
        sr.material = Material;

        //Make sure that shadow us always rendered behind the object
        sr.sortingLayerName = renderer.sortingLayerName;
        sr.sortingOrder = renderer.sortingOrder - 1;

        _shadow.transform.localScale = _shadow.transform.localScale - shadowSize;
    }

    // Update is called once per frame
    void LateUpdate(){
        _shadow.transform.localPosition = Offset;
        
    }
}
