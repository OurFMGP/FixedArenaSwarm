using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    public bool dontMove;

    public float speed = 3f; // Speed at which enemies move.

    [HideInInspector]
    public float baseSpeed;

    public Transform player; // Reference to the player's Transform.

    public float distance;

    [Header("Slow Effect")]
    [SerializeField]
    private bool canBeSlowed;

    [SerializeField]
    private bool slowed;

    [SerializeField]
    private float speedMutli;

    [SerializeField]
    private float slowAfterEffectTime;

    private float counter;
    public void Start()
    {
        if (!dontMove)
        {
            // Find the player's GameObject or your player's reference.
            player = GameObject.FindGameObjectWithTag("Player").transform;
        }

        baseSpeed = speed;
    }

    public void Update()
    {
        // Check if the player is found.
        if (player != null && !dontMove && !PlayerStats.Instance.paused)
        {
            // Calculate the direction towards the player.
            Vector2 directionToPlayer = (player.position - transform.position).normalized;

            // Apply movement towards the player using Rigidbody2D or transform.position.
            // Using transform.position for simple movement without physics.
            transform.Translate(directionToPlayer * speed * speedMutli * Time.deltaTime);           

            //If slowed is complete we call a method 
            if (slowed)
            {
                counter += Time.deltaTime;

                if (counter >= slowAfterEffectTime)
                {
                    counter = 0;

                    ReturnNormalSpeed();
                }
            }
        }

        distance = Vector2.Distance(transform.position, player.transform.position);
    }

    //if this can be slowed we do based on the passed amount
    public void Slowed(float passedSlowAmmount)
    {
        if (!slowed && canBeSlowed)
        {
            slowed = true;

            speedMutli -= passedSlowAmmount;
        }
    }

    //Returns the enemy to normal speed
    public void ReturnNormalSpeed()
    {
        slowed = false;

        speedMutli = 1;
    }
}
