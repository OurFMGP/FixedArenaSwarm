using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class EnemyHealth : MonoBehaviour
{
    [Header("References")]
    [SerializeField]
    protected AnimationManager animationManager;

    [Header("Fields")]
    public float maxHealth;

    public float currentHealth;

    [SerializeField]
    protected float ablityPointsWorth;

    [SerializeField]
    protected float experienceWorth;

    public int goldGainOnKill;

    public bool dead;

    [SerializeField]
    protected float despawnTime;

    [Header("Bosses")]
    [SerializeField]
    protected bool bossEnemy;

    [Header("Enemy Explosion")]
    [SerializeField]
    protected GameObject explosive;

    [SerializeField]
    protected EnemyExplosion enemyExplosion;

    [SerializeField]
    protected float maxHPDamagePassedPercent;

    [SerializeField]
    protected bool canExplode;

    public GameObject damageNumberPrefab;

    //We set the enemies health to max then reduce it based on spawn damage as long as it isnt a boss
    private void Start()
    {
        currentHealth = maxHealth;

        if (!bossEnemy)
        {
            currentHealth = currentHealth * (1f - PlayerStats.Instance.currentSpawnDamageMultiplier);
        }
    }

    //We reduce health by gun damage which can be increased if its a boss and we have extra boss damage, if health is 0 or less call dead
    protected virtual void TakeGunDamage(float damage)
    {
        if (!dead && !PlayerStats.Instance.paused)
        {
            if (bossEnemy)
            {
                damage = damage * (1f + PlayerStats.Instance.currentBossDamageMultiplier);
            }

            currentHealth -= damage;

            if (currentHealth <= 0)
            {
                Dead();
            }

            ShowDamageNumbers(damage);
        }
    }

    //We take damage based on max health percent, if health is 0 or less call dead
    public virtual void TakeStatusDamage(float damagePercent)
    {
        if (!dead && !PlayerStats.Instance.paused)
        {
            float damage = maxHealth * damagePercent;

            currentHealth -= damage;

            if (currentHealth <= 0)
            {
                Dead();
            }

            ShowDamageNumbers(damage);
        }
    }

    //We take damage based on enemy explosion skill which can deal bonus damage to bosses, if health is 0 or less we die but if chain reaction is active we will also explode
    protected virtual void TakeEnemyExplosionDamage(float damage)
    {
        if (!dead && !PlayerStats.Instance.paused)
        {
            if (bossEnemy)
            {
                damage = damage * (1f + PlayerStats.Instance.currentBossDamageMultiplier);
            }

            currentHealth -= damage;

            if (currentHealth <= 0)
            {
                if (!enemyExplosion.chainReaction)
                {
                     canExplode = false;
                }

                Dead();
            }

            ShowDamageNumbers(damage);
        }
    }

    protected void ShowDamageNumbers(float damage)
    {
        if (damage > 0)
        {
            // Instantiate a new damage number at the position of the enemy
            GameObject damageNumber = Instantiate(damageNumberPrefab, transform.position, Quaternion.identity);

            // Adjust the Z position of the damage number to make it appear in front of the enemy
            Vector3 position = damageNumber.transform.position;
            position.z = 1;
            damageNumber.transform.position = position;

            // Set the text of the damage number to the amount of damage dealt
            damageNumber.GetComponentInChildren<TextMeshPro>().text = damage.ToString("F1");

            // Destroy the damage number after a few seconds
            Destroy(damageNumber, 2f);  // Adjust this value as needed
        }
    }

    protected virtual void Dead()
    {
        dead = true;
        // Check for the PlayerStats component on the player object.
        PlayerStats playerStats = FindObjectOfType<PlayerStats>();
        if (playerStats != null)
        {
            // Award experience to the player.
            playerStats.EarnExperience(experienceWorth * PlayerStats.Instance.currentExperienceMultiplier);
        }

        PlayerStats.Instance.currentAbilityGauge += ablityPointsWorth;

        //We gain gold on kill if the value is more than 0
        if (goldGainOnKill > 0)
        {
            PlayerStats.Instance.gold += goldGainOnKill;
        }

        //This allows the enemyExplosion skill to activate once again by cloning an explosion and calling SETBOOM
        if (enemyExplosion.unlocked && canExplode)
        {
            GameObject newExplosive = Instantiate(explosive, transform.position, Quaternion.LookRotation(transform.forward));

            newExplosive.GetComponent<EnemyExplosion>().SETBOOM(maxHealth * maxHPDamagePassedPercent);
        }

        animationManager.ChangeAnimationState("Death");

        Destroy(gameObject, despawnTime);
    }
}
