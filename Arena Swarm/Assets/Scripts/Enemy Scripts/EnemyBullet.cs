using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EnemyBullet : MonoBehaviour
{
	private GameObject player;

	[Header("Fields")]
	public int playerDamage;

	public float shieldDamage;

	[SerializeField]
	private float speed;

	public float delayFireCounter;

	[SerializeField]
	private float lifeTimeWait;

	[SerializeField]
	private float lifeTimeCounter;

	[Header("Modifiers")]
	public float delayFire;

	public bool lockOnPlayer;

	public int swirlChanger; //0 is no swirl, 1 is right curve 2 is left while 3 is random curve

	public float swirlSpeed;

	private bool swirlCurveRight;

	private bool checkSwirlOnce;

	[Serializable]
	public struct BloodOrb
    {
		public GameObject child;

		public List<GameObject> orbBarrels;

		public GameObject bloodBullet;

		public bool usingBloodOrb;

		public float sizePerSecondIncrese;

		public float rotationSpeed;

		public float whenToFireTime;

		public float whenToFireCounter;
	}

	[SerializeField]
	private BloodOrb bloodOrb;

	private void Update()
	{
		//Increases the counter and when the delay is over we call lifecounter and swirlshot if applicable. We have differnt curves based on swirlChanger check variable for more info.
		if (!PlayerStats.Instance.paused)
		{
			if (delayFireCounter <= delayFire)
			{
				delayFireCounter += Time.deltaTime;

				//If we are using the blood orb while in delay fire we increase its size
				if (bloodOrb.usingBloodOrb)
                {
					transform.localScale += new Vector3(transform.localScale.x * bloodOrb.sizePerSecondIncrese, transform.localScale.y * bloodOrb.sizePerSecondIncrese, 
					transform.localScale.z) * Time.deltaTime;
                }

				//We only want to call lock on if we are not longer in delay otherwise this will turn into a homing shot
				if (lockOnPlayer)
				{
					if (player == null)
					{
						player = FindFirstObjectByType<PlayerHealth>().gameObject;
					}

					PlayerLockOn();
				}
			}
			else
            {
				LifeCounter();

				if (swirlChanger > 0)
                {
					if (checkSwirlOnce == false)
                    {
						swirlCurveRight = true;

						if (swirlChanger == 2)
						{
							swirlCurveRight = false;
						}
						else if (swirlChanger == 3)
						{
							int randomCurve = UnityEngine.Random.Range(1, 3);

							if (randomCurve == 2)
							{
								swirlCurveRight = false;
							}
						}

						checkSwirlOnce = true;
					}

					SwirlShot(swirlCurveRight);
                }

				if (bloodOrb.usingBloodOrb)
                {
					BloodOrbController();
                }
			}
		}
	}

	//Keeps track of how long the bullet has been alive for and also applys speed to the rigidbodys velocity
	private void LifeCounter()
	{
		lifeTimeCounter += Time.deltaTime;

		if (lifeTimeCounter >= lifeTimeWait)
		{
			Destroy(gameObject);
		}

		GetComponent<Rigidbody2D>().velocity = -transform.right * speed;
	}

	//If bullets have delay before fire and we want it to aim at the target, this should be active to allow decent aim
	private void PlayerLockOn()
    {
		transform.right -= player.transform.position - transform.position;
	}

	//Adds some rotation on the shot so it fires in a circular angle over time, which the curve can be changed.
	private void SwirlShot(bool rightCurve)
    {
		if (rightCurve)
        {
			transform.Rotate(0, 0, swirlSpeed * Time.deltaTime);
		}
		else
        {
			transform.Rotate(0, 0, -swirlSpeed * Time.deltaTime);
		}
	}

	//We rotate the blood orbs child and like every other bullet we have a counter which when reached we fire from every barrel in the orb barrels. We rotate as this will make our bullets
	//come from differnt sides when fired, usually all the time.
	private void BloodOrbController()
    {
		bloodOrb.child.transform.Rotate(0, 0, bloodOrb.rotationSpeed * Time.deltaTime);

		if (bloodOrb.whenToFireCounter <= bloodOrb.whenToFireTime)
        {
			bloodOrb.whenToFireCounter += Time.deltaTime;
        }
		else
        {
			foreach (GameObject barrel in bloodOrb.orbBarrels)
			{
				Instantiate(bloodOrb.bloodBullet, barrel.transform.position, barrel.transform.rotation);
			}

			bloodOrb.whenToFireCounter = 0;
		}
	}
}
