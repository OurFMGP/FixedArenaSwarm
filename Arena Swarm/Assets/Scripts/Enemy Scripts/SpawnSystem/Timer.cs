using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Timer : MonoBehaviour
{
    public float timer = 0.0f;

    public TextMeshProUGUI timerText; // Reference to the TextMeshPro component

    [SerializeField]
    private BossManager bossManager;

    // Start is called before the first frame update
    void Start()
    {
        timer = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (!PlayerStats.Instance.paused && timer < 300.0f) // 300 seconds = 5 minutes - If you change this number dont forget to change it in the Spawner
        {
            timer += Time.deltaTime;
            int minutes = Mathf.FloorToInt(timer / 60F);
            int seconds = Mathf.FloorToInt(timer - minutes * 60);
            timerText.text = string.Format("Time: {0:00}:{1:00}", minutes, seconds);
        }
        else if (!PlayerStats.Instance.paused && timer > 300.0f)
        {
            bossManager.BossTime();
        }
    }
}
