using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MiniBoss : MonoBehaviour
{
    public float maxHealth;
    public float currentHealth;
    public float ablityPointsWorth;
    public float experienceWorth;
    public int goldGainOnKill;
    public bool dead;
    public float despawnTime;
    public bool bossEnemy;
    public GameObject explosive;
    public EnemyExplosion enemyExplosion;
    public float maxHPDamagePassedPercent;
    public bool canExplode;
    public GameObject damageNumberPrefab;

    public UnityAction OnDefeated;

    private void Start()
    {
        currentHealth = maxHealth;

        if (!bossEnemy)
        {
            currentHealth = currentHealth * (1f - PlayerStats.Instance.currentSpawnDamageMultiplier);
        }
    }

    public void TakeDamage(float damage)
    {
        currentHealth -= damage;

        if (currentHealth <= 0)
        {
            dead = true;
            OnDefeated?.Invoke();
            Destroy(gameObject, despawnTime);
        }
    }
}
