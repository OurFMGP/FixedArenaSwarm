using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BasicSpawn : MonoBehaviour
{
    public GameObject[] enemyPrefabs;
    public Timer timer;
    public float minX;
    public float maxX;
    public float minY;
    public float maxY;
    public float waveInterval = 5f;
    public float counter;
    public bool spawnInstant;
    public float playerDistanceSafty;
    public GameObject player;

    private void Update()
    {
        if (spawnInstant)
        {
            StartCoroutine(SpawnRegularEnemies());

            spawnInstant = false;
        }

        if (!PlayerStats.Instance.paused && timer.timer < 300)
        {
            if (counter < waveInterval)
            {
                counter += Time.deltaTime;
            }
            else
            {
                StartCoroutine(SpawnRegularEnemies());

                counter = 0;
            }
        }
    }

    IEnumerator SpawnRegularEnemies()
    {
        while (true)
        {
            foreach (GameObject enemyPrefab in enemyPrefabs)
            {
                Vector3 spawnPosition = new Vector3(Random.Range(minX, maxX), Random.Range(minY, maxY), 0);

                float distance = Vector2.Distance(spawnPosition, player.transform.position);

                if (distance > playerDistanceSafty)
                {
                    Instantiate(enemyPrefab, spawnPosition, Quaternion.identity);
                }
            }

            yield return null;

            break;
        }
    }
}