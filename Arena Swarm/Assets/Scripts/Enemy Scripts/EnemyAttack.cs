using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyAttack : MonoBehaviour
{
    [Header("References")]
    [SerializeField]
    private AnimationManager animationManager;

    [SerializeField]
    private EnemyHealth enemyHealth;

    [SerializeField]
    private EnemyMovement enemyMovement;

    public PlayerHealth playerHealth; // Reference to the PlayerHealth script.

    private GameObject player;

    [Header("Fields")]
    public int contactDamageAmount; // Amount of damage the enemy attack inflicts.

    [SerializeField]
    private float distanceToAttack;

    [SerializeField]
    private float distanceToTarget;

    public bool attackingStopMove;

    [Header("Ranged")]
    [SerializeField]
    private bool slimeEnemy;

    [SerializeField]
    private bool notSlimeRanged;

    [SerializeField]
    private float distanceToAttackRanged;

    [SerializeField]
    private GameObject spawnSpot;

    [SerializeField]
    private GameObject bullet;

    [SerializeField]
    private float whenToAttack;

    [SerializeField]
    private float whenToAttackCounter;

    private void Start()
    {
        player = FindObjectOfType<PlayerHealth>().gameObject;
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        // Check if the trigger event is with the player.
        if (other.CompareTag("Player"))
        {
            if (playerHealth == null)
            {
                // Get the PlayerHealth script from the player GameObject.
                playerHealth = other.GetComponent<PlayerHealth>();
            }

            // Check if the playerHealth script is not null.
            if (playerHealth != null)
            {
                // Inflict damage on the player.
                playerHealth.TakeDamage(contactDamageAmount);
            }
        }
    }

    //We check the distance of this enemy and compare it to the player. if you have a distance to attack value indicating that you have a melee animation, and the distance to target is less
    //than that value we change the animation to attack. Otherwise we change the animation to walk, we do the same with ranged attacked but with a ranged attack varaible instead.
    //With ranged attacks we have a counter to when we actually fire a bullet, this is reset on each fire.
    private void Update()
    {
        if (!enemyHealth.dead && !PlayerStats.Instance.paused)
        {
            distanceToTarget = Vector2.Distance(transform.position, player.transform.position);

            if (!slimeEnemy)
            {
                if (distanceToTarget < distanceToAttack)
                {
                    animationManager.ChangeAnimationState("Attack");

                    if (attackingStopMove)
                    {
                        enemyMovement.speed = 0;
                    }
                }
                else
                {
                    animationManager.ChangeAnimationState("Walk");

                    if (attackingStopMove)
                    {
                        enemyMovement.speed = enemyMovement.baseSpeed;
                    }
                }

                if (notSlimeRanged && distanceToTarget < distanceToAttackRanged)
                {
                    if (whenToAttackCounter < whenToAttack)
                    {
                        whenToAttackCounter += Time.deltaTime;
                    }
                    else
                    {
                        GameObject treeBullet = Instantiate(bullet, transform.position, Quaternion.identity);

                        treeBullet.GetComponent<EnemyBullet>().lockOnPlayer = true;

                        whenToAttackCounter = 0;
                    }
                }
            }  
            else
            {
                SlimeAttackLogic();
            }
        }
    }

    //We have seperate method for slime attack as they have an animation event that decides when its projectile will fire. Its similar to the ranged attack function above but with the main
    //Changes being, changing animation to walk while charging up an attack and changing animation when its about to attack.
    private void SlimeAttackLogic()
    {
        if (distanceToTarget < distanceToAttackRanged)
        {
            if (whenToAttackCounter < whenToAttack)
            {
                whenToAttackCounter += Time.deltaTime;

                animationManager.ChangeAnimationState("Walk");
            }
            else
            {
                spawnSpot.transform.right -= player.transform.position - spawnSpot.transform.position;

                animationManager.ChangeAnimationState("Attack");
            }

            if (attackingStopMove)
            {
                enemyMovement.speed = 0;
            }
        }
        else
        {
            animationManager.ChangeAnimationState("Walk");

            if (attackingStopMove)
            {
                enemyMovement.speed = enemyMovement.baseSpeed;
            }

            whenToAttackCounter = 0;
        }
    }

    //Called using animation event to spawn a bullet 
    private void SlimeShoot()
    {
        Instantiate(bullet, spawnSpot.transform.position, spawnSpot.transform.rotation);
    }

    //Called using animation event, resets the bullet counter
    private void SlimeBulletReset()
    {
        whenToAttackCounter = 0;
    }
}