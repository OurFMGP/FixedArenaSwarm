using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ToolTipText : MonoBehaviour
{
    //We manually store all tool tip information we dont automate this as there is quite abit that doesnt overlap and will require unquie ways to automate this information to GUI.
    [Header("References")]
    public List<string> currentTexts = new List<string>();

    public TextMeshProUGUI displayInfomation;

    public TextMeshProUGUI extraSkillInfomation;

    [SerializeField]
    private GameObject[] buttons;

    [Header("Weapons")]
    public List<string> shotgunTexts = new List<string>();

    public List<string> sniperTexts = new List<string>();

    public List<string> deagleTexts = new List<string>();

    public List<string> m4Texts = new List<string>();

    [Header("Passives")]
    public List<string> passiveTexts = new List<string>();

    [Header("Active Skills")]
    public List<string> rpgTexts = new List<string>();

    public List<string> axeTexts = new List<string>();

    public List<string> crossbowTexts = new List<string>();

    public List<string> frisbeeTexts = new List<string>();

    public List<string> turretTexts = new List<string>();

    public List<string> shieldTexts = new List<string>();

    public List<string> damageAuraTexts = new List<string>();

    public List<string> flameWalkTexts = new List<string>();

    public List<string> enemyExplosionTexts = new List<string>();

    public List<string> javelinTexts = new List<string>();

    [Header("Field")]
    public string defaultDisplayInformation;

    //Hides buttons and resets some information
    public void ButtonHide()
    {
        foreach (GameObject button in buttons)
        {
            button.SetActive(false);
        }

        displayInfomation.text = defaultDisplayInformation;

        extraSkillInfomation.text = null;
    }

    //Shows buttons as long as the skills isnt empty
    public void ButtonShow(string skillOne, string skillTwo, string skillThree)
    {
        if (skillOne != "Empty1")
        {
            buttons[0].SetActive(true);
        }

        if (skillTwo != "Empty2")
        {
            buttons[1].SetActive(true);
        }

        if (skillThree != "Empty3")
        {
            buttons[2].SetActive(true);
        }
    }
}
