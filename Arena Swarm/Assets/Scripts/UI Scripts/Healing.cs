using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Healing : MonoBehaviour
{
    public int healAmount;

    public void HealPlayer(PlayerHealth playerHealth)
    {
        playerHealth.Heal(healAmount);
    }
}
