using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerUIManager : MonoBehaviour
{
    public Button healButton;
    public PlayerHealth playerHealth;
    public PotionItem potionItem;
    public TextMeshProUGUI potionCountText; // Reference to the UI Text element for displaying potion count.    
    public int currentPotionCount = 0; // Current potion count.
    public int maxPotionCount = 5; // Maximum number of potions the player can hold.
    public TextMeshProUGUI levelText; // Reference to the UI Text for displaying the player's level.
    public Image expBar; // Reference to the UI Image for the experience bar.
    public List<GameObject> expIcons; // List of experience icons.

    void Start()
    {
        SetActiveExpIcon(PlayerStats.Instance.selectedSpriteIndex);

        // Disable the heal button by default.
        healButton.interactable = false;
        
        // Add an onClick listener to the healButton to perform the PotionUsed action.
        healButton.onClick.AddListener(PotionUsed);

        UpdateExpUI(); // Update the UI text to display the initial values.
    }

    public void SetActiveExpIcon(int characterIndex)
    {
        // Deactivate all health bars
        foreach (GameObject expIconObject in expIcons)
        {
            expIconObject.SetActive(false);
        }

        // Activate the health bar that corresponds to the character
        GameObject selectedExpIconObject = expIcons[characterIndex];
        selectedExpIconObject.SetActive(true);
    }

    public void EnableHealButton()
    {
        // Check if the player hasn't reached the maximum potion count.
        if (currentPotionCount < maxPotionCount)
        {
            // Enable the heal button when the player picks up a health potion.
            healButton.interactable = true;
            // Increment the potion count.
            currentPotionCount++;
            // Update the potion count text.
            UpdatePotionCountText();
        }
        else
        {
            // If the player has reached the maximum potion count, destroy the item.
            Destroy(potionItem.gameObject);
        }
    }

    public void PotionUsed()
    {        
        // Decrease the potion count.
        currentPotionCount--;

        // Check if the player is out of potions and disable the button.
        if (currentPotionCount <= 0)
        {
            healButton.interactable = false;
        }

        // Update the potion count text.
        UpdatePotionCountText();
    }
    public void UpdatePotionCountText()
    {
        // Update the UI Text to display the current potion count.
        potionCountText.text = currentPotionCount + " / " + maxPotionCount;
    }

    public void UpdateExpUI()
    {
        // Find the PlayerStats component in the scene.
        PlayerStats playerStats = FindObjectOfType<PlayerStats>();

        // Check if the playerStats is not null.
        if (playerStats != null)
        {
            // Update the level text to display the player's current level.
            levelText.text = "Level: " + playerStats.level;

            // Calculate the percentage of experience points the player has earned.
            float expPercentage = playerStats.experience / playerStats.CalculateRequiredExperienceForNextLevel();

            // Update the fill amount of the experience bar.
            expBar.fillAmount = expPercentage;
        }
    }
}
