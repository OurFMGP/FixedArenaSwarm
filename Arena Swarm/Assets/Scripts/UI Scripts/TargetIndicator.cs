using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetIndicator : MonoBehaviour
{
    public float hideDistance;

    private Transform bossTarget;

    void Start()
    {
        // Find and set the boss target dynamically
        bossTarget = FindBoss();

        if (bossTarget != null)
        {
            SetTarget(bossTarget);
        }
    }

    void Update()
    {
        if (bossTarget == null)
        {
            SetChildrenActive(false);
        }
        else
        {
            var dir = bossTarget.position - transform.position;

            if (dir.magnitude < hideDistance)
            {
                SetChildrenActive(false);
            }
            else
            {
                foreach (Transform child in transform)
                {
                    SetChildrenActive(true);
                }
                var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
                transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
            }
        }
    }

    public void SetTarget(Transform newTarget)
    {
        bossTarget = newTarget;
    }

    void SetChildrenActive(bool value)
    {
        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(value);
        }
    }

    // Implement FindBoss() to locate and return the spawned boss.
    Transform FindBoss()
    {
        // Find the boss object and return its transform.
        GameObject bossObject = GameObject.FindWithTag("Boss");

        if (bossObject != null)
        {
            return bossObject.transform;
        }
        else
        {
            return null; // Return null if the boss is not found.
        }
    }
}
