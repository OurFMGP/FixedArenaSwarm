using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class PermanentSkillManager : MonoBehaviour
{
    [Header("Health Upgrades")]
    [SerializeField]
    private float healthIncrease;
    [SerializeField]
    private float bonusHealthIncrease;
    public int healthLevel;

    [Header("Speed Upgrades")]
    [SerializeField]
    private float speedIncrease;
    [SerializeField]
    private float bonusSpeedIncrease;
    public int speedLevel;

    [Header("FireRate Upgrades")]
    [SerializeField]
    private float fireRateIncrease;
    [SerializeField]
    private float bonusFireRateIncrease;
    public int fireRateLevel;

    [Header("Damage Upgrades")]
    [SerializeField]
    private float damageIncrease;
    [SerializeField]
    private float bonusDamageIncrease;
    public int damageLevel;

    [Header("Boss Damage Upgrades")]
    [SerializeField]
    private float increasedBossDamage;
    [SerializeField]
    private float bonusBossDamageIncrease;
    public static float currentBossDamage;
    public int bossDamageLevel;

    [Header("Cooldown Upgrades")]
    [SerializeField]
    private float reducedCooldown;
    [SerializeField]
    private float bonusCooldownReduction;
    public static float currentCooldownReduction;
    public int cooldownLevel;

    [Header("Exp Upgrades")]
    [SerializeField]
    private float increasedExp;
    [SerializeField]
    private float bonusExpIncrease;
    public static float currentExpGain = 1;
    public int expUpgradeLevel;

    [Header("Gold Upgrades")]
    [SerializeField]
    private int increasedGold;
    [SerializeField]
    private int bonusGoldIncrease;
    public static int currentGoldGain;
    public int goldLevel;

    // Start is called before the first frame update
    void Start()
    {
        currentExpGain = 1;
        currentBossDamage = 1;
        currentCooldownReduction = 1;
        currentGoldGain = 1;
    }
    public void HealthIncrease()
    {
        if (healthLevel != 50)
        {
            healthLevel++;
            PlayerStats.Instance.maxHealth = PlayerStats.Instance.maxHealth * (1f + healthIncrease);
            PlayerStats.Instance.currentHealth = PlayerStats.Instance.maxHealth;
        }
    }
    public void BonusHealthIncrease()
    {
        PlayerStats.Instance.maxHealth = PlayerStats.Instance.maxHealth * (1f + bonusHealthIncrease);
        PlayerStats.Instance.currentHealth = PlayerStats.Instance.maxHealth;
    }
    public void SpeedIncrease()
    {
        if (speedLevel != 50)
        {
            speedLevel++;
            PlayerStats.Instance.maxMoveSpeed = PlayerStats.Instance.maxMoveSpeed * (1f + speedIncrease);
            PlayerStats.Instance.currentMoveSpeed = PlayerStats.Instance.maxMoveSpeed;
        }
    }
    public void BonusSpeedIncrease()
    {
        PlayerStats.Instance.maxMoveSpeed = PlayerStats.Instance.maxMoveSpeed * (1f + bonusSpeedIncrease);
        PlayerStats.Instance.currentMoveSpeed = PlayerStats.Instance.maxMoveSpeed;
    }
    public void FireRateIncrease()
    {
        if (fireRateLevel != 50)
        {
            fireRateLevel++;
            PlayerStats.Instance.maxFireRate = PlayerStats.Instance.maxFireRate * (1f + fireRateIncrease);
            PlayerStats.Instance.currentFireRate = PlayerStats.Instance.maxFireRate;
        }
    }
    public void BonusFireRateIncrease()
    {
        PlayerStats.Instance.maxFireRate = PlayerStats.Instance.maxFireRate * (1f + bonusFireRateIncrease);
        PlayerStats.Instance.currentFireRate = PlayerStats.Instance.maxFireRate;
    }
    public void DamageIncrease()
    {
        if (damageLevel != 50)
        {
            damageLevel++;
            PlayerStats.Instance.maxDamage = PlayerStats.Instance.maxDamage * (1f + damageIncrease);
            PlayerStats.Instance.currentDamage = PlayerStats.Instance.maxDamage;
        }
    }
    public void BonusDamageIncrease()
    {
        PlayerStats.Instance.maxDamage = PlayerStats.Instance.maxDamage * (1f + bonusDamageIncrease);
        PlayerStats.Instance.currentDamage = PlayerStats.Instance.maxDamage;
    }
    public void IncreasedBossDamage()
    {
        if (bossDamageLevel != 50)
        {
            bossDamageLevel++;
            PlayerStats.Instance.maxBossDamageMultiplier = PlayerStats.Instance.maxBossDamageMultiplier * (1f + increasedBossDamage);
            PlayerStats.Instance.currentBossDamageMultiplier = PlayerStats.Instance.maxBossDamageMultiplier;
        }
    }
    public void BonusBossDamage()
    {
        PlayerStats.Instance.maxBossDamageMultiplier = PlayerStats.Instance.maxBossDamageMultiplier * (1f + bonusBossDamageIncrease);
        PlayerStats.Instance.currentBossDamageMultiplier = PlayerStats.Instance.maxBossDamageMultiplier;
    }
    public void CooldownReduction()
    {
        if (cooldownLevel != 50)
        {
            cooldownLevel++;
            PlayerStats.Instance.maxCooldownReduction = PlayerStats.Instance.maxCooldownReduction * (1f + reducedCooldown);
            PlayerStats.Instance.currentCooldownReduction = PlayerStats.Instance.maxCooldownReduction;
        }
    }
    public void BonusCooldownReduction()
    {
        PlayerStats.Instance.maxCooldownReduction = PlayerStats.Instance.maxCooldownReduction * (1f + bonusCooldownReduction);
        PlayerStats.Instance.currentCooldownReduction = PlayerStats.Instance.maxCooldownReduction;
    }
    public void IncreasedExp()
    {
        if (expUpgradeLevel != 50)
        {
            expUpgradeLevel++;
            PlayerStats.Instance.maxExperienceMultiplier = PlayerStats.Instance.maxExperienceMultiplier * (1f + increasedExp);
            PlayerStats.Instance.currentExperienceMultiplier = PlayerStats.Instance.maxExperienceMultiplier;
        }
    }
    public void BonusExp()
    {
        PlayerStats.Instance.maxExperienceMultiplier = PlayerStats.Instance.maxExperienceMultiplier * (1f + bonusExpIncrease);
        PlayerStats.Instance.currentExperienceMultiplier = PlayerStats.Instance.maxExperienceMultiplier;
    }
    public void IncreasedGold()
    {
        if (goldLevel != 50)
        {
            goldLevel++;
            PlayerStats.Instance.maxGoldMultiplier = PlayerStats.Instance.maxGoldMultiplier * (1 + increasedGold);
            PlayerStats.Instance.currentGoldMultiplier = PlayerStats.Instance.maxGoldMultiplier;
        }
    }
    public void BonusGold()
    {
        PlayerStats.Instance.maxGoldMultiplier = PlayerStats.Instance.maxGoldMultiplier * (1 + bonusGoldIncrease);
        PlayerStats.Instance.currentGoldMultiplier = PlayerStats.Instance.maxGoldMultiplier;
    }
}