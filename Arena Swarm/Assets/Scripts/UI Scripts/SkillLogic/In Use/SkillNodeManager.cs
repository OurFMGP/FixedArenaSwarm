using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using System.Linq;

public class SkillNodeManager : MonoBehaviour
{
    public List<SkillNodePassive> skillNodes; // List of all skill nodes
    public List<string> unlockedNodeNames; // List of all unlocked skill nodes

    void Start()
    {
        LoadUnlockedNodes(); // Load unlocked nodes from PlayerPrefs
        
    }

    public void AddUnlockedNode(SkillNodePassive node) // Add unlocked node to list
    {
        unlockedNodeNames.Add(node.skillName); // Add node to list
        SaveUnlockedNodes(); // Save unlocked nodes to PlayerPrefs
    }

    public void SaveUnlockedNodes() // Save unlocked nodes to PlayerPrefs
    {
        PlayerPrefs.SetString("UnlockedNodes", string.Join(",", unlockedNodeNames.ToArray())); // Save unlocked nodes to PlayerPrefs
        PlayerPrefs.Save(); // Save PlayerPrefs
    }

    public void ResetUnlockedNodes() // Reset unlocked nodes
    {
        PlayerPrefs.DeleteKey("UnlockedNodes"); // Delete unlocked nodes from PlayerPrefs
        PlayerPrefs.Save(); // Save PlayerPrefs

        // Clear the list of unlocked node names
        unlockedNodeNames.Clear();

        // Loop through all skill nodes and set them as locked
        foreach (SkillNodePassive node in skillNodes)
        {
            node.SetUnlocked(false); // Set the node as locked
            node.UpdateButtonState(); // Update the button state
        }

        // Add the total skill points spent back to the player's skill points
        PlayerStats.Instance.skillPoints += PlayerStats.Instance.totalSkillPointsSpent;

        // Reset the total skill points spent
        PlayerStats.Instance.totalSkillPointsSpent = 0;

        PlayerStats.Instance.maxHealth = PlayerStats.Instance.initialHealth; // Reset health
        PlayerStats.Instance.maxMoveSpeed = PlayerStats.Instance.initialMoveSpeed; // Reset move speed
        PlayerStats.Instance.maxDamage = PlayerStats.Instance.initialDamage; // Reset damage
        PlayerStats.Instance.maxFireRate = PlayerStats.Instance.initialFireRate; // Reset fire rate
        PlayerStats.Instance.maxBossDamageMultiplier = PlayerStats.Instance.initialBossDamageMultiplier; // Reset boss damage multiplier
        PlayerStats.Instance.maxCooldownReduction = PlayerStats.Instance.initialCooldownReduction; // Reset cooldown reduction
        PlayerStats.Instance.maxExperienceMultiplier = PlayerStats.Instance.initialExperienceMultiplier; // Reset experience multiplier
        PlayerStats.Instance.maxGoldMultiplier = PlayerStats.Instance.initialGoldMultiplier; // Reset gold multiplier

        PlayerStats.Instance.currentHealth = PlayerStats.Instance.initialHealth; // Reset health
        PlayerStats.Instance.currentMoveSpeed = PlayerStats.Instance.initialMoveSpeed; // Reset move speed
        PlayerStats.Instance.currentDamage = PlayerStats.Instance.initialDamage; // Reset damage
        PlayerStats.Instance.currentFireRate = PlayerStats.Instance.initialFireRate; // Reset fire rate
        PlayerStats.Instance.currentBossDamageMultiplier = PlayerStats.Instance.initialBossDamageMultiplier; // Reset boss damage multiplier
        PlayerStats.Instance.currentCooldownReduction = PlayerStats.Instance.initialCooldownReduction; // Reset cooldown reduction
        PlayerStats.Instance.currentExperienceMultiplier = PlayerStats.Instance.initialExperienceMultiplier; // Reset experience multiplier
        PlayerStats.Instance.currentGoldMultiplier = PlayerStats.Instance.initialGoldMultiplier; // Reset gold multiplier  

        PermanentSkillManager permanentSkillManager = FindObjectOfType<PermanentSkillManager>();
        if (permanentSkillManager != null)
        {
            permanentSkillManager.healthLevel = 1;
            permanentSkillManager.speedLevel = 1;
            permanentSkillManager.fireRateLevel = 1;
            permanentSkillManager.damageLevel = 1;
            permanentSkillManager.bossDamageLevel = 1;
            permanentSkillManager.cooldownLevel = 1;
            permanentSkillManager.expUpgradeLevel = 1;
            permanentSkillManager.goldLevel = 1;
        }        
    }

    public void LoadUnlockedNodes() // Load unlocked nodes from PlayerPrefs
    {
        string unlockedNodes = PlayerPrefs.GetString("UnlockedNodes"); // Get unlocked nodes from PlayerPrefs
        if (!string.IsNullOrEmpty(unlockedNodes)) // Check if unlocked nodes is not empty
        {
            unlockedNodeNames = new List<string>(unlockedNodes.Split(',')); // Split unlocked nodes by comma
            foreach (string nodeName in unlockedNodeNames) // Loop through unlocked nodes
            {
                SkillNodePassive node = skillNodes.Find(x => x.skillName == nodeName); // Find node by name
                if (node != null)
                {
                    node.SetUnlocked(true); // Set node as unlocked without actually unlocking it
                }
            }
        }
    }
}