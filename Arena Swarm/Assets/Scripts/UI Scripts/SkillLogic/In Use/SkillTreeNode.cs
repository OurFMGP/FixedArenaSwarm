using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SkillTreeNode : MonoBehaviour
{
    public string skillName;
    public string skillDescription;
    public int skillLevel; // Minimum required skill level to unlock.
    public int maxSkillLevel; // Maximum level the skill can reach.
    public int skillPointCost = 1; // Skill point cost to unlock or upgrade.
    public SkillTreeNode[] prerequisites; // An array of prerequisite skills.
    public Button unlockButton; // Reference to the unlock button in the UI.

    private SkillTreeManager skillTreeManager;

    private void Start()
    {
        // Get a reference to the SkillTreeManager.
        skillTreeManager = FindObjectOfType<SkillTreeManager>();

        // Add click event listeners to the buttons.
        unlockButton.onClick.AddListener(UnlockSkill);
    }

    private void UnlockSkill()
    {
        skillTreeManager.TryUnlockSkill(this);
    }
}
