using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillNodeList : MonoBehaviour
{
    public List<SkillNodePassive> skillNodes; // List of all skill nodes in the passive skill tree.
    public List<string> unlockedNodeNames;

    // Add a node to the list of unlocked nodes
    public void AddUnlockedNode(SkillNodePassive node)
    {
        if (!unlockedNodeNames.Contains(node.skillName))
        {
            unlockedNodeNames.Add(node.skillName);
        }
    }

    // Remove a node from the list of unlocked nodes
    public void RemoveUnlockedNode(SkillNodePassive node)
    {
        if (unlockedNodeNames.Contains(node.skillName))
        {
            unlockedNodeNames.Remove(node.skillName);
        }
    }

    // Save the list of unlocked nodes to PlayerPrefs
    public void SaveUnlockedNodes()
    {
        PlayerPrefs.SetString("UnlockedNodes", string.Join(",", unlockedNodeNames.ToArray()));
    }

    // Load the list of unlocked nodes from PlayerPrefs
    public void LoadUnlockedNodes()
    {
        string unlockedNodesString = PlayerPrefs.GetString("UnlockedNodes", "");
        string[] unlockedNodesArray = unlockedNodesString.Split(',');

        unlockedNodeNames.Clear();

        foreach (string nodeName in unlockedNodesArray)
        {
            if (!string.IsNullOrEmpty(nodeName))
            {
                unlockedNodeNames.Add(nodeName);
            }
        }
    }
}
