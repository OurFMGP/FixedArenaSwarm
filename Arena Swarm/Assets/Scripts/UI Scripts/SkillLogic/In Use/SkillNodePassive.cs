using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using System.Linq;

public class SkillNodePassive : MonoBehaviour
{
    public string skillName; // Name of the skill
    public string skillDescription; // Description of the skill
    public int skillPointCost; // Skill point cost
    public Button unlockButton; // Unlock button
    public List<SkillNodePassive> prerequisites = new List<SkillNodePassive>(); // List of prerequisites
    public List<SkillNodePassive> dependentNodes = new List<SkillNodePassive>(); // List of dependent nodes
    private bool unlocked; // Is the skill unlocked
    public ShowSkillInfo showSkillInfo; // Show skill info

    private void Start() // Start is called before the first frame update
    {
        PopulateDependentNodes(); // Populate dependent nodes
        UpdateButtonState(); // Update button state

        EventTrigger trigger = gameObject.AddComponent<EventTrigger>();
        EventTrigger.Entry entryEnter = new EventTrigger.Entry { eventID = EventTriggerType.PointerEnter };
        entryEnter.callback.AddListener((data) => { OnPointerEnter((PointerEventData)data); });
        trigger.triggers.Add(entryEnter);

        EventTrigger.Entry entryExit = new EventTrigger.Entry { eventID = EventTriggerType.PointerExit };
        entryExit.callback.AddListener((data) => { OnPointerExit((PointerEventData)data); });
        trigger.triggers.Add(entryExit);
    }

    private void PopulateDependentNodes() // Populate dependent nodes
    {
        foreach (SkillNodePassive node in FindObjectsOfType<SkillNodePassive>()) // Loop through all skill nodes
        {
            if (node != this) // Check if node is not the same as this node
            {
                dependentNodes.Add(node); // Add node to dependent nodes
            }
        }
    }

    public void TryUnlockSkill() // Try to unlock the skill
    {
        if (!IsUnlocked() && CanUnlockSkill()) // Check if skill is not unlocked and can be unlocked
        {
            UnlockSkill(); // Unlock skill
        }
    }

    public bool CanUnlockSkill() // Check if the skill can be unlocked
    {
        return PlayerStats.Instance.skillPoints >= skillPointCost && (prerequisites == null || prerequisites.All(prerequisite => prerequisite.IsUnlocked())); // Check if player has enough skill points and all prerequisites are unlocked
    }

    public void SetUnlocked(bool unlocked)
    {
        this.unlocked = unlocked;
    }

    public void UnlockSkill()
    {
        if (!IsUnlocked() && CanUnlockSkill()) // Check if the node can be unlocked again before unlocking
        {
            PlayerStats.Instance.skillPoints -= this.skillPointCost; // Deduct skill points only when unlocking this specific node.
            PlayerStats.Instance.totalSkillPointsSpent += this.skillPointCost; // Increment total skill points spent
            SetUnlocked(true); // Set the node as unlocked
            UpdateButtonState(); // Notify the manager of the state change.

            foreach (SkillNodePassive dependentNode in dependentNodes)
            {
                dependentNode.UpdateButtonState(); // Update the state of the dependent nodes.
            }
        }
    }

    public bool IsUnlocked() // Check if the skill is unlocked
    {
        return unlocked; // Return unlocked
    }

    public void UpdateButtonState() // Update button state
    {
        if (unlockButton != null) // Check if unlock button is assigned
        {
            if (IsUnlocked()) 
            {
                unlockButton.interactable = false; // Set button to not interactable
                unlockButton.image.color = Color.red; // Set button color to red
            }
            else if (CanUnlockSkill()) 
            {
                unlockButton.interactable = true; // Set button to interactable
                unlockButton.image.color = Color.green; // Set button color to green
            }
            else
            {
                unlockButton.interactable = false; // Set button to not interactable
                unlockButton.image.color = Color.grey; // Set button color to grey
            }
        }
    }

    public void OnPointerEnter(BaseEventData eventData)
    {
        // Show skill information when the mouse enters the node
        if (showSkillInfo != null)
        {
            showSkillInfo.ShowSelectedSkillInfo(this);
        }
    }

    public void OnPointerExit(BaseEventData eventData)
    {
        // You can add code here to hide the skill information when the mouse exits the node
        // For example, clear the text in the UI
        if (showSkillInfo != null)
        {
            showSkillInfo.ClearSkillInfo();
        }
    }
}