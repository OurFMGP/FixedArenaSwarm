using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IncreaseHealthSkill : MonoBehaviour
{
    // Called when the button is clicked.
    public void IncreaseMaxHealth()
    {
        // Increase the player's max health by 5 points.
        PlayerStats.Instance.maxHealth += 5;
    }
}