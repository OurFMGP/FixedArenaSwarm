using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class SkillTreeControl : MonoBehaviour
{
    public GameObject skillTreeUI; // Reference to the Skill Tree UI GameObject.
    private bool isSkillTreeOpen = false; // Flag to track the skill tree state.
    private PlayerControls controls;
    private float originalTimeScale; // Store the original time scale.

    private void Awake()
    {
        controls = new PlayerControls();
    }
    private void Start()
    {
        // Ensure the skill tree UI is initially hidden.
        if (skillTreeUI != null)
        {
            skillTreeUI.SetActive(false);
        }
    }

    private void Update()
    {
        // Check for the "ToggleSkillTree" input action (Tab key).
        if (Keyboard.current[Key.Tab].wasPressedThisFrame)
        {
            ToggleSkillTree();
        }
        // Pause or unpause the game based on the skill tree state.
        if (isSkillTreeOpen)
        {
            // Pause the game.
            Time.timeScale = 0f;
        }
        else
        {
            // Unpause the game.
            Time.timeScale = 1f;
        }
    }

    // Toggle the visibility of the skill tree UI.
    private void ToggleSkillTree()
    {
        isSkillTreeOpen = !isSkillTreeOpen;

        if (skillTreeUI != null)
        {
            skillTreeUI.SetActive(isSkillTreeOpen);
        }
    }
}
