using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SkillTreeManager : MonoBehaviour
{
    public List<SkillTreeNode> skillNodes; // A list of all skill nodes in the tree.

    // Call this function to attempt unlocking a skill node.
    public void TryUnlockSkill(SkillTreeNode skillNode)
    {
        if (CanUnlockSkill(skillNode))
        {
            // Deduct skill point cost and mark the skill as unlocked.
            PlayerStats.Instance.skillPoints -= skillNode.skillPointCost;
            PlayerStats.Instance.unlockedSkills[skillNode.skillName] = 1; // 1 represents the initial level of the skill.
        }
    }

    // Check if a skill can be unlocked.
    public bool CanUnlockSkill(SkillTreeNode skillNode)
    {
        if (PlayerStats.Instance.skillPoints >= skillNode.skillPointCost &&
            !PlayerStats.Instance.unlockedSkills.ContainsKey(skillNode.skillName) &&
            ArePrerequisitesMet(skillNode))
        {
            return true;
        }
        return false;
    }

    // Check if all skill prerequisites are met.
    private bool ArePrerequisitesMet(SkillTreeNode skillNode)
    {
        foreach (SkillTreeNode prerequisite in skillNode.prerequisites)
        {
            if (!PlayerStats.Instance.unlockedSkills.ContainsKey(prerequisite.skillName) ||
                PlayerStats.Instance.unlockedSkills[prerequisite.skillName] < prerequisite.skillLevel)
            {
                return false;
            }
        }
        return true;
    }    
}
