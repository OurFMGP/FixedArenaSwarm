using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplaySkillUI : MonoBehaviour
{
    public Text skillPointsText; // Reference to the Text UI element where skill points will be displayed.
    public Text healthText; // Reference to the Text UI element where health will be displayed.
    public Text speedText; // Reference to the Text UI element where speed will be displayed.
    public PlayerHealth playerHealth; // Reference to the PlayerHealth script.
    public PlayerMovement playerMovement; // Reference to the PlayerMovement script.

    private void Start()
    {
        // Initialize the skill points text.
        UpdateSkillPointsUI();
        UpdateUpgradedHealthUI();
        UpdateUpgradedSpeedUI();
    }

    private void Update()
    {
        // Continuously update the UI text.
        UpdateSkillPointsUI();
        UpdateUpgradedHealthUI();
        UpdateUpgradedSpeedUI();
    }

    // Update the UI to display the player's skill points.
    private void UpdateSkillPointsUI()
    {
        if (skillPointsText != null && PlayerStats.Instance != null)
        {
            // Get the current skill points from the PlayerStats script.
            int skillPoints = PlayerStats.Instance.skillPoints;

            // Update the skill points text.
            skillPointsText.text = "Skill Points: " + skillPoints.ToString();
        }
    }

    // Update the UI to display the player's health.
    private void UpdateUpgradedHealthUI()
    {
        if (healthText != null && playerHealth != null)
        {
            // Get the current max health from the PlayerHealth script.
            float maxHealth = PlayerStats.Instance.maxHealth;

            // Update the health text with the current max health.
            healthText.text = "" + PlayerStats.Instance.maxHealth.ToString();
        }
    }

    // Update the UI to display the player's speed.
    private void UpdateUpgradedSpeedUI()
    {
        if (speedText != null && playerMovement != null)
        {
            // Get the current speed from the PlayerMovement script.
            float speed = PlayerStats.Instance.currentMoveSpeed;

            // Update the speed text with the current speed.
            speedText.text = "" + speed.ToString("F2");
        }
    }   
}
