using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplaySkillPoints : MonoBehaviour
{
    public Text skillPointsText; // Reference to the Text UI element where skill points will be displayed.
    public PlayerStats playerStats; // Reference to the PlayerStats script.

    private void Start()
    {
        // Initialize the skill points text.
        UpdateSkillPointsUI();
    }

    private void Update()
    {
        // Continuously update the skill points text (if the skill points change).
        UpdateSkillPointsUI();
    }

    // Update the UI to display the player's skill points.
    private void UpdateSkillPointsUI()
    {
        if (skillPointsText != null && playerStats != null)
        {
            // Get the current skill points from the PlayerStats script.
            int skillPoints = playerStats.skillPoints;

            // Update the skill points text.
            skillPointsText.text = "Skill Points: " + skillPoints.ToString();
        }
    }
}
