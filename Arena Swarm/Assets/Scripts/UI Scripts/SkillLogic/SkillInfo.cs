using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SkillInfo : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public Text skillInfo; // Reference to the Text element where skill info will be displayed.
    public SkillTreeNode skillNode; // Reference to the skill node that this script is attached to.

    // Called when the mouse enters a skill node image.
    public void OnPointerEnter(PointerEventData eventData)
    {
        if (skillNode != null)
        {
            // Update the SkillInfoText with the skill's information.
            skillInfo.text = "Skill Name: " + skillNode.skillName + "\n" +
            "Description: " + skillNode.skillDescription;
        }
    }

    // Called when the mouse exits the skill node image.
    public void OnPointerExit(PointerEventData eventData)
    {
        // Clear the SkillInfoText when the mouse exits the skill node.
        skillInfo.text = "";
    }
}