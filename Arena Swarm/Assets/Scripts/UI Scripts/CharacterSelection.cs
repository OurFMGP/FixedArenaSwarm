using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterSelection : MonoBehaviour
{
    public List<Sprite> characterSprites; // List of character sprites
    public List<Sprite> weaponSprites; // List of weapon sprites
    public List<Sprite> expSprites; // List of experience sprites
    public List<RuntimeAnimatorController> characterAnimators; // List of character animator controllers
    private int selectedIndex = 0; // Index of the currently selected character

    public void SelectCharacter(int index)
    {        
        // Select the character at the given index
        selectedIndex = index;
        // Save the selected sprite to the PlayerStats instance
        PlayerStats.Instance.selectedSprite = characterSprites[selectedIndex];
        PlayerStats.Instance.selectedWeapon = weaponSprites[selectedIndex]; // Assign the selected weapon icon
        PlayerStats.Instance.selectedExpIcon = expSprites[selectedIndex];
        PlayerStats.Instance.selectedAnimatorController = characterAnimators[selectedIndex];
        PlayerStats.Instance.selectedSpriteIndex = selectedIndex;
    }
}