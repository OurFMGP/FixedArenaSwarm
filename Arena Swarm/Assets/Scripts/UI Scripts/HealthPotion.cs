using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HealthPotion : MonoBehaviour
{
    public int healthToRestore;
    public PlayerHealth playerHealth;
    public TextMeshProUGUI healthText;
    public GameObject healthPotionUI; // Reference to the Health Potion UI element.    
    private bool hasPickedUpHealthPotion = false;

    private void Start()
    {
        playerHealth = FindObjectOfType<PlayerHealth>();
        healthPotionUI.SetActive(false); // Disable the UI element initially.
    }

    public void PickUpHealthPotion()
    {
        hasPickedUpHealthPotion = true;
        healthPotionUI.SetActive(true); // Enable the UI element when a health potion is picked up.
    }

    public void RestoreHealth()
    {
        if (playerHealth != null && hasPickedUpHealthPotion)
        {
            playerHealth.ModifyHealth(healthToRestore);
        }
    }
}