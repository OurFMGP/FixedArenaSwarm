using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;
using UnityEngine.InputSystem;
using UnityEngine.EventSystems;

public class MainHub : MonoBehaviour
{
    //reference points for specific items within the Main Hub and different Settings pages
    public GameObject character, skills, settings, credits;
    public string stage1, stage2, stage3;
    //used to identify which items should be selected on specific screen open and close actions
    public GameObject characterReturnButton, characterClosed, skillsReturnButton, skillsClosed, settingsReturnButton, settingsClosed, creditsReturnButton, creditsClosed, mainHub;
    private PlayerControls controls;

    private void Awake()
    {
        controls = new PlayerControls();
    }

    private void Start()
    {
        PlayerStats.Instance.paused = true;
    }

    public void StartStage1()
    {
        SceneManager.LoadScene(stage1);
    }
    public void StartStage2()
    {
        SceneManager.LoadScene(stage2);
    }

    public void StartStage3()
    {
        SceneManager.LoadScene(stage3);
    }

    //the functions below open and close the relevant page, ie weapons, and reurn the player to the last button pressed for easy navigation
    public void OpenCharacter()
    {
        character.SetActive(true);
        mainHub.SetActive(false);

        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(characterReturnButton);
    }

    public void CloseCharacter()
    {
        character.SetActive(false);
        mainHub.SetActive(true);

        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(characterClosed);
    }
    public void OpenSkills()
    {
        skills.SetActive(true);
        mainHub.SetActive(false); 

        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(skillsReturnButton);
    }

    public void CloseSkills()
    {
        skills.SetActive(false);
        mainHub.SetActive(true);

        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(skillsClosed);
    }

    public void OpenSettings()
    {
        settings.SetActive(true);
        mainHub.SetActive(false);

        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(settingsReturnButton);
    }

    public void CloseSettings()
    {
        settings.SetActive(false);
        mainHub.SetActive(true);

        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(settingsClosed);
    }

    public void OpenCredits()
    {
        credits.SetActive(true);
        settings.SetActive(false);

        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(creditsReturnButton);
    }

    public void CloseCredits()
    {
        credits.SetActive(false);
        settings.SetActive(true);

        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(creditsClosed);
    }

    //when the Quit button is pressed, the game is closed
    public void QuitGame()
    {
        Application.Quit();
    }

    private void OnEnable() => controls.Player.Enable();

    private void OnDisable() => controls.Player.Disable();
}