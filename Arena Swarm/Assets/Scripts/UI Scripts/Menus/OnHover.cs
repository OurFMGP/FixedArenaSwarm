using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

//This kind of acts as a button, which is useful such as in this case when gui buttons dont want to work.
public class OnHover : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [Header("References")]
    [SerializeField]
    private SkillLevelRandom SkillLevelRandom;

    [SerializeField]
    private ToolTipText toolTipText;

    [Header("Fields")]
    [SerializeField]
    private int skillCube;

    //On enter we Show the revalant information based on whats in tooltips. (this is why we have show buttons in toolTip on an invoke as there isnt an onPointerStay with Ipointer, so
    //we force the exit so we can enter again).
    public void OnPointerEnter(PointerEventData pointerEventData)
    {
        if (skillCube == 1)
        {
            toolTipText.displayInfomation.text = SkillLevelRandom.skillOne;

            toolTipText.extraSkillInfomation.text = toolTipText.currentTexts[0];
        }
        else if (skillCube == 2)
        {
            toolTipText.displayInfomation.text = SkillLevelRandom.skillTwo;

            toolTipText.extraSkillInfomation.text = toolTipText.currentTexts[1];
        }
        else
        {
            toolTipText.displayInfomation.text = SkillLevelRandom.skillThree;

            toolTipText.extraSkillInfomation.text = toolTipText.currentTexts[2];
        }
    }

    //If we do exit the button we reset to defualt values in toolTip
    public void OnPointerExit(PointerEventData pointerEventData)
    {
        toolTipText.displayInfomation.text = toolTipText.defaultDisplayInformation;

        toolTipText.extraSkillInfomation.text = null;
    }
}
