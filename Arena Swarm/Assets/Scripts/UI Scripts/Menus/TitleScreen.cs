using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;
using UnityEngine.InputSystem;
using UnityEngine.EventSystems;

public class TitleScreen : MonoBehaviour
{
    public string mainHub;

    public void StartGame()
    {
        SceneManager.LoadScene(mainHub);
    }
}
