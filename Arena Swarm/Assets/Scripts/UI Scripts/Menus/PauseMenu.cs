using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class PauseMenu : MonoBehaviour
{
    public string mainHubScene;
    public GameObject pauseMenu;
    private float originalTimeScale;
    public bool paused = false;

    private PlayerControls controls;

    private void Awake()
    {
        controls = new PlayerControls();
    }

    void Update()
    {
        if (controls.Player.Pause.triggered && !paused) // Check if not paused and Pause triggered
        {
            PauseGame();
        }
        else if (controls.Player.Pause.triggered && paused) // Check if paused and Pause triggered
        {
            ResumeGame();
        }
    }


    //when game is paused, time stops and prevents the game from continuing to run during the pause sequence
    //the pause screen is turned on
    public void PauseGame()
    {
        Time.timeScale = 0f;
        paused = true;
        pauseMenu.SetActive(true);
        //EventSystem.current.SetSelectedGameObject(null);
    }

    //on resume, the opposite effect occurs, time starts again and the pause screen is turned off
    public void ResumeGame()
    {
        Time.timeScale = 1f;
        paused = false;
        pauseMenu.SetActive(false);
    }

    //on selecting Quit Game, player is returned to the Main Menu
    public void MainHub()
    {
        ResumeGame();
        SceneManager.LoadScene(mainHubScene);
    }

    private void OnEnable()
    {
        // Enable the input action
        controls.Player.Enable();
    }

    private void OnDisable()
    {
        // Disable the input action
        controls.Player.Disable();
    }
}
