using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FullScreen : MonoBehaviour
{
    public Toggle fullscreenToggle;

    void Start()
    {
        // Set the initial state of the toggle to match the fullscreen state
        fullscreenToggle.isOn = Screen.fullScreen;

        // Add a listener to the toggle
        fullscreenToggle.onValueChanged.AddListener(ToggleFullscreen);
    }

    void ToggleFullscreen(bool isFullscreen)
    {
        // Set the fullscreen state to match the toggle
        Screen.fullScreen = isFullscreen;
    }
}
