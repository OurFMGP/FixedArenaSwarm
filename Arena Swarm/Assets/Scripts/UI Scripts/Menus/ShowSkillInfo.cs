using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ShowSkillInfo : MonoBehaviour
{
    public TextMeshProUGUI skillPointsText;
    private int previousSkillPoints = -1; // Initialize to a value that is not valid.
    public TextMeshProUGUI skillDescriptionText;
    public TextMeshProUGUI skillPointCostText;

    private void Start()
    {
        // Update the UI initially.
        UpdateSkillPointsUI();
    }

    private void Update()
    {
        // Check if the playerStats reference is valid.
        if (PlayerStats.Instance != null)
        {
            // Check if skillPoints have changed.
            if (previousSkillPoints != PlayerStats.Instance.skillPoints)
            {
                UpdateSkillPointsUI();
                previousSkillPoints = PlayerStats.Instance.skillPoints;
            }
        }
    }

    private void UpdateSkillPointsUI()
    {
        // Update the displayed skill points.
        skillPointsText.text = "" + PlayerStats.Instance.skillPoints;
    }

    public void ShowSelectedSkillInfo(SkillNodePassive selectedSkillNode)
    {
        skillDescriptionText.text = "Skill Description: " + selectedSkillNode.skillDescription;
        skillPointCostText.text = "Skill Point Cost: " + selectedSkillNode.skillPointCost;
    }

    public void ClearSkillInfo()
    {
        skillDescriptionText.text = "Skill Description: ";
        skillPointCostText.text = "Skill Point Cost: ";
    }
}