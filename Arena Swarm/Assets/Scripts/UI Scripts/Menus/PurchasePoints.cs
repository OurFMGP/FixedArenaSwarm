using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PurchasePoints : MonoBehaviour
{
    public TextMeshProUGUI goldText;
    private int previousGold = -1;

    // Start is called before the first frame update
    void Start()
    {
        UpdateGoldUI();
    }

    // Update is called once per frame
    void Update()
    {
        if (PlayerStats.Instance != null)
        {
            // Check if Gold has changed.
            if (previousGold != PlayerStats.Instance.gold)
            {
                UpdateGoldUI();
                previousGold = PlayerStats.Instance.gold;
            }

        }
    }

    void UpdateGoldUI()
    {
        // Update the displayed gold.
        goldText.text = "" + PlayerStats.Instance.gold;
    }

    public void PointPurchase()
    {
        if (PlayerStats.Instance.gold >= 1000)
        {
            PlayerStats.Instance.skillPoints++;
            PlayerStats.Instance.gold -= 1000;

            UpdateGoldUI();
        }
    }
}