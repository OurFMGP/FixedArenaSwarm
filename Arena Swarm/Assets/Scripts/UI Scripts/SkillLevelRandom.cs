using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillLevelRandom : MonoBehaviour
{
    [Header("References")]
    [SerializeField]
    private GunUpgrades gunUpgrades;

    [SerializeField]
    private PassiveSkillManager passiveSkillManager;

    [SerializeField]
    private ProjectileUpgradeManager projectileUpgradeManager;

    [SerializeField]
    private PermanentUpgradeManager permanentUpgradeManager;

    [SerializeField]
    private OtherUpgradeManager otherUpgradeManager;

    [SerializeField]
    private GunSystem gunSystem;

    [SerializeField]
    private ToolTipText toolTipText;

    [SerializeField]
    public GameObject menu;

    [SerializeField]
    private List<RawImage> button1Stars = new List<RawImage>();

    [SerializeField]
    private List<RawImage> button2Stars = new List<RawImage>();

    [SerializeField]
    private List<RawImage> button3Stars = new List<RawImage>();

    [SerializeField]
    private List<GameObject> button1GUI = new List<GameObject>();

    [SerializeField]
    private List<GameObject> button1GUIWeapons = new List<GameObject>();

    [SerializeField]
    private List<GameObject> button2GUI = new List<GameObject>();

    [SerializeField]
    private List<GameObject> button2GUIWeapons = new List<GameObject>();

    [SerializeField]
    private List<GameObject> button3GUI = new List<GameObject>();

    [SerializeField]
    private List<GameObject> button3GUIWeapons = new List<GameObject>();

    [Header("Sort")]
    public int gamePoints;

    [SerializeField]
    private List<string> passiveSkills = new List<string>();

    [SerializeField]
    private int passiveSkillCap;

    [SerializeField]
    private List<string> activeSkills = new List<string>();

    [SerializeField]
    private int activeSkillCap;

    [SerializeField]
    private List<string> randomlyChosenSkills = new List<string>();

    [SerializeField]
    private List<string> inUseSkills = new List<string>();

    [SerializeField]
    private List<string> maxedSkills = new List<string>();

    [SerializeField]
    private int loopNum = 0;

    [SerializeField]
    private int maxLoopNum = 3;

    private int randomPassiveSkillNumber;

    private int randomActiveSkillNumber;

    private int randomInUseSkillNumber;

    public string skillOne;

    private int skillOneSort;

    public int skillOneUpgradeLevel;

    public string skillTwo;

    private int skillTwoSort;

    public int skillTwoUpgradeLevel;

    public string skillThree;

    private int skillThreeSort;

    public int skillThreeUpgradeLevel;

    private bool skillInUse;

    private bool maxedOut;

    private bool running;

    //We start by giving the player a free gamePoint and call StartSkillSelect
    private void Start()
    {
        gamePoints += 1;

        StartSkillSelect();
    }

    //If we level up this gets called which adds a gamePoint. if we have mutiple gamePoints and running is true we dont want to call StartSkillSelect as its not ready for us yet
    public void AddSkillPoints(int addedSkillPoints)
    {
        gamePoints += addedSkillPoints;

        if (!running)
        {
            StartSkillSelect();
        }
    }

    //We activate our skill select menu. if we have reached the passive and active skill cap and in use skills are maxed out as well, we make maxed out to be true which forces closes the
    //menu. otherwise providing wwe have gamePoints we start the Coroutine RandomSkillSort
    private void StartSkillSelect()
    {
        toolTipText.ButtonHide();

        if (!running)
        {
            running = true;

            SetMenu(true);
        }

        if (inUseSkills.Count == 0 && passiveSkillCap == 0 && activeSkillCap == 0)
        {
            skillOne = "Empty1";

            maxedOut = true;
        }

        if (!maxedOut && gamePoints > 0)
        {
            randomlyChosenSkills.Clear();

            loopNum = 1;

            StartCoroutine(RandomSkillSort());
        }
        else
        {
            SetMenu(false);
        }
    }

    //We have a switch to turn off or on the menu which also pauses and unpauses the game
    public void SetMenu(bool on)
    {
        if (on)
        {
            menu.SetActive(true);

            PlayerStats.Instance.paused = true;
        }
        else
        {
            menu.SetActive(false);

            PlayerStats.Instance.paused = false;

            running = false;
        }
    }
    
    //We run a loop for as long as its reahced maxLoop and get a random number. Depending on this number we get a skill from a random pool of skills providing we havent reached its cap,
    //otherwise we get a new number. Depending on the number we get a random skill out of the pool and add it to randomlyChosenSkills. Depending on the loop number is where we add the
    //skill on the GUI position. If any skills so happen to repeat we remove it from our randomly chosen list and repead the loop again other we increment the loop. If we are in our last
    //3 or less skills and all passives as well as active skills caps are reached we call LastSetSkills. We set our Upgrade stars and translate all skills chosen (similar to skill select
    //check for more info), this so we can get the upgrade level of that skill for the upgrades stars and to add tool tip information for that skill. 
    private IEnumerator RandomSkillSort()
    {
        while (loopNum <= maxLoopNum)
        {
            int randomNum = Random.Range(1, 4);

            if (randomNum == 1 && passiveSkillCap == 0)
            {
                randomNum = 2;
            }

            if (randomNum == 2 && activeSkillCap == 0)
            {
                randomNum = 3;
            }

            if (randomNum == 1)
            {
                randomPassiveSkillNumber = Random.Range(0, passiveSkills.Count);

                randomlyChosenSkills.Add(passiveSkills[randomPassiveSkillNumber]);

                if (loopNum == 1)
                {
                    skillOne = randomlyChosenSkills[0];

                    skillOneSort = 1;
                }
                else if (loopNum == 2)
                {
                    skillTwo = randomlyChosenSkills[1];

                    skillTwoSort = 1;
                }
                else
                {
                    skillThree = randomlyChosenSkills[2];

                    skillThreeSort = 1;
                }
            }
            else if (randomNum == 2)
            {
                randomActiveSkillNumber = Random.Range(0, activeSkills.Count);

                randomlyChosenSkills.Add(activeSkills[randomActiveSkillNumber]);

                if (loopNum == 1)
                {
                    skillOne = randomlyChosenSkills[0];

                    skillOneSort = 2;
                }
                else if (loopNum == 2)
                {
                    skillTwo = randomlyChosenSkills[1];

                    skillTwoSort = 2;
                }
                else
                {
                    skillThree = randomlyChosenSkills[2];

                    skillThreeSort = 2;
                }
            }
            else if (randomNum == 3 && inUseSkills.Count > 0)
            {
                if (passiveSkillCap == 0 && activeSkillCap == 0 && inUseSkills.Count <= 3)
                {
                    LastSetSkills();
                }
                else
                {
                    randomInUseSkillNumber = Random.Range(0, inUseSkills.Count);

                    randomlyChosenSkills.Add(inUseSkills[randomInUseSkillNumber]);

                    if (loopNum == 1)
                    {
                        skillOne = randomlyChosenSkills[0];

                        skillOneSort = 3;
                    }
                    else if (loopNum == 2)
                    {
                        skillTwo = randomlyChosenSkills[1];

                        skillTwoSort = 3;
                    }
                    else
                    {
                        skillThree = randomlyChosenSkills[2];

                        skillThreeSort = 3;
                    }
                }              
            }

            if (skillOne == skillTwo || skillOne == skillThree || skillTwo == skillThree)
            {
                int last = randomlyChosenSkills.Count - 1;

                randomlyChosenSkills.RemoveAt(last);

                yield return null;
            }
            else
            {             
                loopNum++;
            }
        }

        toolTipText.currentTexts.Clear();

        SetUpgradeStars();

        foreach (GameObject guiSkill in button1GUI)
        {
            guiSkill.SetActive(false);
        }

        foreach (GameObject guiSkill in button1GUIWeapons)
        {
            guiSkill.SetActive(false);
        }

        foreach (GameObject guiSkill in button2GUI)
        {
            guiSkill.SetActive(false);
        }

        foreach (GameObject guiSkill in button2GUIWeapons)
        {
            guiSkill.SetActive(false);
        }

        foreach (GameObject guiSkill in button3GUI)
        {
            guiSkill.SetActive(false);
        }

        foreach (GameObject guiSkill in button3GUIWeapons)
        {
            guiSkill.SetActive(false);
        }

        if (skillOneSort == 1)
        {
            TranslatePassiveSkill(skillOne, false, 1);
        }
        else if (skillOneSort == 2)
        {
            TranslateActiveSkill(skillOne, false, 1);
        }
        else if (skillOneSort == 3)
        {
            //We check weapon like this as its in a category in its own and we do all the usual checks for other skills here as well
            if (skillOne == "Weapon")
            {
                int skillLevel = gunUpgrades.upgradeLevel;

                skillOneUpgradeLevel = skillLevel;

                foreach (RawImage star in button1Stars)
                {
                    star.gameObject.SetActive(true);
                }

                if (skillLevel < 5)
                {
                    button1Stars[5].gameObject.SetActive(false);
                }
                else
                {
                    button1Stars[5].color = Color.magenta;
                }

                for (int i = 0; i < skillOneUpgradeLevel; i++)
                {
                    button1Stars[i].color = Color.yellow;
                }

                GunGUIToolTip(skillLevel);

                if (gunUpgrades.currentGun == "Shotgun")
                {
                    button1GUIWeapons[0].SetActive(true);
                }
                else if (gunUpgrades.currentGun == "Sniper")
                {
                    button1GUIWeapons[1].SetActive(true);
                }
                else if (gunUpgrades.currentGun == "Deagle")
                {
                    button1GUIWeapons[2].SetActive(true);
                }
                else
                {
                    button1GUIWeapons[3].SetActive(true); ;
                }
            }
            else
            {
                TranslatePassiveSkill(skillOne, false, 1);

                TranslateActiveSkill(skillOne, false, 1);
            }
        }

        if (skillTwoSort == 1)
        {
            TranslatePassiveSkill(skillTwo, false, 2);
        }
        else if (skillTwoSort == 2)
        {
            TranslateActiveSkill(skillTwo, false, 2);
        }
        else if (skillTwoSort == 3)
        {
            //We check weapon like this as its in a category in its own and we do all the usual checks for other skills here as well
            if (skillTwo == "Weapon")
            {
                int skillLevel = gunUpgrades.upgradeLevel;

                skillTwoUpgradeLevel = skillLevel;

                foreach (RawImage star in button2Stars)
                {
                    star.gameObject.SetActive(true);
                }

                if (skillLevel < 5)
                {
                    button2Stars[5].gameObject.SetActive(false);
                }
                else
                {
                    button2Stars[5].color = Color.magenta;
                }

                for (int i = 0; i < skillTwoUpgradeLevel; i++)
                {
                    button2Stars[i].color = Color.yellow;
                }

                GunGUIToolTip(skillLevel);

                if (gunUpgrades.currentGun == "Shotgun")
                {
                    button2GUIWeapons[0].SetActive(true);
                }
                else if (gunUpgrades.currentGun == "Sniper")
                {
                    button2GUIWeapons[1].SetActive(true);
                }
                else if (gunUpgrades.currentGun == "Deagle")
                {
                    button2GUIWeapons[2].SetActive(true);
                }
                else
                {
                    button2GUIWeapons[3].SetActive(true); ;
                }
            }
            else
            {
                TranslatePassiveSkill(skillTwo, false, 2);

                TranslateActiveSkill(skillTwo, false, 2);
            }
        }

        if (skillThreeSort == 1)
        {
            TranslatePassiveSkill(skillThree, false, 3);
        }
        else if (skillThreeSort == 2)
        {
            TranslateActiveSkill(skillThree, false, 3);
        }
        else if (skillThreeSort == 3)
        {
            //We check weapon like this as its in a category in its own and we do all the usual checks for other skills here as well
            if (skillThree == "Weapon")
            {
                int skillLevel = gunUpgrades.upgradeLevel;

                skillThreeUpgradeLevel = skillLevel;

                foreach (RawImage star in button3Stars)
                {
                    star.gameObject.SetActive(true);
                }

                if (skillLevel < 5)
                {
                    button3Stars[5].gameObject.SetActive(false);
                }
                else
                {
                    button3Stars[5].color = Color.magenta;
                }

                for (int i = 0; i < skillThreeUpgradeLevel; i++)
                {
                    button3Stars[i].color = Color.yellow;
                }

                GunGUIToolTip(skillLevel);

                if (gunUpgrades.currentGun == "Shotgun")
                {
                    button3GUIWeapons[0].SetActive(true);
                }
                else if (gunUpgrades.currentGun == "Sniper")
                {
                    button3GUIWeapons[1].SetActive(true);
                }
                else if (gunUpgrades.currentGun == "Deagle")
                {
                    button3GUIWeapons[2].SetActive(true);
                }
                else
                {
                    button3GUIWeapons[3].SetActive(true); ;
                }
            }
            else
            {
                TranslatePassiveSkill(skillThree, false, 3);

                TranslateActiveSkill(skillThree, false, 3);
            }
        }

        //We need this delay as we want to reset our onEnter Ipointer check OnHover Script
        Invoke("DelayShowCubes", 0.5f);

        yield return null;
    }

    private void DelayShowCubes()
    {
        toolTipText.ButtonShow(skillOne, skillTwo, skillThree);
    }

    //Since the upgrades differ for each weapon we add the specific gun text to current text here
    private void GunGUIToolTip(int skillLevel)
    {
        if (gunSystem.weaponNumber == 1)
        {
            toolTipText.currentTexts.Add(toolTipText.shotgunTexts[skillLevel]);
        }
        else if (gunSystem.weaponNumber == 2)
        {
            toolTipText.currentTexts.Add(toolTipText.sniperTexts[skillLevel]);
        }
        else if (gunSystem.weaponNumber == 3)
        {
            toolTipText.currentTexts.Add(toolTipText.deagleTexts[skillLevel]);
        }
        else
        {
            toolTipText.currentTexts.Add(toolTipText.m4Texts[skillLevel]);
        }
    }

    //Simply resets all stars to defualt 
    private void SetUpgradeStars()
    {
        foreach (RawImage star in button1Stars)
        {
            star.color = Color.white;

            star.gameObject.SetActive(false);
        }

        foreach (RawImage star in button2Stars)
        {
            star.color = Color.white;

            star.gameObject.SetActive(false);
        }

        foreach (RawImage star in button3Stars)
        {
            star.color = Color.white;

            star.gameObject.SetActive(false);
        }
    }

    //For each button (where the stars are located outside of script) we check if this skill is already chosen (0 and -1 upgrade level for passive and active skills respectivly means not
    //chosen) if so we set all stars to be active. For the active skills we have a hidden level 6 star which we only show if that skill has reached level 5 otherwise we hide it as well as
    //if the skill is passive as they do not have one. We then set the stars colour to yellow for each upgrade level.
    private void ShowUpgradeStars(int buttonLink, bool passive)
    {
        if (buttonLink == 1)
        {
            if (passive && skillOneUpgradeLevel > 0 || !passive && skillOneUpgradeLevel > -1)
            {
                foreach (RawImage star in button1Stars)
                {
                    star.gameObject.SetActive(true);
                }

                if (passive || !passive && skillOneUpgradeLevel < 5)
                {
                    button1Stars[5].gameObject.SetActive(false);
                }
                else if (!passive && skillOneUpgradeLevel == 5)
                {
                    button1Stars[5].color = Color.magenta;
                }

                for (int i = 0; i < skillOneUpgradeLevel; i++)
                {
                    button1Stars[i].color = Color.yellow;
                }
            }         
        }
        else if (buttonLink == 2)
        {
            if (passive && skillTwoUpgradeLevel > 0 || !passive && skillTwoUpgradeLevel > -1)
            {
                foreach (RawImage star in button2Stars)
                {
                    star.gameObject.SetActive(true);
                }

                if (passive || !passive && skillTwoUpgradeLevel < 5)
                {
                    button2Stars[5].gameObject.SetActive(false);
                }
                else if (!passive && skillTwoUpgradeLevel == 5)
                {
                    button2Stars[5].color = Color.magenta;
                }

                for (int i = 0; i < skillTwoUpgradeLevel; i++)
                {
                    button2Stars[i].color = Color.yellow;
                }
            }
        }
        else
        {
            if (passive && skillThreeUpgradeLevel > 0 || !passive && skillThreeUpgradeLevel > -1)
            {
                foreach (RawImage star in button3Stars)
                {
                    star.gameObject.SetActive(true);
                }

                if (passive || !passive && skillThreeUpgradeLevel < 5)
                {
                    button3Stars[5].gameObject.SetActive(false);
                }
                else if (!passive && skillThreeUpgradeLevel == 5)
                {
                    button3Stars[5].color = Color.magenta;
                }

                for (int i = 0; i < skillThreeUpgradeLevel; i++)
                {
                    button3Stars[i].color = Color.yellow;
                }
            }
        }
    }

    //Depending on how many of the inUseSkills left we manually set them into each position on the GUI. If we only have 2 active skills we hide the 3rd choice button and if we only have
    //One skill left we also hide the 2nd choice button. We hide these buttons by typing in empty with its number, We then end the loop.
    private void LastSetSkills()
    {
        if (inUseSkills.Count == 3)
        {
            randomlyChosenSkills.Add(inUseSkills[0]);

            skillOne = randomlyChosenSkills[0];

            skillOneSort = 3;

            randomlyChosenSkills.Add(inUseSkills[1]);

            skillTwo = randomlyChosenSkills[1];

            skillTwoSort = 3;

            randomlyChosenSkills.Add(inUseSkills[2]);

            skillThree = randomlyChosenSkills[2];

            skillThreeSort = 3;
        }
        else if (inUseSkills.Count == 2)
        {
            skillThree = "Empty3";

            maxLoopNum = 2;

            randomlyChosenSkills.Add(inUseSkills[0]);

            skillOne = randomlyChosenSkills[0];

            skillOneSort = 3;

            randomlyChosenSkills.Add(inUseSkills[1]);

            skillTwo = randomlyChosenSkills[1];

            skillTwoSort = 3;
        }
        else
        {
            skillTwo = "Empty2";

            maxLoopNum = 1;

            randomlyChosenSkills.Add(inUseSkills[0]);

            skillOne = randomlyChosenSkills[0];

            skillOneSort = 3;
        }

        loopNum = maxLoopNum;
    }

    //SkillOneSelect up to three all operate the same way the only differnt is the stored skill contained in each position. We first have to translate the skill string into something
    //the computer can understand so depending on the skill sort number we enter different translate methods, while if the skill sort is 3 an already chosen skill we check if its the
    //initial skill which is the weapon. If it is we call the gun upgrade chooser and if we and now max level with that weapon we remove it from in use skills and add it to maxed ones.
    //If its not a weapon but an in use skill we check both passive and active skill pools. We then call CheckSkillPoints.
    public void SkillOneSelect()
    {
        if (skillOneSort == 1)
        {
            skillInUse = false;

            TranslatePassiveSkill(skillOne, true, 1);
        }
        else if (skillOneSort == 2)
        {
            skillInUse = false;

            TranslateActiveSkill(skillOne, true, 1);
        }
        else
        {
            if (skillOne == "Weapon")
            {
                int skillLevel = gunUpgrades.upgradeLevel;

                gunUpgrades.UpgradeChooser();

                if(skillLevel >= 5)
                {
                    inUseSkills.Remove(skillOne);

                    maxedSkills.Add(skillOne);
                }               
            }
            else
            {
                skillInUse = true;

                TranslatePassiveSkill(skillOne, true, 1);

                TranslateActiveSkill(skillOne, true, 1);
            }
        }

        CheckSkillPoints();
    }

    public void SkillTwoSelect()
    {
        if (skillTwoSort == 1)
        {
            skillInUse = false;

            TranslatePassiveSkill(skillTwo, true, 2);
        }
        else if (skillTwoSort == 2)
        {
            skillInUse = false;

            TranslateActiveSkill(skillTwo, true, 2);
        }
        else
        {
            if (skillTwo == "Weapon")
            {
                int skillLevel = gunUpgrades.upgradeLevel;

                gunUpgrades.UpgradeChooser();

                if (skillLevel >= 5)
                {
                    inUseSkills.Remove(skillTwo);

                    maxedSkills.Add(skillTwo);
                }
            }
            else
            {
                skillInUse = true;

                TranslatePassiveSkill(skillTwo, true, 2);

                TranslateActiveSkill(skillTwo, true, 2);
            }
        }

        CheckSkillPoints();
    }

    public void SkillThreeSelect()
    {
        if (skillThreeSort == 1)
        {
            skillInUse = false;

            TranslatePassiveSkill(skillThree, true, 3);
        }
        else if (skillThreeSort == 2)
        {
            skillInUse = false;

            TranslateActiveSkill(skillThree, true, 3);
        }
        else
        {
            if (skillThree == "Weapon")
            {
                int skillLevel = gunUpgrades.upgradeLevel;

                gunUpgrades.UpgradeChooser();

                if (skillLevel >= 5)
                {
                    inUseSkills.Remove(skillThree);

                    maxedSkills.Add(skillThree);
                }
            }
            else
            {
                skillInUse = true;

                TranslatePassiveSkill(skillThree, true, 3);

                TranslateActiveSkill(skillThree, true, 3);
            }
        }

        CheckSkillPoints();
    }

    //Based on the skill selected name we match it up with all known skills, if it does match we upgrade that skill otherwise or add its tooltip if nethier we keep checking the skills.
    //if we are upgrading and this skill is already in use we check if its max level as if so we remove it otherwise we remove this skill from the current pool its in and then add it to
    //in use skills. If we are not upgrading we make sure that the skillLevel has been changed as a fail safe. If the number has been changed we check what button its linked to then
    //record the skills level as well as set our colour starts using that information.
    private void TranslatePassiveSkill(string skill, bool upgrading, int buttonLinkedTo)
    {
        int skillLevel = -99;

        if (skill == "Health")
        {
            skillLevel = passiveSkillManager.healthLevel;

            if (upgrading)
            {
                passiveSkillManager.HealthIncrease();
            }       
            else
            {
                toolTipText.currentTexts.Add(toolTipText.passiveTexts[0]);

                if (buttonLinkedTo == 1)
                {
                    button1GUI[0].SetActive(true);
                }
                else if (buttonLinkedTo == 2)
                {
                    button2GUI[0].SetActive(true);
                }
                else if (buttonLinkedTo == 3)
                {
                    button3GUI[0].SetActive(true);
                }
            }
        }
        else if (skill == "Speed")
        {
            skillLevel = passiveSkillManager.speedLevel;

            if (upgrading)
            {
                passiveSkillManager.SpeedIncrease();
            }
            else
            {
                toolTipText.currentTexts.Add(toolTipText.passiveTexts[1]);

                if (buttonLinkedTo == 1)
                {
                    button1GUI[1].SetActive(true);
                }
                else if (buttonLinkedTo == 2)
                {
                    button2GUI[1].SetActive(true);
                }
                else if (buttonLinkedTo == 3)
                {
                    button3GUI[1].SetActive(true);
                }
            }
        }
        else if (skill == "Fire Rate")
        {
            skillLevel = passiveSkillManager.fireRateLevel;

            if (upgrading)
            {
                passiveSkillManager.FireRateIncrease();
            }
            else
            {
                toolTipText.currentTexts.Add(toolTipText.passiveTexts[2]);

                if (buttonLinkedTo == 1)
                {
                    button1GUI[2].SetActive(true);
                }
                else if (buttonLinkedTo == 2)
                {
                    button2GUI[2].SetActive(true);
                }
                else if (buttonLinkedTo == 3)
                {
                    button3GUI[2].SetActive(true);
                }
            }
        }
        else if (skill == "Damage")
        {
            skillLevel = passiveSkillManager.damageLevel;

            if (upgrading)
            {
                passiveSkillManager.DamageIncrease();
            }
            else
            {
                toolTipText.currentTexts.Add(toolTipText.passiveTexts[3]);

                if (buttonLinkedTo == 1)
                {
                    button1GUI[3].SetActive(true);
                }
                else if (buttonLinkedTo == 2)
                {
                    button2GUI[3].SetActive(true);
                }
                else if (buttonLinkedTo == 3)
                {
                    button3GUI[3].SetActive(true);
                }
            }
        }
        else if (skill == "Explosive Radius")
        {
            skillLevel = passiveSkillManager.explosiveRadiusLevel;

            if (upgrading)
            {
                passiveSkillManager.IncreasedExplosiveRadius();
            }
            else
            {
                toolTipText.currentTexts.Add(toolTipText.passiveTexts[4]);

                if (buttonLinkedTo == 1)
                {
                    button1GUI[4].SetActive(true);
                }
                else if (buttonLinkedTo == 2)
                {
                    button2GUI[4].SetActive(true);
                }
                else if (buttonLinkedTo == 3)
                {
                    button3GUI[4].SetActive(true);
                }
            }
        }
        else if (skill == "Boss Damage")
        {
            skillLevel = passiveSkillManager.bossDamageLevel;

            if (upgrading)
            {
                passiveSkillManager.IncreasedBossDamage();
            }
            else
            {
                toolTipText.currentTexts.Add(toolTipText.passiveTexts[5]);

                if (buttonLinkedTo == 1)
                {
                    button1GUI[5].SetActive(true);
                }
                else if (buttonLinkedTo == 2)
                {
                    button2GUI[5].SetActive(true);
                }
                else if (buttonLinkedTo == 3)
                {
                    button3GUI[5].SetActive(true);
                }
            }
        }
        else if (skill == "Cooldown")
        {
            skillLevel = passiveSkillManager.cooldownLevel;

            if (upgrading)
            {
                passiveSkillManager.DecreasedCooldown();
            }
            else
            {
                toolTipText.currentTexts.Add(toolTipText.passiveTexts[6]);

                if (buttonLinkedTo == 1)
                {
                    button1GUI[6].SetActive(true);
                }
                else if (buttonLinkedTo == 2)
                {
                    button2GUI[6].SetActive(true);
                }
                else if (buttonLinkedTo == 3)
                {
                    button3GUI[6].SetActive(true);
                }
            }
        }
        else if (skill == "Exp")
        {
            skillLevel = passiveSkillManager.expUpgradeLevel;

            if (upgrading)
            {
                passiveSkillManager.IncreasedExp();
            }
            else
            {
                toolTipText.currentTexts.Add(toolTipText.passiveTexts[7]);

                if (buttonLinkedTo == 1)
                {
                    button1GUI[7].SetActive(true);
                }
                else if (buttonLinkedTo == 2)
                {
                    button2GUI[7].SetActive(true);
                }
                else if (buttonLinkedTo == 3)
                {
                    button3GUI[7].SetActive(true);
                }
            }
        }
        else if (skill == "Spawn Damage")
        {
            skillLevel = passiveSkillManager.spawnDamageLevel;

            if (upgrading)
            {
                passiveSkillManager.SpawnDamageIncreased();
            }
            else
            {
                toolTipText.currentTexts.Add(toolTipText.passiveTexts[8]);

                if (buttonLinkedTo == 1)
                {
                    button1GUI[8].SetActive(true);
                }
                else if (buttonLinkedTo == 2)
                {
                    button2GUI[8].SetActive(true);
                }
                else if (buttonLinkedTo == 3)
                {
                    button3GUI[8].SetActive(true);
                }
            }
        }
        else if (skill == "Gold")
        {
            skillLevel = passiveSkillManager.goldLevel;

            if (upgrading)
            {
                passiveSkillManager.IncreasedGold();
            }
            else
            {
                toolTipText.currentTexts.Add(toolTipText.passiveTexts[9]);

                if (buttonLinkedTo == 1)
                {
                    button1GUI[9].SetActive(true);
                }
                else if (buttonLinkedTo == 2)
                {
                    button2GUI[9].SetActive(true);
                }
                else if (buttonLinkedTo == 3)
                {
                    button3GUI[9].SetActive(true);
                }
            }
        }

        if (upgrading)
        {
            if (!skillInUse)
            {
                passiveSkills.Remove(skill);

                inUseSkills.Add(skill);

                passiveSkillCap--;
            }
            else
            {
                if (skillLevel >= 4)
                {
                    inUseSkills.Remove(skill);

                    maxedSkills.Add(skill);
                }
            }
        }
        else
        {
            if (skillLevel != -99)
            {
                if (buttonLinkedTo == 1)
                {
                    skillOneUpgradeLevel = skillLevel;

                    ShowUpgradeStars(1, true);
                }
                else if (buttonLinkedTo == 2)
                {
                    skillTwoUpgradeLevel = skillLevel;

                    ShowUpgradeStars(2, true);
                }
                else if (buttonLinkedTo == 3)
                {
                    skillThreeUpgradeLevel = skillLevel;

                    ShowUpgradeStars(3, true);
                }
            }
        }
    }

    private void TranslateActiveSkill(string skill, bool upgrading, int buttonLinkedTo)
    {
        int skillLevel = -99;

        if (skill == "RPG")
        {
            skillLevel = projectileUpgradeManager.rpgLevel;

            if (upgrading)
            {
                projectileUpgradeManager.RPGUpgrades();
            }
            else
            {
                toolTipText.currentTexts.Add(toolTipText.rpgTexts[skillLevel + 1]);

                if (buttonLinkedTo == 1)
                {
                    button1GUI[10].SetActive(true);
                }
                else if (buttonLinkedTo == 2)
                {
                    button2GUI[10].SetActive(true);
                }
                else if (buttonLinkedTo == 3)
                {
                    button3GUI[10].SetActive(true);
                }
            }
        }
        else if (skill == "Axe")
        {
            skillLevel = projectileUpgradeManager.axeLevel;

            if (upgrading)
            {
                projectileUpgradeManager.ThrowingAxeUpgrades();
            }
            else
            {
                toolTipText.currentTexts.Add(toolTipText.axeTexts[skillLevel + 1]);

                if (buttonLinkedTo == 1)
                {
                    button1GUI[11].SetActive(true);
                }
                else if (buttonLinkedTo == 2)
                {
                    button2GUI[11].SetActive(true);
                }
                else if (buttonLinkedTo == 3)
                {
                    button3GUI[11].SetActive(true);
                }
            }
        }
        else if (skill == "Crossbow")
        {
            skillLevel = projectileUpgradeManager.crossLevel;

            if (upgrading)
            {
                projectileUpgradeManager.CrossBowUpgrades();
            }
            else
            {
                toolTipText.currentTexts.Add(toolTipText.crossbowTexts[skillLevel + 1]);

                if (buttonLinkedTo == 1)
                {
                    button1GUI[12].SetActive(true);
                }
                else if (buttonLinkedTo == 2)
                {
                    button2GUI[12].SetActive(true);
                }
                else if (buttonLinkedTo == 3)
                {
                    button3GUI[12].SetActive(true);
                }
            }
        }
        else if (skill == "Frisbee")
        {
            skillLevel = projectileUpgradeManager.frisLevel;

            if (upgrading)
            {
                projectileUpgradeManager.FrisbeeUpgrades();
            }
            else
            {
                toolTipText.currentTexts.Add(toolTipText.frisbeeTexts[skillLevel + 1]);

                if (buttonLinkedTo == 1)
                {
                    button1GUI[13].SetActive(true);
                }
                else if (buttonLinkedTo == 2)
                {
                    button2GUI[13].SetActive(true);
                }
                else if (buttonLinkedTo == 3)
                {
                    button3GUI[13].SetActive(true);
                }
            }
        }
        else if (skill == "Turret")
        {
            skillLevel = projectileUpgradeManager.turretLevel;

            if (upgrading)
            {
                projectileUpgradeManager.TurretUpgrades();
            }
            else
            {
                toolTipText.currentTexts.Add(toolTipText.turretTexts[skillLevel + 1]);

                if (buttonLinkedTo == 1)
                {
                    button1GUI[14].SetActive(true);
                }
                else if (buttonLinkedTo == 2)
                {
                    button2GUI[14].SetActive(true);
                }
                else if (buttonLinkedTo == 3)
                {
                    button3GUI[14].SetActive(true);
                }
            }
        }
        else if (skill == "Shield")
        {
            skillLevel = permanentUpgradeManager.sheLevel;

            if (upgrading)
            {
                permanentUpgradeManager.SheildUpgrades();
            }
            else
            {
                toolTipText.currentTexts.Add(toolTipText.shieldTexts[skillLevel + 1]);

                if (buttonLinkedTo == 1)
                {
                    button1GUI[15].SetActive(true);
                }
                else if (buttonLinkedTo == 2)
                {
                    button2GUI[15].SetActive(true);
                }
                else if (buttonLinkedTo == 3)
                {
                    button3GUI[15].SetActive(true);
                }
            }
        }
        else if (skill == "Damage Aura")
        {
            skillLevel = permanentUpgradeManager.auraLevel;

            if (upgrading)
            {
                permanentUpgradeManager.DamageAuraUpgrade();
            }
            else
            {
                toolTipText.currentTexts.Add(toolTipText.damageAuraTexts[skillLevel + 1]);

                if (buttonLinkedTo == 1)
                {
                    button1GUI[16].SetActive(true);
                }
                else if (buttonLinkedTo == 2)
                {
                    button2GUI[16].SetActive(true);
                }
                else if (buttonLinkedTo == 3)
                {
                    button3GUI[16].SetActive(true);
                }
            }
        }
        else if (skill == "Flame Walk")
        {
            skillLevel = otherUpgradeManager.flameWalkLevel;

            if (upgrading)
            {
                otherUpgradeManager.FlameWalkpgrades();
            }
            else
            {
                toolTipText.currentTexts.Add(toolTipText.flameWalkTexts[skillLevel + 1]);

                if (buttonLinkedTo == 1)
                {
                    button1GUI[17].SetActive(true);
                }
                else if (buttonLinkedTo == 2)
                {
                    button2GUI[17].SetActive(true);
                }
                else if (buttonLinkedTo == 3)
                {
                    button3GUI[17].SetActive(true);
                }
            }
        }
        else if (skill == "Enemy Explosion")
        {
            skillLevel = otherUpgradeManager.enemyExplosionLevel;

            if (upgrading)
            {
                otherUpgradeManager.EnemyExplosionUpgrades();
            }
            else
            {
                toolTipText.currentTexts.Add(toolTipText.enemyExplosionTexts[skillLevel + 1]);

                if (buttonLinkedTo == 1)
                {
                    button1GUI[18].SetActive(true);
                }
                else if (buttonLinkedTo == 2)
                {
                    button2GUI[18].SetActive(true);
                }
                else if (buttonLinkedTo == 3)
                {
                    button3GUI[18].SetActive(true);
                }
            }
        }
        else if (skill == "Javelin")
        {
            skillLevel = otherUpgradeManager.javalinLevel;

            if (upgrading)
            {
                otherUpgradeManager.JavalinUpgrades();
            }
            else
            {
                toolTipText.currentTexts.Add(toolTipText.javelinTexts[skillLevel + 1]);

                if (buttonLinkedTo == 1)
                {
                    button1GUI[19].SetActive(true);
                }
                else if (buttonLinkedTo == 2)
                {
                    button2GUI[19].SetActive(true);
                }
                else if (buttonLinkedTo == 3)
                {
                    button3GUI[19].SetActive(true);
                }
            }
        }

        if (upgrading)
        {
            if (!skillInUse)
            {
                activeSkills.Remove(skill);

                inUseSkills.Add(skill);

                activeSkillCap--;
            }
            else
            {
                if (skillLevel >= 5)
                {
                    inUseSkills.Remove(skill);

                    maxedSkills.Add(skill);
                }
            }
        }
        else
        {
            if (skillLevel != -99)
            {
                if (buttonLinkedTo == 1)
                {
                    skillOneUpgradeLevel = skillLevel;

                    ShowUpgradeStars(1, false);
                }
                else if (buttonLinkedTo == 2)
                {
                    skillTwoUpgradeLevel = skillLevel;

                    ShowUpgradeStars(2, false);
                }
                else if (buttonLinkedTo == 3)
                {
                    skillThreeUpgradeLevel = skillLevel;

                    ShowUpgradeStars(3, false);
                }
            }          
        }
    }

    //We remove a gamePoint and if we still have points we call startSkillSelect again otherwise we hide the menu
    private void CheckSkillPoints()
    {
        gamePoints--;

        if (gamePoints > 0)
        {
            StartSkillSelect();
        }
        else
        {
            SetMenu(false);
        }
    }
}
