using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//We inherit from bulletManager
public class ThrowingAxe : BulletManager
{
    [Header("General")]
    [SerializeField]
    private GameObject child;

    [SerializeField]
    private float rotateSpeed;

    [Header("Level 3")]
    public bool canRebound;

    public int reboundAmount;

    [SerializeField]
    private int originalReboundAmount;

    [SerializeField]
    private int startingPierce;

    [HideInInspector]
    public GameObject player;

    private bool doOnce;

    //We set the size of this axe and its starting pierce
    private void Start()
    {
        transform.localScale = size;

        startingPierce = pierce;
    }

    //Similer to the original except we call reboundReset if we can rebound and reboundAmount is more than 0
    protected override void LifeCounter()
    {
        child.transform.Rotate(0, 0, rotateSpeed * Time.deltaTime);

        lifeTimeCounter += Time.deltaTime;

        if (lifeTimeCounter >= lifeTimeWait)
        {
            if (!canRebound || canRebound && reboundAmount == 0)
            {
                Destroy(gameObject);
            }
            else
            {
                ReboundReset();
            }
        }

        GetComponent<Rigidbody2D>().velocity = -transform.right * speed;
    }

    //Similer to the original except we call reboundReset if we can rebound and pierce is 0 or less
    protected override void TriggerEnter2D(Collider2D collider)
    {
        if (collider.CompareTag("BulletCollider"))
        {
            pierce -= 1;

            collider.gameObject.SendMessageUpwards("TakeGunDamage", damage * PlayerStats.Instance.currentDamage);

            if (!canRebound && pierce <= 0 || canRebound && reboundAmount == 0 && pierce <= 0)
            {
                Destroy(gameObject);
            }
            else if (canRebound && pierce <= 0)
            {
                ReboundReset();
            }
        }
    }

    //On each rebound we add the original starting pierce and add extra time life once, we then change the axes forward to face the player (so it comes back to us)
    private void ReboundReset()
    {
        pierce = startingPierce;

        lifeTimeCounter = 0;

        if (!doOnce)
        {
            lifeTimeWait += lifeTimeWait;

            doOnce = true;
        }

        transform.right += transform.position - player.transform.position;

        reboundAmount--;
    }

    //We turn off can rebound and call bullet manager reset value
    public override void ResetValues()
    {       
        canRebound = false;

        reboundAmount = originalReboundAmount;

        base.ResetValues();
    }
}
