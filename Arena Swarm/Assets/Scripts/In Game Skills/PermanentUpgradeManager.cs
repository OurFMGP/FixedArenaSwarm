using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PermanentUpgradeManager : MonoBehaviour
{
    [Header("Sheild Upgrades")]
    [SerializeField]
    private RotateAround rotateAround;

    [SerializeField]
    private List<RevolvingSheild> revolvingSheilds = new List<RevolvingSheild>();

    public int sheLevel;

    [SerializeField]
    private float sheLevel1Speed;

    [SerializeField]
    private float sheLevel2Damage;

    [SerializeField]
    private float sheLevel2Health;

    [SerializeField]
    private float sheLevel4Damage;

    [SerializeField]
    private float sheLevel4Health;

    [SerializeField]
    private float sheLevel6Speed;

    [SerializeField]
    private float sheLevel6Damage;

    [SerializeField]
    private float sheLevel6Health;

    [SerializeField]
    private float sheLevel6RecoverTime;

    [Header("Damage Aura Upgrades")]
    [SerializeField]
    private DamageAura damageAura;

    public int auraLevel;

    [SerializeField]
    private Vector2 auraLevel1Size;

    [SerializeField]
    private Vector2 auraLevel2Size;

    [SerializeField]
    private Vector2 auraLevel3Size;

    [SerializeField]
    private Vector2 auraLevel4Size;

    [SerializeField]
    private Vector2 auraLevel5Size;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            //DamageAuraUpgrade();
        }
    }

    //when called for the first time we set this to be active as its unlocked otherwise we increase certian stats of this upgrade, key points is at level 3 and 5 where we increase
    //the amount of active shields
    public void SheildUpgrades()
    {
        if (sheLevel <= 5)
        {
            sheLevel++;

            switch (sheLevel)
            {
                case 0:
                    revolvingSheilds[0].gameObject.SetActive(true);
                    break;

                case 1:
                    rotateAround.rotateSpeed = rotateAround.rotateSpeed * (1f + sheLevel1Speed);
                    break;

                case 2:

                    foreach (RevolvingSheild sheild in revolvingSheilds)
                    {
                        sheild.sheildDamagePerSecond = sheild.sheildDamagePerSecond * (1f + sheLevel2Damage);
                    }

                    foreach (RevolvingSheild sheild in revolvingSheilds)
                    {
                        sheild.sheildMaxHealth = sheild.sheildMaxHealth * (1f + sheLevel2Health);

                        sheild.sheildCurrentHealth = sheild.sheildCurrentHealth * (1f + sheLevel2Health);
                    }

                    break;

                case 3:
                    revolvingSheilds[1].gameObject.SetActive(true);
                    break;

                case 4:

                    foreach (RevolvingSheild sheild in revolvingSheilds)
                    {
                        sheild.sheildDamagePerSecond = sheild.sheildDamagePerSecond * (1f + sheLevel4Damage);
                    }

                    foreach (RevolvingSheild sheild in revolvingSheilds)
                    {
                        sheild.sheildMaxHealth = sheild.sheildMaxHealth * (1f + sheLevel4Health);

                        sheild.sheildCurrentHealth = sheild.sheildCurrentHealth * (1f + sheLevel4Health);
                    }

                    break;

                case 5:
                    revolvingSheilds[2].gameObject.SetActive(true);
                    break;

                case 6:
                    rotateAround.rotateSpeed = rotateAround.rotateSpeed * (1f + sheLevel6Speed);

                    foreach (RevolvingSheild sheild in revolvingSheilds)
                    {
                        sheild.sheildDamagePerSecond = sheild.sheildDamagePerSecond * (1f + sheLevel6Damage);
                    }

                    foreach (RevolvingSheild sheild in revolvingSheilds)
                    {
                        sheild.sheildMaxHealth = sheild.sheildMaxHealth * (1f + sheLevel6Health);

                        sheild.sheildCurrentHealth = sheild.sheildCurrentHealth * (1f + sheLevel6Health);
                    }

                    foreach (RevolvingSheild sheild in revolvingSheilds)
                    {
                        sheild.sheildRecoverTime = sheild.sheildRecoverTime * (1f + sheLevel6RecoverTime);
                    }

                    break;

                default:
                    break;
            }
        }
    }

    //when called for the first time we set this to be active as its unlocked otherwise we increase certian stats of this upgrade, key points is at level 6 where it can now slow
    public void DamageAuraUpgrade()
    {
        if (auraLevel <= 5)
        {
            auraLevel++;

            switch (auraLevel)
            {
                case 0:
                    damageAura.gameObject.SetActive(true);
                    break;

                case 1:
                    damageAura.size += auraLevel1Size;

                    damageAura.UpdatedDamageRadius();
                    break;

                case 2:
                    damageAura.size += auraLevel2Size;

                    damageAura.UpdatedDamageRadius();
                    break;

                case 3:
                    damageAura.size += auraLevel3Size;

                    damageAura.UpdatedDamageRadius();
                    break;

                case 4:
                    damageAura.size += auraLevel4Size;

                    damageAura.UpdatedDamageRadius();
                    break;

                case 5:
                    damageAura.size += auraLevel5Size;

                    damageAura.UpdatedDamageRadius();
                    break;

                case 6:
                    damageAura.canSlow = true;
                    break;

                default:
                    break;
            }
        }
    }
}
