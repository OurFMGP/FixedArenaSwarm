using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR  
using UnityEditor;      //This paticular library gives us access to Editor keywords
#endif

public class ProjectileSkills : MonoBehaviour
{
    [Header("Target")]
    [SerializeField]
    private GameObject player;

    [SerializeField]
    private float viewCircle;        //This allows us to use decimal numbers

    [SerializeField]
    private Collider2D[] targetInCircleRadius;      //This is an array that will contain Coliiders

    [SerializeField]
    private List<Transform> biggestThreatTarget = new List<Transform>();

    [SerializeField]
    protected LayerMask targetMask;     //This allows me to choose one or more layers masks that i have created in this prodject 

    [SerializeField]
    protected LayerMask obstacleMask;       //This allows me to choose one or more layers masks that i have created in this prodject 

    private Transform target;        //This allows me to contain a Tranform

    private Vector3 directionToTarget;        //This allows me to contain a Vector3

    private float distanceToTarget;      //This allows us to use decimal numbers

    private bool doOnce;

    [Header("RPG")]
    public bool rpgUnlocked;

    [SerializeField]
    private GameObject rocket;

    public float rpgFireTime;

    [SerializeField]
    private float rpgFireCounter;

    [Header("Throwing Axe")]
    public bool axeUnlocked;

    [SerializeField]
    private GameObject axe;

    public float axeThrowTime;

    [SerializeField]
    private float axeThrowCounter;

    [Header("CrossBow")]
    public bool crossUnlocked;

    [SerializeField]
    private GameObject crossBow;

    public float crossFireTime;

    [SerializeField]
    private float crossFireCounter;

    public bool crossLevel6Multi;

    private GameObject crossBow1;

    private GameObject crossBow2;

    [SerializeField]
    private float crossMultiTilt;

    [Header("Frisbee")]
    public bool frisbeeUnlocked;

    [SerializeField]
    private GameObject frisbee;

    [SerializeField]
    private float frisbeeTossTime;

    [SerializeField]
    private float frisbeeTossCounter;

    public int frisbeeExtraCount;

    [SerializeField]
    private int frisbeeSpawnTiltMin;

    [SerializeField]
    private int frisbeeSpawnTiltMax;

    [Header("Turret")]
    public bool turretUnlocked;

    [SerializeField]
    private GameObject turret;

    public float turretSpawnTime;

    [SerializeField]
    private float turretSpawnCounter;

    //We call all these methods every frame as long as they are unlocked
    private void Update()
    {
        if (!PlayerStats.Instance.paused)
        {
            Target();

            if (rpgUnlocked)
            {
                RPG();
            }

            if (axeUnlocked)
            {
                AxeThrow();
            }

            if (crossUnlocked)
            {
                CrossBowFire();
            }

            if (frisbeeUnlocked)
            {
                FrisbeToss();
            }

            if (turretUnlocked)
            {
                TurretSpawn();
            }
        }
    }

    //We make an overlap circle based on the view circle. all targets that we see that are not behind the obstacle mask we then add to the biggestThreat list, if returned from
    //findtarget method
    private void Target()
    {
        biggestThreatTarget.Clear();

        targetInCircleRadius = Physics2D.OverlapCircleAll(player.transform.position, viewCircle, targetMask);       //We create a sphere around us to detect anyone inside of it based on targetMask      

        for (int i = 0; i < targetInCircleRadius.Length; i++)
        {
            target = targetInCircleRadius[i].transform;
            //This gives us the direction of our target we normalize so we can keep the same direction but the max length is 1
            directionToTarget = (target.position - player.transform.position).normalized;
            distanceToTarget = Vector3.Distance(player.transform.position, target.position);        //This gives us the distance between us and our target

            //We use a raycast pointed towards our target started from our location to see if we are behind any obstacles based on the obstacleMask
            if (!Physics2D.Raycast(player.transform.position, directionToTarget, distanceToTarget, obstacleMask) && doOnce == false)
            {
                biggestThreatTarget.Clear();

                biggestThreatTarget.Add(FindTarget());
            }
        }
    }

    //Our target is based on the closest enemy to the player which is constantly checked. we take priorioty over the boss class enemies as long as we see them in our view circle
    private Transform FindTarget()
    {
        doOnce = true;

        float lowestDistance = Mathf.Infinity;

        bool priority = false;

        bool reduceOnce = false;

        Transform closestEnemy = null;

        for (int i = 0; i < targetInCircleRadius.Length; i++)
        {
            float distance = Vector3.Distance(targetInCircleRadius[i].transform.position, player.transform.position);

            if (targetInCircleRadius[i].tag == "Boss")
            {
                priority = true;

                if (reduceOnce == false)
                {
                    reduceOnce = true;

                    lowestDistance = Mathf.Infinity;
                }

                if (distance < lowestDistance)
                {
                    lowestDistance = distance;

                    closestEnemy = targetInCircleRadius[i].transform;
                }
            }
            else
            {
                if (distance < lowestDistance && !priority)
                {
                    lowestDistance = distance;

                    closestEnemy = targetInCircleRadius[i].transform;
                }
            }
        }

        doOnce = false;

        return closestEnemy;
    }

    //if the counter is more or equal to the fire time we spawn a clone of this gameobject. We aim this gameobject at the enemy within biggest threat if available.
    private void RPG()
    {
        rpgFireCounter += Time.deltaTime;

        if (rpgFireCounter >= rpgFireTime * PlayerStats.Instance.currentCooldownReduction)
        {
            rpgFireCounter = 0;

            GameObject newRocket = Instantiate(rocket, player.transform.position, gameObject.transform.rotation);

            if (biggestThreatTarget.Count > 0)
            {
                newRocket.transform.right -= biggestThreatTarget[0].position - player.transform.position;
            }
        }
    }

    //if the counter is more or equal to the fire time we spawn a clone of this gameobject. we set the player within the throwing axe as the one set in this script
    //We aim this gameobject at the enemy within biggest threat if available.
    private void AxeThrow()
    {
        axeThrowCounter += Time.deltaTime;

        if (axeThrowCounter >= axeThrowTime * PlayerStats.Instance.currentCooldownReduction)
        {
            axeThrowCounter = 0;

            GameObject newAxe = Instantiate(axe, player.transform.position, gameObject.transform.rotation);

            newAxe.GetComponent<ThrowingAxe>().player = player;

            if (biggestThreatTarget.Count > 0)
            {
                newAxe.transform.right -= biggestThreatTarget[0].position - player.transform.position;
            }
        }
    }

    //if the counter is more or equal to the fire time we spawn a clone of this gameobject. if the crossLevel6Multi is active we clone it two more times.
    //We aim this gameobject at the enemy within biggest threat if available, the other spawned crossbow bolts we add a left and right tilt to them
    private void CrossBowFire()
    {
        crossFireCounter += Time.deltaTime;

        if (crossFireCounter >= crossFireTime * PlayerStats.Instance.currentCooldownReduction)
        {
            crossFireCounter = 0;

            GameObject newCrossBow = Instantiate(crossBow, player.transform.position, gameObject.transform.rotation);

            if (crossLevel6Multi)
            {
                crossBow1 = Instantiate(crossBow, player.transform.position, gameObject.transform.rotation);

                crossBow2 = Instantiate(crossBow, player.transform.position, gameObject.transform.rotation);
            }

            if (biggestThreatTarget.Count > 0)
            {
                newCrossBow.transform.right -= biggestThreatTarget[0].position - player.transform.position;

                if (crossLevel6Multi)
                {
                    crossBow1.transform.right -= biggestThreatTarget[0].position - player.transform.position;

                    crossBow1.transform.eulerAngles += new Vector3(0, 0, crossMultiTilt);

                    crossBow2.transform.right -= biggestThreatTarget[0].position - player.transform.position;

                    crossBow2.transform.eulerAngles += new Vector3(0, 0, -crossMultiTilt);
                }
            }
        }
    }

    //if the counter is more or equal to the fire time we spawn a clone of this gameobject. We spawn extra clones based on firsbeeExtraCount.
    //We aim this gameobject at the enemy within biggest threat if available, the other spawend clones are set at random tilts
    private void FrisbeToss()
    {
        frisbeeTossCounter += Time.deltaTime;

        if (frisbeeTossCounter >= frisbeeTossTime * PlayerStats.Instance.currentCooldownReduction)
        {
            frisbeeTossCounter = 0;

            GameObject newfrisbee = Instantiate(frisbee, player.transform.position, gameObject.transform.rotation);

            for (int i = 0; i < frisbeeExtraCount; i++)
            {
                GameObject newestfrisbee = Instantiate(frisbee, player.transform.position, gameObject.transform.rotation);

                int frisbeeTilt = Random.Range(frisbeeSpawnTiltMin, frisbeeSpawnTiltMax);

                newestfrisbee.transform.eulerAngles += new Vector3(0, 0, frisbeeTilt);
            }

            if (biggestThreatTarget.Count > 0)
            {
                newfrisbee.transform.right -= biggestThreatTarget[0].position - player.transform.position;
            }
        }
    }

    //We spawn a turret if the counter is more than or equal to the spawn time
    private void TurretSpawn()
    {
        turretSpawnCounter += Time.deltaTime;

        if (turretSpawnCounter >= turretSpawnTime * PlayerStats.Instance.currentCooldownReduction)
        {
            turretSpawnCounter = 0;
            
            Instantiate(turret, player.transform.position, Quaternion.identity);
        }
    }

    protected virtual void OnDrawGizmosSelected()
    {
        //This is defining a directive where we can tell the complier if we want to run this since this wont build wihtout errors since its not an editor script we use directive so it only runs in editor
        #if UNITY_EDITOR
        Handles.color = Color.blue;
        Handles.DrawWireArc(player.transform.position, Vector3.forward, Vector3.right, 360, viewCircle);      //Draws our now blue circle on the floor around the AI based on circle size

        Handles.color = Color.red;

        foreach (Transform visableTarget in biggestThreatTarget)
        {
            Handles.DrawLine(player.transform.position, visableTarget.position);     //Draws our now red line from the AI to the player so we know its targeting them
        }
        #endif
    }
}
