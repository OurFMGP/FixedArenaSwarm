using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JavalinRain : MonoBehaviour
{
	[Header("Target")]
	[HideInInspector]
	private float viewCircle;        //This allows us to use decimal numbers

	[HideInInspector]
	private Collider2D[] targetInCircleRadius;      //This is an array that will contain Coliiders

	[SerializeField]
	private List<Transform> visableTargets = new List<Transform>();

	[SerializeField]
	protected LayerMask targetMask;     //This allows me to choose one or more layers masks that i have created in this prodject 

	private Transform target;        //This allows me to contain a Tranform

	[Header("DamageAura")]
	public float damage;

	[SerializeField]
	private float originalDamage;

	public Vector2 size;

	[SerializeField]
	private Vector2 originalSize;

	[SerializeField]
	private float damageIntervalTime;

	[SerializeField]
	private float damageIntervalCounter;

	[Header("LifeTime")]
	public float lifeTime;

	[SerializeField]
	private float originalLifeTime;

	[SerializeField]
	private float lifeTimeCounter;

	//We set our size and our view circle based on our x size / 2, we set our damage counter to damage any enemy immediatly 
	private void Start()
	{
		transform.localScale = size;

		viewCircle = size.x / 2;

		damageIntervalCounter = damageIntervalTime;
	}

	//if our life counter is more than life time we destory this object otherwise we call a method
	private void Update()
	{
		if (!PlayerStats.Instance.paused)
        {
			lifeTimeCounter += Time.deltaTime;

			if (lifeTimeCounter >= lifeTime)
			{
				Destroy(gameObject);
			}

			TargetAndHurt();
		}
	}

	//For every enemy in circle radius we deal damage to them as long as the counter is more than or equal to damage time
	private void TargetAndHurt()
	{
		visableTargets.Clear();

		targetInCircleRadius = Physics2D.OverlapCircleAll(transform.position, viewCircle, targetMask);       //We create a sphere around us to detect anyone inside of it based on targetMask      

		for (int i = 0; i < targetInCircleRadius.Length; i++)
		{
			target = targetInCircleRadius[i].transform;

			visableTargets.Add(target);
		}

		damageIntervalCounter += Time.deltaTime;

		if (damageIntervalCounter >= damageIntervalTime)
		{
			foreach (Transform enemy in visableTargets)
			{
				enemy.GetComponent<Collider2D>().gameObject.SendMessageUpwards("TakeGunDamage", damage * PlayerStats.Instance.currentDamage, SendMessageOptions.DontRequireReceiver);
			}

			damageIntervalCounter = 0;
		}
	}

	//we reset prefab valaues
	public void ResetValues()
    {
		damage = originalDamage;

		size = originalSize;

		lifeTime = originalLifeTime;
    }
}
