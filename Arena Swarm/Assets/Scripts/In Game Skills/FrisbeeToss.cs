using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//We inherit from bulletManager
public class FrisbeeToss : BulletManager
{
    [SerializeField]
    private float bounceTiltMin;

    [SerializeField]
    private float bounceTiltMax;

    [SerializeField]
    private bool reverseSpeed;

    //Similer to the original except if we have revereSpeed active we move in the opposite direction
    protected override void LifeCounter()
    {
        lifeTimeCounter += Time.deltaTime;

        if (lifeTimeCounter >= lifeTimeWait)
        {
            Destroy(gameObject);
        }

        if (!reverseSpeed)
        {
            GetComponent<Rigidbody2D>().velocity = -transform.right * speed;
        }
        else
        {
            GetComponent<Rigidbody2D>().velocity = transform.right * speed;
        }
    }

    //Similer to the original except we switch reverseSpeed depending on what its set and we tilt this object randomly based on tilt min and max values
    protected override void TriggerEnter2D(Collider2D collider)
    {
        if (collider.CompareTag("BulletCollider"))
        {
            pierce -= 1;

            collider.gameObject.SendMessageUpwards("TakeGunDamage", damage * PlayerStats.Instance.currentDamage);

            if (pierce <= 0)
            {
                Destroy(gameObject);
            }

            if (!reverseSpeed)
            {
                reverseSpeed = true;
            }
            else
            {
                reverseSpeed = false;
            }

            lifeTimeCounter = 0;

            float bounceTilt = Random.Range(bounceTiltMin, bounceTiltMax);

            transform.eulerAngles += new Vector3(0, 0, bounceTilt);
        }
    }
}
