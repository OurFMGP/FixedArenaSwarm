using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JavalinSpawner : MonoBehaviour
{
    [Header("Javalin")]
    [SerializeField]
    private GameObject javalin;

    public float spawnTime;

    [SerializeField]
    private float spawnCounter;

    [SerializeField]
    private float spawnRadiusMin;

    [SerializeField]
    private float spawnRadiusMax;

    public int javalinAmount;

    //We clone javalins as long as the spawn counter is more than spawntime, the amount of javalins spawned depends on the variable javalinAmount. The place these javalins are spawned
    //depends on spawn min and max around the player in a circle
    private void Update()
    {
        if (!PlayerStats.Instance.paused)
        {
            spawnCounter += Time.deltaTime;

            if (spawnCounter >= spawnTime * PlayerStats.Instance.currentCooldownReduction)
            {
                for (int i = 0; i < javalinAmount; i++)
                {
                    Vector2 direction = Random.insideUnitCircle.normalized;

                    float spawnRadius = Random.Range(spawnRadiusMin, spawnRadiusMax);

                    Vector2 pos = direction * spawnRadius;

                    Instantiate(javalin, transform.position + new Vector3(pos.x, pos.y), Quaternion.identity);
                }

                spawnCounter = 0;
            }
        }       
    }
}
