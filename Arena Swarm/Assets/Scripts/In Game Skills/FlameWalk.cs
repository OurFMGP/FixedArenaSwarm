using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlameWalk : MonoBehaviour
{
    [Header("References")]
    [SerializeField]
    private PlayerMovement playerMovement;

    [SerializeField]
    private GameObject trail;

    [Header("Flame Walk Spawn")]
    [SerializeField]
    private float spawnTime;

    [SerializeField]
    private float spawnCounter;

    //We clone a fire trail game object when the spawn counter is equal to or more than spawn time. The spawn counter is based on delta time and current move speed * 1.1
    private void Update()
    {
        if (playerMovement.moving && !PlayerStats.Instance.paused)
        {
            spawnCounter += Time.deltaTime * PlayerStats.Instance.currentMoveSpeed * 1.1f;

            if (spawnCounter >= spawnTime)
            {
                Instantiate(trail, transform.position, transform.rotation);

                spawnCounter = 0;
            }
        }
    }
}
