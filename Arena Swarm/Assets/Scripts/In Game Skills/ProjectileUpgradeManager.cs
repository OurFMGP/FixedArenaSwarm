using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileUpgradeManager : MonoBehaviour
{
    [Header("References")]
    [SerializeField]
    private ProjectileSkills projectileSkills;

    [Header("RPG Upgrades")]
    [SerializeField]
    private Explosion rpgExplosion;

    public int rpgLevel;

    [SerializeField]
    private float rpgLevel1Damage;

    [SerializeField]
    private float rpgLevel2Damage;

    [SerializeField]
    private float rpgLevel3Cooldown;

    [SerializeField]
    private float rpgLevel4Damage;

    [SerializeField]
    private float rpgLevel5Damage;

    [SerializeField]
    private float rpgLevel6ExplosionRadius;

    [Header("Throwing Axe")]
    [SerializeField]
    private ThrowingAxe throwingAxe;

    public int axeLevel;

    [SerializeField]
    private float axeLevel1Damage;

    [SerializeField]
    private int axeLevel2Pierce;

    [SerializeField]
    private float axeLevel4Damage;

    [SerializeField]
    private int axeLevel5Pierce;

    [SerializeField]
    private int axeLevel6Rebound;

    [Header("CrossBow")]
    [SerializeField]
    private CrossBow crossBow;

    public int crossLevel;

    [SerializeField]
    private float crossLevel1Damage;

    [SerializeField]
    private float crossLevel2Cooldown;

    [SerializeField]
    private float crossLevel4Damage;

    [SerializeField]
    private float crossLevel5Cooldown;

    [Header("Frisbee")]
    [SerializeField]
    private FrisbeeToss frisbeeToss;

    public int frisLevel;

    [SerializeField]
    private int frisLevel1CountAdd;

    [SerializeField]
    private float frisLevel2Damage;

    [SerializeField]
    private int frisLevel3CountAdd;

    [SerializeField]
    private float frisLevel4Damage;

    [SerializeField]
    private int frisLevel5CountAdd;

    [SerializeField]
    private int frisLevel6BounceAdd;

    [Header("Turret Upgrades")]
    [SerializeField]
    private Turret turret;

    [SerializeField]
    private TurretBullet turretBullet;

    public int turretLevel;

    [SerializeField]
    private float turretLevel1Damage;

    [SerializeField]
    private float turretLevel2Firerate;

    [SerializeField]
    private float turretLevel3LifeTime;

    [SerializeField]
    private float turretLevel4Damage;

    [SerializeField]
    private float turretLevel5Firerate;

    [SerializeField]
    private int turretLevel6Peirce;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            //CrossBowUpgrades();
        }
    }

    //when called for the first time we set this to be active as its unlocked otherwise we increase certian stats of this upgrade, key points is at level 6 where it can apply radiation 
    public void RPGUpgrades()
    {
        if (rpgLevel <= 5)
        {
            rpgLevel++;

            switch (rpgLevel)
            {
                case 0:
                    projectileSkills.rpgUnlocked = true;
                    break;

                case 1:
                    rpgExplosion.damage = rpgExplosion.damage * (1f + rpgLevel1Damage);
                    break;

                case 2:
                    rpgExplosion.damage = rpgExplosion.damage * (1f + rpgLevel2Damage);
                    break;

                case 3:
                    projectileSkills.rpgFireTime = projectileSkills.rpgFireTime * (1f - rpgLevel3Cooldown);
                    break;

                case 4:
                    rpgExplosion.damage = rpgExplosion.damage * (1f + rpgLevel4Damage);
                    break;

                case 5:
                    rpgExplosion.damage = rpgExplosion.damage * (1f + rpgLevel5Damage);
                    break;

                case 6:
                    rpgExplosion.explosionRadius = rpgExplosion.explosionRadius * (1f + rpgLevel6ExplosionRadius);

                    rpgExplosion.applyRadiation = true;
                    break;

                default:
                    break;
            }
        }
    }

    //when called for the first time we set this to be active as its unlocked otherwise we increase certian stats of this upgrade, key points is at level 3 where it can rebound to the 
    //player
    public void ThrowingAxeUpgrades()
    {
        if (axeLevel <= 5)
        {
            axeLevel++;

            switch (axeLevel)
            {
                case 0:
                    projectileSkills.axeUnlocked = true;

                    break;

                case 1:
                    throwingAxe.damage = throwingAxe.damage * (1f + axeLevel1Damage);
                    break;

                case 2:
                    throwingAxe.pierce += axeLevel2Pierce;
                    break;

                case 3:
                    throwingAxe.canRebound = true;
                    break;

                case 4:
                    throwingAxe.damage = throwingAxe.damage * (1f + axeLevel4Damage);
                    break;

                case 5:
                    throwingAxe.pierce += axeLevel5Pierce;
                    break;

                case 6:
                    throwingAxe.reboundAmount += axeLevel6Rebound;
                    break;

                default:
                    break;
            }
        }
    }

    //when called for the first time we set this to be active as its unlocked otherwise we increase certian stats of this upgrade, key points is at level 3 where it can apply midas touch
    //and at level 6 where it fires mutiple crossbow bolts
    public void CrossBowUpgrades()
    {
        if (crossLevel <= 5)
        {
            crossLevel++;

            switch (crossLevel)
            {
                case 0:
                    projectileSkills.crossUnlocked = true;

                    break;

                case 1:
                    crossBow.damage = crossBow.damage * (1f + crossLevel1Damage);
                    break;

                case 2:
                    projectileSkills.crossFireTime = projectileSkills.crossFireTime * (1f - crossLevel2Cooldown);
                    break;

                case 3:
                    crossBow.applyMidasTouch = true;
                    break;

                case 4:
                    crossBow.damage = crossBow.damage * (1f + crossLevel4Damage);
                    break;

                case 5:
                    projectileSkills.crossFireTime = projectileSkills.crossFireTime * (1f - crossLevel5Cooldown);
                    break;

                case 6:
                    projectileSkills.crossLevel6Multi = true;
                    break;

                default:
                    break;
            }
        }
    }

    //when called for the first time we set this to be active as its unlocked otherwise we increase certian stats of this upgrade
    public void FrisbeeUpgrades()
    {
        if (frisLevel <= 5)
        {
            frisLevel++;

            switch (frisLevel)
            {
                case 0:
                    projectileSkills.frisbeeUnlocked = true;

                    break;

                case 1:
                    projectileSkills.frisbeeExtraCount += frisLevel1CountAdd;
                    break;

                case 2:
                    frisbeeToss.damage = frisbeeToss.damage * (1f + frisLevel2Damage);
                    break;

                case 3:
                    projectileSkills.frisbeeExtraCount += frisLevel3CountAdd;
                    break;

                case 4:
                    frisbeeToss.damage = frisbeeToss.damage * (1f + frisLevel4Damage);
                    break;

                case 5:
                    projectileSkills.frisbeeExtraCount += frisLevel5CountAdd;
                    break;

                case 6:
                    frisbeeToss.pierce += frisLevel6BounceAdd;
                    break;

                default:
                    break;
            }
        }
    }

    //when called for the first time we set this to be active as its unlocked otherwise we increase certian stats of this upgrade, key points is at level 3 where it can follow the player
    //and at level 6 where is can apply random status effects
    public void TurretUpgrades()
    {
        if (turretLevel <= 5)
        {
            turretLevel++;

            switch (turretLevel)
            {
                case 0:
                    projectileSkills.turretUnlocked = true;
                    break;

                case 1:
                    turretBullet.damage = turretBullet.damage * (1f + turretLevel1Damage);
                    break;

                case 2:
                    turret.fireRate = turret.fireRate * (1f + turretLevel2Firerate);
                    break;

                case 3:
                    turret.followPlayer = true;

                    turret.lifeTime = turret.lifeTime * (1f + turretLevel3LifeTime);
                    break;

                case 4:
                    turretBullet.damage = turretBullet.damage * (1f + turretLevel4Damage);
                    break;

                case 5:
                    turret.fireRate = turret.fireRate * (1f + turretLevel5Firerate);
                    break;

                case 6:
                    turretBullet.pierce += turretLevel6Peirce;

                    turretBullet.funkyBullets = true;
                    break;

                default:
                    break;
            }
        }
    }

    //we call the reset values of these scripts
    private void OnApplicationQuit()
    {
        rpgExplosion.ResetValues();

        throwingAxe.ResetValues();

        crossBow.ResetValues();

        frisbeeToss.ResetValues();

        turret.ResetValues();

        turretBullet.ResetValues();
    }
}
