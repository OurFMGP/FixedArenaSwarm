using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PassiveSkillManager : MonoBehaviour
{
    [Header("References")]
    [SerializeField]
    private PlayerHealth playerHealth;

    [SerializeField]
    private PlayerMovement playerMovement;

    [SerializeField]
    private GunUpgrades gunUpgrades;

    [SerializeField]
    private GunAbility gunAbility;

    [SerializeField]
    private List<Explosion> playerBasedExplosive = new List<Explosion>();

    [SerializeField]
    private EnemyExplosion enemyExplosion;

    [Header("Health Upgrades")]
    [SerializeField]
    private float healthIncrease;

    public int healthLevel;

    [Header("Speed Upgrades")]
    [SerializeField]
    private float speedIncrease;

    public int speedLevel;

    [Header("FireRate Upgrades")]
    [SerializeField]
    private float fireRateIncrease;

    public int fireRateLevel;

    [Header("Damage Upgrades")]
    [SerializeField]
    private float damageIncrease;

    public int damageLevel;

    [Header("Explosive Radius Upgrades")]
    [SerializeField]
    private float increasedExplosiveRadius;

    public int explosiveRadiusLevel;

    [Header("Boss Damage Upgrades")]
    [SerializeField]
    private float increasedBossDamage;

    public int bossDamageLevel;

    [Header("Cooldown Upgrades")]
    [SerializeField]
    private float reducedCooldown;

    public int cooldownLevel;

    [Header("Exp Upgrades")]
    [SerializeField]
    private float increasedExp;

    public int expUpgradeLevel;

    [Header("Spawn Damage Upgrades")]
    [SerializeField]
    private float initialSpawnDamage;

    public int spawnDamageLevel;

    [Header("Gold Upgrades")]
    [SerializeField]
    private int increasedGold;

    public int goldLevel;

    //Increases our health and max health
    public void HealthIncrease()
    {
        if (healthLevel != 5)
        {
            healthLevel++;

            PlayerStats.Instance.maxHealth = PlayerStats.Instance.maxHealth * (1f + healthIncrease);

            playerHealth.ModifyHealth(PlayerStats.Instance.currentHealth = PlayerStats.Instance.currentHealth * (1f + healthIncrease));
        }
    }

    //Increases our movement speed
    public void SpeedIncrease()
    {
        if (speedLevel != 5)
        {
            speedLevel++;

            PlayerStats.Instance.maxMoveSpeed = PlayerStats.Instance.maxMoveSpeed * (1f + speedIncrease);
            PlayerStats.Instance.currentMoveSpeed = PlayerStats.Instance.currentMoveSpeed* (1f + speedIncrease);
            PlayerStats.Instance.initialMoveSpeed = PlayerStats.Instance.initialMoveSpeed * (1f + speedIncrease);
        }
    }

    //Increases our current guns fire rate
    public void FireRateIncrease()
    {
        if (fireRateLevel != 5)
        {
            fireRateLevel++;

            PlayerStats.Instance.currentFireRate = PlayerStats.Instance.currentFireRate * (1f - fireRateIncrease);
        }
    }

    //Increases our damage
    public void DamageIncrease()
    {
        if (damageLevel != 5)
        {
            damageLevel++;

            PlayerStats.Instance.currentDamage  = PlayerStats.Instance.currentDamage * (1f + damageIncrease);
        }
    }

    //Increases our explosive radius maunualy by grabbing the explosives to increase
    public void IncreasedExplosiveRadius()
    {
        if (explosiveRadiusLevel != 5)
        {
            explosiveRadiusLevel++;

            foreach (Explosion explosive in playerBasedExplosive)
            {
                explosive.explosionRadius = explosive.explosionRadius * (1f + increasedExplosiveRadius);
            }

            enemyExplosion.explosionRadius = enemyExplosion.explosionRadius * (1f + increasedExplosiveRadius);

            if (GunUpgrades.ablityBeingUsed)
            {
                gunAbility.m4ExplosionRadiusBoostOriginal = gunAbility.m4ExplosionRadiusBoostOriginal * (1f + increasedExplosiveRadius);
            }
        }
    }

    //Increases our base damage against bosses
    public void IncreasedBossDamage()
    {
        if (bossDamageLevel != 5)
        {
            bossDamageLevel++;

            PlayerStats.Instance.currentBossDamageMultiplier += increasedBossDamage;
        }
    }

    //Decreases our general skill cooldowns
    public void DecreasedCooldown()
    {
        if (cooldownLevel != 5)
        {
            cooldownLevel++;

            PlayerStats.Instance.currentCooldownReduction -= reducedCooldown;
        }
    }

    //Increases our base exp gain
    public void IncreasedExp()
    {
        if (expUpgradeLevel != 5)
        {
            expUpgradeLevel++;

            PlayerStats.Instance.currentExperienceMultiplier += increasedExp;
        }
    }

    //Increases our base enemy spawn damage
    public void SpawnDamageIncreased()
    {
        if (spawnDamageLevel != 5)
        {
            spawnDamageLevel++;

            PlayerStats.Instance.currentSpawnDamageMultiplier += initialSpawnDamage;
        }
    }

    //Increases our base gold gain
    public void IncreasedGold()
    {
        if (goldLevel != 5)
        {
            goldLevel++;

            PlayerStats.Instance.currentGoldMultiplier += increasedGold;
        }
    }
}
