using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlameDamage : MonoBehaviour
{
    [Header("Flame Damage")]
    public float fireDamagePerSecond;

    [SerializeField]
    private float originalFireDamagePerSecond;

    [SerializeField]
    private float damageCounter;

    public float whenToDestroyTime;

    [SerializeField]
    private float originalWhenToDestroyTime;

    [SerializeField]
    private float whenToDestroyCounter;

    [Header("On Fire")]
    public bool causeOnFire;

    public float onFireDuration;

    [SerializeField]
    private float originalOnFireDuration;

    //We set damage value to 1 so it can deal damage straight away
    private void Start()
    {
        damageCounter = 1;
    }

    //If the life is more than we set we destroy the object
    private void Update()
    {
        if (!PlayerStats.Instance.paused)
        {
            whenToDestroyCounter += Time.deltaTime;

            if (whenToDestroyCounter >= whenToDestroyTime)
            {
                Destroy(gameObject);
            }
        }
    }

    //if the damage counter is 1 or more we deal damage to the enemy that matches the tag if we can casue fire we call ApplyFire for the duration set
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.CompareTag("BulletCollider"))
        {
            damageCounter += Time.deltaTime;

            if (damageCounter >= 1)
            {
                collider.gameObject.SendMessageUpwards("TakeGunDamage", fireDamagePerSecond * PlayerStats.Instance.currentDamage);

                if (causeOnFire)
                {
                    collider.GetComponent<Collider2D>().gameObject.SendMessageUpwards("ApplyFire", onFireDuration, SendMessageOptions.DontRequireReceiver);
                }               

                damageCounter = 0;
            }
        }
    }

    //we reset prefab valaues
    public void ResetValues()
    {
        fireDamagePerSecond = originalFireDamagePerSecond;

        onFireDuration = originalOnFireDuration;

        whenToDestroyTime = originalWhenToDestroyTime;

        causeOnFire = false;
    }
}
