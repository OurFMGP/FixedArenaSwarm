using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RevolvingSheild : MonoBehaviour
{
    [Header("References")]
    [SerializeField]
    private SpriteRenderer spriteRenderer;

    [Header("Sheild stats")]
    public float sheildMaxHealth;

    public float sheildCurrentHealth;

    [SerializeField]
    private bool sheildDown;

    [SerializeField]
    private float sheildDownAlpha;

    public float sheildRecoverTime;

    [SerializeField]
    private float sheildRecoverCounter;

    public float sheildDamagePerSecond;

    [SerializeField]
    private float damageCounter;

    //Sets health to max
    private void Start()
    {
        sheildCurrentHealth = sheildMaxHealth;
    }

    //If the shield is down we have to wait til the counter is more than or equal to the recovery time before we can activate it again. We set its color back to white
    private void Update()
    {
        if (sheildDown && !PlayerStats.Instance.paused)
        {
            sheildRecoverCounter += Time.deltaTime;

            if (sheildRecoverCounter >= sheildRecoverTime * PlayerStats.Instance.currentCooldownReduction)
            {
                sheildCurrentHealth = sheildMaxHealth;

                sheildRecoverCounter = 0;

                spriteRenderer.color = Color.white;

                sheildDown = false;
            }
        }
    }

    //If we collide with an EnemyBulet we take damage and destroy that enemey bullet, if our health is 0 or less we change our aplha colour down. Othwerise if we hit an enemy we deal
    //some damage to them
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!PlayerStats.Instance.paused)
        {
            if (collision.CompareTag("EnemyBullet") && !sheildDown)
            {
                sheildCurrentHealth -= collision.GetComponent<EnemyBullet>().shieldDamage;

                Destroy(collision.gameObject);

                if (sheildCurrentHealth <= 0)
                {
                    Color changedColour = spriteRenderer.color;

                    changedColour.a = sheildDownAlpha;

                    spriteRenderer.color = changedColour;

                    sheildDown = true;
                }
            }

            if (collision.CompareTag("Enemy") || collision.CompareTag("Boss"))
            {
                damageCounter = 0;

                collision.gameObject.SendMessageUpwards("TakeGunDamage", sheildDamagePerSecond * PlayerStats.Instance.currentDamage);
            }
        }       
    }

    //We also deal damage to the enemys on stay since object is slow and can be used to stick onto an enemy
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemy") || collision.CompareTag("Boss"))
        {
            damageCounter += Time.deltaTime;

            if (damageCounter >= 1)
            {
                collision.gameObject.SendMessageUpwards("TakeGunDamage", sheildDamagePerSecond * PlayerStats.Instance.currentDamage);

                damageCounter = 0;
            }
        }     
    }
}
