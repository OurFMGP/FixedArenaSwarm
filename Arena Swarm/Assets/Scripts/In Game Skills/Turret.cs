using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR  
using UnityEditor;      //This paticular library gives us access to Editor keywords
#endif

public class Turret : MonoBehaviour
{
    [Header("Target")]
    [SerializeField]
    private GameObject player;

    [SerializeField]
    private float viewCircle;

    [SerializeField]
    private Collider2D[] targetInCircleRadius;

    [SerializeField]
    private List<Transform> biggestThreatTarget = new List<Transform>();

    [SerializeField]
    private LayerMask targetMask;       

    [SerializeField]
    private LayerMask obstacleMask;       //This allows me to choose one or more layers masks that i have created in this prodject 

    private Transform target;        //This allows me to contain a Tranform

    private Vector3 directionToTarget;        //This allows me to contain a Vector3

    private float distanceToTarget;      //This allows us to use decimal numbers      

    private bool doOnce;

    [Header("Fire")]
    [SerializeField]
    private GameObject currentBullet;

    [SerializeField]
    private GameObject barrel;     

    [SerializeField]
    private GameObject spawnSpot;

    public float fireRate;

    [SerializeField]
    private float originalFireRate;

    [SerializeField]
    private float actualFireRate;

    [SerializeField]
    private float fireCounter;

    [Header("Follow")]
    public bool followPlayer;

    [SerializeField]
    private float distanceToKeep;

    [SerializeField]
    private float distance;

    [SerializeField]
    private float spawnRadius;

    [Header("LifeTime")]
    public float lifeTime;

    [SerializeField]
    private float originalLifeTime;

    [SerializeField]
    private float lifeTimeCounter;

    //We set our player gameobject to the player and calculate its fire rate
    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");

        if (fireRate != 0)
        {
            actualFireRate = 1.0f / fireRate;
        }
        else
        {
            actualFireRate = 0.01f;
        }
    }

    //If counter is more than or equal to life time we destroy this. If follow player is active we check the distance between this and player, if the distance is more than distance to
    //keep we move this turret to teliport around the player. We also call target.
    private void Update()
    {
        if (!PlayerStats.Instance.paused)
        {
            lifeTimeCounter += Time.deltaTime;

            if (lifeTimeCounter >= lifeTime)
            {
                Destroy(gameObject);
            }

            if (followPlayer)
            {
                distance = Vector2.Distance(transform.position, player.transform.position);

                if (distance > distanceToKeep)
                {
                    Vector2 direction = Random.insideUnitCircle.normalized;

                    Vector2 pos = direction * spawnRadius;

                    transform.position = player.transform.position + new Vector3(pos.x, pos.y);
                }
            }

            Target();
        }       
    }

    //We make an overlap circle based on the view circle. all targets that we see that are not behind the obstacle mask we then add to the biggestThreat list, if returned from
    //findtarget method. We aim our barrel and bullet spawn spot at the target and call turretFIre.
    private void Target()
    {
        biggestThreatTarget.Clear();

        targetInCircleRadius = Physics2D.OverlapCircleAll(barrel.transform.position, viewCircle, targetMask);

        for (int i = 0; i < targetInCircleRadius.Length; i++)
        {
            target = targetInCircleRadius[i].transform;

            directionToTarget = (target.position - transform.position).normalized;

            distanceToTarget = Vector2.Distance(transform.position, target.position);

            if (!Physics.Raycast(transform.position, directionToTarget, distanceToTarget, obstacleMask) && doOnce == false)
            {
                biggestThreatTarget.Add(FindTarget());
            }           
        }

        if (biggestThreatTarget.Count > 0)
        {
            barrel.transform.up = biggestThreatTarget[0].position - barrel.transform.position;

            spawnSpot.transform.right -= biggestThreatTarget[0].position - spawnSpot.transform.position;

            TurretFire();
        }
    }

    //Our target is based on the closest enemy to the player which is constantly checked. we take priorioty over the boss class enemies as long as we see them in our view circle
    private Transform FindTarget()
    {
        doOnce = true;

        float lowestDistance = Mathf.Infinity;

        bool priority = false;

        bool reduceOnce = false;

        Transform closestEnemy = null;

        for (int i = 0; i < targetInCircleRadius.Length; i++)
        {
            float distance = Vector3.Distance(targetInCircleRadius[i].transform.position, transform.position);

            if (targetInCircleRadius[i].tag == "Boss")
            {
                priority = true;

                if (reduceOnce == false)
                {
                    reduceOnce = true;

                    lowestDistance = Mathf.Infinity;
                }

                if (distance < lowestDistance)
                {
                    lowestDistance = distance;

                    closestEnemy = targetInCircleRadius[i].transform;
                }
            }
            else
            {
                if (distance < lowestDistance && !priority)
                {
                    lowestDistance = distance;

                    closestEnemy = targetInCircleRadius[i].transform;
                }
            }
        }

        doOnce = false;

        return closestEnemy;
    }

    //If our fire counter is more than actual fire rate we clone a bullet
    private void TurretFire()
    {
        if (fireCounter < actualFireRate)
        {
            fireCounter += Time.deltaTime;
        }

        if (fireCounter >= actualFireRate)
        {
            fireCounter = 0;

            Instantiate(currentBullet, spawnSpot.transform.position, spawnSpot.transform.rotation);
        }
    }

    //We reset these values to the original
    public void ResetValues()
    {
        fireRate = originalFireRate;

        followPlayer = false;

        lifeTime = originalLifeTime;
    }

    protected virtual void OnDrawGizmosSelected()
    {
        //This is defining a directive where we can tell the complier if we want to run this since this wont build wihtout errors since its not an editor script we use directive so it only runs in editor
#if UNITY_EDITOR
        Handles.color = Color.blue;
        Handles.DrawWireArc(transform.position, Vector3.forward, Vector3.right, 360, viewCircle);      //Draws our now blue circle on the floor around the AI based on circle size

        Handles.color = Color.red;

        foreach (Transform visableTarget in biggestThreatTarget)
        {
            Handles.DrawLine(barrel.transform.position, visableTarget.position);     //Draws our now red line from the AI to the player so we know its targeting them
        }
#endif
    }
}
