using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//We inherit from bulletManager
public class TurretBullet : BulletManager
{
    [Header("Funky Bullets")]
    public bool funkyBullets;

    [SerializeField]
    private int chanceToStatusPercent;

    [SerializeField]
    private int radiationDuration;

    [SerializeField]
    private int midasDuration;

    [SerializeField]
    private int burnDuration;

    //For every bulletCollider we hit we apply damage to it. if we have funkyBullets active we do a random check to see if we can apply a status, if so we do another random check to see
    //which one to apply. if pierce is 0 or less we destroy this bullet
    protected override void TriggerEnter2D(Collider2D collider)
    {
        if (collider.CompareTag("BulletCollider"))
        {
            pierce -= 1;

            collider.gameObject.SendMessageUpwards("TakeGunDamage", damage * PlayerStats.Instance.currentDamage);

            if (funkyBullets)
            {
                int randomNum = Random.Range(1, 101);

                if (randomNum <= chanceToStatusPercent)
                {
                    int newRandomNum = Random.Range(1, 4);

                    switch (newRandomNum)
                    {
                        case 1:
                            collider.GetComponent<Collider2D>().gameObject.SendMessageUpwards("ApplyRadiation", radiationDuration, SendMessageOptions.DontRequireReceiver);
                            break;

                        case 2:
                            collider.GetComponent<Collider2D>().gameObject.SendMessageUpwards("ApplyMidasTouch", radiationDuration, SendMessageOptions.DontRequireReceiver);
                            break;
                        default:
                            collider.GetComponent<Collider2D>().gameObject.SendMessageUpwards("ApplyFire", radiationDuration, SendMessageOptions.DontRequireReceiver);
                            break;
                    }
                }
            }

            if (pierce <= 0)
            {
                Destroy(gameObject);
            }
        }
    }

    //We turn funky bullets off and call reset value in bullet manager
    public override void ResetValues()
    {
        funkyBullets = false;

        base.ResetValues();
    }
}
