using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//We inherit from bulletManager
public class CrossBow : BulletManager
{
	[Header("Level 3")]
	public bool applyMidasTouch;

	[SerializeField]
	private float midasDuration;

	//Similer to the original except we apply midas touch to that enemy if its active
	protected override void TriggerEnter2D(Collider2D collider)
	{
		if (collider.CompareTag("BulletCollider"))
		{
			pierce -= 1;

			if (applyMidasTouch)
			{
				collider.GetComponent<Collider2D>().gameObject.SendMessageUpwards("ApplyMidasTouch", midasDuration, SendMessageOptions.DontRequireReceiver);
			}

			collider.gameObject.SendMessageUpwards("TakeGunDamage", damage * PlayerStats.Instance.currentDamage);

			if (pierce <= 0)
			{
				Destroy(gameObject);
			}
		}
	}

	//We turn off midastouch and call bullet manager reset value
	public override void ResetValues()
    {
		applyMidasTouch = false;

		base.ResetValues();
    }
}
