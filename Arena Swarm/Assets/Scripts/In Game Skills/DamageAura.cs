using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;      //This paticular library gives us access to Editor keywords
#endif

public class DamageAura : MonoBehaviour
{
	[Header("Target")]
	[SerializeField]
	private GameObject player;

	[HideInInspector]
	private float viewCircle;        //This allows us to use decimal numbers

	[HideInInspector]
	private Collider2D[] targetInCircleRadius;      //This is an array that will contain Coliiders

	[SerializeField]
	private List<Transform> visableTargets = new List<Transform>();

	[SerializeField]
	protected LayerMask targetMask;     //This allows me to choose one or more layers masks that i have created in this prodject 

	private Transform target;        //This allows me to contain a Tranform

	[Header("DamageAura")]
	[SerializeField]
	private float damage;

	public Vector2 size;

	[SerializeField]
	private float damageIntervalTime;

	[SerializeField]
	private float damageIntervalCounter;

	[Header("Slowing")]
	public bool canSlow;

	[SerializeField]
	private float slowingSpeedReduction;

	//we add bonus damage if we have any and call a method
	private void Start()
	{
		damage = damage * PlayerStats.Instance.currentDamage;

		UpdatedDamageRadius();
	}

	//We call a method
    private void Update()
    {
		if (!PlayerStats.Instance.paused)
        {
			TargetAndHurt();
		}
	}

	//We get get all objects with the target mask in the target radius in which we apply damage to everything inside of it based on our damage counter, if it can slow we reduce its speed
    private void TargetAndHurt()
	{
		visableTargets.Clear();

		targetInCircleRadius = Physics2D.OverlapCircleAll(player.transform.position, viewCircle, targetMask);       //We create a sphere around us to detect anyone inside of it based on targetMask      

		for (int i = 0; i < targetInCircleRadius.Length; i++)
		{
			target = targetInCircleRadius[i].transform;

			visableTargets.Add(target);
		}

		damageIntervalCounter += Time.deltaTime;

		if (damageIntervalCounter >= damageIntervalTime)
		{
			foreach (Transform enemy in visableTargets)
			{
				enemy.GetComponent<Collider2D>().gameObject.SendMessageUpwards("TakeGunDamage", damage * PlayerStats.Instance.currentDamage, SendMessageOptions.DontRequireReceiver);				
			}

			damageIntervalCounter = 0;
		}

		if (canSlow)
		{
			foreach (Transform enemy in visableTargets)
			{
				enemy.GetComponent<EnemyMovement>().Slowed(slowingSpeedReduction);
			}			
		}
	}

	//We set the scale based on size as well as its viewcircle used to apply damage
	public void UpdatedDamageRadius()
    {
		transform.localScale = size;

		viewCircle = size.x / 2;
	}

	protected virtual void OnDrawGizmosSelected()
	{
		//This is defining a directive where we can tell the complier if we want to run this since this wont build wihtout errors since its not an editor script we use directive so it only runs in editor
		#if UNITY_EDITOR
		Handles.color = Color.blue;
		Handles.DrawWireArc(player.transform.position, Vector3.forward, Vector3.right, 360, viewCircle);      //Draws our now blue circle on the floor around the AI based on circle size
		#endif
	}
}
