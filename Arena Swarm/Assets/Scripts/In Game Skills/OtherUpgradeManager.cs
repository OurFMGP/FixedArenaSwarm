using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OtherUpgradeManager : MonoBehaviour
{
    [Header("Flame Walk Upgrades")]
    [SerializeField]
    private FlameWalk flameWalk;

    [SerializeField]
    private FlameDamage flameDamage;

    public int flameWalkLevel;

    [SerializeField]
    private float flameLevel1Damage;

    [SerializeField]
    private float flameLevel2FireLife;

    [SerializeField]
    private int flameLevel4OnFireLife;

    [SerializeField]
    private float flameLevel5Damage;

    [SerializeField]
    private float flameLevel6FireLife;

    [SerializeField]
    private int flameLevel6OnFireLife;

    [Header("Enemy Explosion Upgrades")]
    [SerializeField]
    private EnemyExplosion enemyExplosion;

    public int enemyExplosionLevel;

    [SerializeField]
    private float enemyLevel1ExplosionRadius;

    [SerializeField]
    private float enemyLevel2Damage;

    [SerializeField]
    private float enemyLevel3ExplosionRadius;

    [SerializeField]
    private float enemyLevel4Damage;

    [SerializeField]
    private float enemyLevel5ExplosionRadius;

    [Header("Javalin Upgrades")]
    [SerializeField]
    private JavalinSpawner javalinSpawner;

    [SerializeField]
    private JavalinRain JavalinRain;

    public int javalinLevel;

    [SerializeField]
    private float javalinLevel1Cooldown;

    [SerializeField]
    private float javalinLevel2Damage;

    [SerializeField]
    private int javalinLevel3LifeTime;

    [SerializeField]
    private Vector2 javalinLevel3Size;

    [SerializeField]
    private float javalinLevel4Damage;

    [SerializeField]
    private Vector2 javalinLevel5Size;

    [SerializeField]
    private int javalinLevel6AddCount;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            //EnemyExplosionUpgrades();
        }
    }

    //when called for the first time we set this to be active as its unlocked otherwise we increase certian stats of this upgrade, key points is at level 3 where we allow this to cause
    //fire damage
    public void FlameWalkpgrades()
    {
        if (flameWalkLevel <= 5)
        {
            flameWalkLevel++;

            switch (flameWalkLevel)
            {
                case 0:
                    flameWalk.gameObject.SetActive(true);
                    break;

                case 1:
                    flameDamage.fireDamagePerSecond = flameDamage.fireDamagePerSecond * (1f + flameLevel1Damage);
                    break;

                case 2:
                    flameDamage.whenToDestroyTime = flameDamage.whenToDestroyTime * (1f + flameLevel2FireLife);
                    break;

                case 3:
                    flameDamage.causeOnFire = true;
                    break;

                case 4:
                    flameDamage.onFireDuration += flameLevel4OnFireLife;
                    break;

                case 5:
                    flameDamage.fireDamagePerSecond = flameDamage.fireDamagePerSecond * (1f + flameLevel5Damage);
                    break;

                case 6:
                    flameDamage.whenToDestroyTime = flameDamage.whenToDestroyTime * (1f + flameLevel6FireLife);

                    flameDamage.onFireDuration += flameLevel6OnFireLife; 
                    break;

                default:
                    break;
            }
        }
    }

    //when called for the first time we set this to be active as its unlocked otherwise we increase certian stats of this upgrade, key points is at level 6 where we allow this to cause
    //chain reactions 
    public void EnemyExplosionUpgrades()
    {
        if (enemyExplosionLevel <= 5)
        {
            enemyExplosionLevel++;

            switch (enemyExplosionLevel)
            {
                case 0:
                    enemyExplosion.unlocked = true;
                    break;

                case 1:
                    enemyExplosion.explosionRadius = enemyExplosion.explosionRadius * (1f + enemyLevel1ExplosionRadius);
                    break;

                case 2:
                    enemyExplosion.damageMulti += enemyLevel2Damage;
                    break;

                case 3:
                    enemyExplosion.explosionRadius = enemyExplosion.explosionRadius * (1f + enemyLevel3ExplosionRadius);
                    break;

                case 4:
                    enemyExplosion.damageMulti += enemyLevel4Damage;
                    break;

                case 5:
                    enemyExplosion.explosionRadius = enemyExplosion.explosionRadius * (1f + enemyLevel5ExplosionRadius);
                    break;

                case 6:
                    enemyExplosion.chainReaction = true;
                    break;

                default:
                    break;
            }
        }
    }

    //when called for the first time we set this to be active as its unlocked otherwise we increase certian stats of this upgrade
    public void JavalinUpgrades()
    {
        if (javalinLevel <= 5)
        {
            javalinLevel++;

            switch (javalinLevel)
            {
                case 0:
                    javalinSpawner.gameObject.SetActive(true);
                    break;

                case 1:
                    javalinSpawner.spawnTime = javalinSpawner.spawnTime * (1f - javalinLevel1Cooldown);
                    break;

                case 2:
                    JavalinRain.damage = JavalinRain.damage * (1f + javalinLevel2Damage);
                    break;

                case 3:
                    JavalinRain.size += javalinLevel3Size;

                    JavalinRain.lifeTime = JavalinRain.lifeTime * (1f + javalinLevel3LifeTime);
                    break;

                case 4:
                    JavalinRain.damage = JavalinRain.damage * (1f + javalinLevel4Damage);
                    break;

                case 5:
                    JavalinRain.size += javalinLevel5Size;
                    break;

                case 6:
                    javalinSpawner.javalinAmount += javalinLevel6AddCount;
                    break;

                default:
                    break;
            }
        }
    }

    //we call the reset values of these scripts
    private void OnApplicationQuit()
    {
        flameDamage.ResetValues();

        enemyExplosion.ResetValues();

        JavalinRain.ResetValues();
    }
}
