using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyExplosion : MonoBehaviour
{
	[Header("References")]
	[SerializeField]
	private GameObject sphere;

	[Header("Explosion")]
	public float explosionRadius;

	[SerializeField]
	private float originalExplosionRadius;

	[SerializeField]
	private float explosionLife;

	[SerializeField]
	private float explosionTimeLimit;

	[Header("Enemy Checks")]
	public bool unlocked;

	public float damageMulti;

	public bool chainReaction;

	//We damage anthing inside the explosion radius
	public void SETBOOM(float passedDamage)
	{
		Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, explosionRadius);

		sphere.SetActive(true);

		foreach (Collider2D collider in colliders)
		{
			float damageAmount = passedDamage * damageMulti;

			if (collider.CompareTag("BulletCollider"))
			{
				collider.GetComponent<Collider2D>().gameObject.SendMessageUpwards("TakeEnemyExplosionDamage", damageAmount * PlayerStats.Instance.currentDamage, SendMessageOptions.DontRequireReceiver);
			}
		}
	}

	//If the explosion life is more than we set we destroy the object
	private void Update()
	{
		if (!PlayerStats.Instance.paused)
        {
			explosionLife += Time.deltaTime;

			if (explosionLife >= explosionTimeLimit)
			{
				Destroy(transform.gameObject);
			}
		}
	}

	//we reset prefab valaues
	public void ResetValues()
	{
		explosionRadius = originalExplosionRadius;

		unlocked = false;

		damageMulti = 1;

		chainReaction = false;
	}
}
