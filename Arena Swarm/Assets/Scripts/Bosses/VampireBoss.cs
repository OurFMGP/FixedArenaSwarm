using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VampireBoss : MonoBehaviour
{
    [Header("References")]
    [SerializeField]
    private AnimationManager animationManager;

    [SerializeField]
    private BossHealth bossHealth;

    [SerializeField]
    private VampireMovement vampireMovement;

    [SerializeField]
    private List<GameObject> lightStates = new List<GameObject>();

    [SerializeField]
    private GameObject player;

    [Header("General Fields")]
    public bool itsShowTime;

    private int healthCheckerCounter = 1;

    public bool stage2Animations;

    [Header("Main Attack")]
    [SerializeField]
    private GameObject mainSpawnSpot;

    [SerializeField]
    private GameObject mainBullet;

    [SerializeField]
    private bool stopMainAttacks;

    [SerializeField]
    private float mainAttackTime;

    [SerializeField]
    private float mainAttackCounter;

    public bool freeMainAttack;

    [SerializeField]
    private float mainAttackFlurry;

    [SerializeField]
    private float mainAttackFlurryDelay;

    private int mainAttackIncrement;

    [SerializeField]
    private float mainAttackTilt;

    private bool mainAttackOnce;

    [Header("Orb Attack")]
    [SerializeField]
    private GameObject orbBullet;

    [SerializeField]
    private GameObject orbSpawnSpot;

    private EnemyBullet currentOrb;

    public bool stopOrbAttacks;

    [SerializeField]
    private int tpAmountToUseOrb;

    [Header("Clone Attack")]
    [SerializeField]
    private GameObject vampireClone1;

    [SerializeField]
    private GameObject vampireClone2;

    [SerializeField]
    private bool clone;

    private void Start()
    {
        animationManager.ChangeAnimationState("Vampire Transition");
    }

    //We run counter to see if we can use that paticular attack and call that method, we have a special attacks based on how many times we have teliported. We always call
    //HealthBasedEvents if we are not a clone. While using the orb attack we make sure not to do a normal attack or teliport during it
    private void Update()
    {
        if (itsShowTime)
        {
            if (!stopMainAttacks)
            {
                if (mainAttackCounter <= mainAttackTime && !freeMainAttack)
                {
                    mainAttackCounter += Time.deltaTime;
                }
                else
                {
                    mainSpawnSpot.transform.right -= player.transform.position - mainSpawnSpot.transform.position;

                    if (!mainAttackOnce)
                    {
                        mainAttackOnce = true;

                        StartCoroutine(MainAttackSpread());
                    }
                }
            }

            if (!stopOrbAttacks)
            {
                if (vampireMovement.teliportOrbUseCounter >= tpAmountToUseOrb)
                {
                    BloodOrb();
                }
            }

            if (currentOrb != null)
            {
                if (currentOrb.delayFireCounter >= currentOrb.delayFire || vampireMovement.freeTeliport)
                {
                    stopMainAttacks = false;

                    vampireMovement.movementType = 1;

                    currentOrb = null;
                }
            }

            if (!clone)
            {
                HealthBasedEvents();
            }
        }
    }

    //Just a simple shotgun spread type shot adding tilt to the first and last bullet.
    private IEnumerator MainAttackSpread()
    {
        while (true)
        {
            if (mainAttackIncrement < mainAttackFlurry && !stopMainAttacks)
            {
                if (!stage2Animations)
                {
                    animationManager.ChangeAnimationState("Vampire Attack");
                }
                else
                {
                    animationManager.ChangeAnimationState("Vampire Attack 2");
                }

                for (int i = 0; i < 3; i++)
                {
                    GameObject newBullet = Instantiate(mainBullet, mainSpawnSpot.transform.position, mainSpawnSpot.transform.rotation);

                    if (i == 0)
                    {
                        newBullet.transform.eulerAngles += new Vector3(0, 0, -mainAttackTilt);
                    }
                    else if (i == 2)
                    {
                        newBullet.transform.eulerAngles += new Vector3(0, 0, mainAttackTilt);
                    }
                }

                mainAttackIncrement++;

                yield return new WaitForSeconds(mainAttackFlurryDelay);
            }
            else
            {
                break;
            }
        }

        mainAttackIncrement = 0;

        mainAttackCounter = 0;

        freeMainAttack = false;

        mainAttackOnce = false;
    }

    //We make sure we cant move and reset the counter for the blood orb as otherwise we would always be using it after the teliport goal has been reached
    private void BloodOrb()
    {
        if (!stage2Animations)
        {
            animationManager.ChangeAnimationState("Vampire Attack");
        }
        else
        {
            animationManager.ChangeAnimationState("Vampire Attack 2");
        }

        vampireMovement.movementType = 0;

        vampireMovement.teliportOrbUseCounter = 0;

        stopMainAttacks = true;

        GameObject bloodOrb = Instantiate(orbBullet, orbSpawnSpot.transform.position, Quaternion.identity);

        currentOrb = bloodOrb.GetComponent<EnemyBullet>();
    }

    //When we reach approximatly 75% Health we set active a clone and activate as well as deactive some lights to make it darker. We do the same with healthchecker2 when we reach half
    //health, making it even darker. We make sure to only activate the clone when we have finished teliporting after which we also gives the boss an instant teliport;
    private void HealthBasedEvents()
    {
        if (healthCheckerCounter == 1)
        {
            if (bossHealth.currentHealth <= bossHealth.maxHealth / 1.33f && vampireMovement.getNewTpPosition == true)
            {
                stage2Animations = true;
                
                animationManager.ChangeAnimationState("Vampire Clone");

                vampireClone1.transform.parent = null;

                vampireClone1.SetActive(true);

                lightStates[0].SetActive(false);

                lightStates[1].SetActive(true);

                vampireMovement.freeTeliport = true;

                healthCheckerCounter++;
            }
        }
        else if (healthCheckerCounter == 2)
        {
            if (bossHealth.currentHealth <= bossHealth.maxHealth / 2 && vampireMovement.getNewTpPosition == true)
            {
                animationManager.ChangeAnimationState("Vampire Clone 2");

                vampireClone2.transform.parent = null;

                vampireClone2.SetActive(true);

                lightStates[1].SetActive(false);

                lightStates[2].SetActive(true);

                vampireMovement.freeTeliport = true;

                healthCheckerCounter++;
            }
        }
    }
}
