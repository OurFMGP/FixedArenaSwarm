using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeBoss : MonoBehaviour
{
    [Header("General References")]
    [SerializeField]
    private AnimationManager animationManager;

    [SerializeField]
    private BossHealth bossHealth;

    [SerializeField]
    private PlayerHealth playerHealth;

    [SerializeField]
    private EnemyMovement enemyMovement;

    [SerializeField]
    private Collider2D collider2D;

    [SerializeField]
    private GameObject player;

    [SerializeField]
    private SpriteRenderer spriteRenderer;

    [SerializeField]
    private Sprite secondPhaseSprite;

    [SerializeField]
    private GameObject bossTrees;

    [SerializeField]
    private List<SpriteRenderer> bossTreesRenderer = new List<SpriteRenderer>();

    [Header("General Fields")]
    public bool itsShowTime;

    [SerializeField]
    private int contactDamageAmount;

    [SerializeField]
    private bool stopSpecialAttacks;

    [SerializeField]
    private float hideAlphaChangeRate;

    private int healthCheckerCounter = 1;

    [Header("Main Attack")]
    [SerializeField]
    private GameObject flurryBullet;

    [SerializeField]
    private GameObject flurryBullet2;

    private GameObject currentFlurryBullet;

    [SerializeField]
    private bool stopMainAttacks;

    [SerializeField]
    private float mainAttackTime;

    [SerializeField]
    private float mainAttackCounter;

    [SerializeField]
    private int flurryAmmount;

    private int flurryCountDelayAdd;

    [Header("Circular Attack")]
    [SerializeField]
    private List<GameObject> circularPulseAttack = new List<GameObject>();

    [SerializeField]
    private float circularPulseAttackTime;

    [SerializeField]
    private float circularPulseAttackCounter;

    [SerializeField]
    private int circularFlurryAmount;

    [Header("Pincer Attack")]
    [SerializeField]
    private GameObject pincerBullet;

    [SerializeField]
    private GameObject pincerSpawnSpot;

    [SerializeField]
    private float pincerAttackTime;

    [SerializeField]
    private float pincerAttackCounter;

    [SerializeField]
    private float pincerTilt;

    private bool leftRightSwitch;

    [Header("Ads Attack")]
    [SerializeField]
    private GameObject treeFolk;

    [SerializeField]
    private GameObject smallTree;

    [SerializeField]
    private List<GameObject> enemyAdsLeft = new List<GameObject>();

    [SerializeField]
    private int adsToSpawn;

    [SerializeField]
    private float adsSpawnRadius;

    private bool adsSpawned;

    //We run counter to see if we can use that paticular attack and call that method, we have two differnt special attacks based on if we are in the second phase or not. We always call
    //HealthBasedEvents and BossAdChecker
    private void Update()
    {
        if (itsShowTime)
        {
            if (!stopMainAttacks)
            {
                if (mainAttackCounter <= mainAttackTime)
                {
                    mainAttackCounter += Time.deltaTime;
                }
                else
                {
                    MainAttackFlurryDelay();
                }
            }


            if (!stopSpecialAttacks)
            {
                if (!bossHealth.inSecondPhase)
                {
                    if (circularPulseAttackCounter <= circularPulseAttackTime)
                    {
                        circularPulseAttackCounter += Time.deltaTime;
                    }
                    else
                    {
                        StartCoroutine(CircularPulseAttack());
                    }
                }
                else
                {
                    //Aims at the player
                    pincerSpawnSpot.transform.right -= player.transform.position - pincerSpawnSpot.transform.position;

                    if (pincerAttackCounter <= pincerAttackTime)
                    {
                        pincerAttackCounter += Time.deltaTime;
                    }
                    else
                    {
                        PincerAttack();
                    }
                }
            }

            HealthBasedEvents();

            BossAdChecker();
        }
    }

    private void MainAttackFlurryDelay()
    {
        //for each flurryAmmount we spawn a bullet to fire towards the player we an increasing delay to space the pellets out
        for (int i = 0; i < flurryAmmount; i++)
        {
            if (!bossHealth.inSecondPhase)
            {
                currentFlurryBullet = Instantiate(flurryBullet, transform.position, Quaternion.identity);
            }
            else
            {
                currentFlurryBullet = Instantiate(flurryBullet2, transform.position, Quaternion.identity);
            }

            EnemyBullet enemyBullet = currentFlurryBullet.GetComponent<EnemyBullet>();

            for (int x = 0; x < flurryCountDelayAdd; x++)
            {
                enemyBullet.delayFire += 0.5f;
            }

            flurryCountDelayAdd++;
        }

        mainAttackCounter = 0;

        flurryCountDelayAdd = 0;
    }

    //We have set all these bullets alreadys in the scene and that made them invisable, so we clone all these bullets in the list and make them visable. We have a delay for each flurry.
    private IEnumerator CircularPulseAttack()
    {
        circularPulseAttackCounter = 0;

        int newFlurryAmount = circularFlurryAmount;

        while (newFlurryAmount > 0)
        {
            foreach (GameObject bullet in circularPulseAttack)
            {
                GameObject newBullet = Instantiate(bullet, bullet.transform.position, bullet.transform.rotation);

                newBullet.SetActive(true);
            }

            yield return new WaitForSeconds(0.5f);

            newFlurryAmount--;
        }
    }

    //We spawn a bullet facing the player and add a tilt which keeps changing based on our leftrRightSwich
    private void PincerAttack()
    {   
        GameObject spawnBullet = Instantiate(pincerBullet, pincerSpawnSpot.transform.position, pincerSpawnSpot.transform.rotation);

        if (!leftRightSwitch)
        {
            spawnBullet.transform.eulerAngles += new Vector3(0, 0, pincerTilt);

            leftRightSwitch = true;
        }
        else
        {
            spawnBullet.transform.eulerAngles += new Vector3(0, 0, -pincerTilt);

            leftRightSwitch = false;
        }

        pincerAttackCounter = 0;
    }

    //When at half heath we start some coroutines and make the boss invunrable to damage, we also stop our special attack. for the second health checker we do the same but we also stop
    //our main attack and turn off our collider, we also set the trees around the boss to be inactive
    private void HealthBasedEvents()
    {
        if (healthCheckerCounter == 1)
        {
            if (bossHealth.currentHealth <= bossHealth.maxHealth / 2)
            {
                stopSpecialAttacks = true;

                bossHealth.invunrable = true;

                StartCoroutine(SpawnAds());

                StartCoroutine(FlipAlpha(true, false));

                healthCheckerCounter++;
            }
        }
        else if (healthCheckerCounter == 2)
        {
            if (bossHealth.currentHealth <= 0)
            {
                collider2D.enabled = false;

                bossHealth.invunrable = true;

                stopMainAttacks = true;

                stopSpecialAttacks = true;

                StartCoroutine(SpawnAds());

                bossTrees.SetActive(false);

                StartCoroutine(FlipAlpha(true, true));

                healthCheckerCounter++;
            }
        }
    }

    //We check all the ads added into our list and if they are destroyed we remove them from that list. If the list is now empty  we return all bools to make the boss active again.
    //If we are in the second phase we also allow the boss to move and change our sprite
    private void BossAdChecker()
    {
        if (adsSpawned && enemyAdsLeft.Count > 0)
        {
            for (int i = enemyAdsLeft.Count - 1; i >= 0; i--)
            {
                if (enemyAdsLeft[i] == null)
                {
                    enemyAdsLeft.RemoveAt(i);
                }
            }
        }
        else if (adsSpawned)
        {
            stopSpecialAttacks = false;

            bossHealth.invunrable = false;

            adsSpawned = false;

            if (!bossHealth.inSecondPhase)
            {
                StartCoroutine(FlipAlpha(false, false));
            }
            else
            {
                animationManager.ChangeAnimationState("Transition");

                bossHealth.canDie = true;

                collider2D.enabled = true;

                enemyMovement.dontMove = false;

                stopMainAttacks = false;

                bossHealth.currentHealth = bossHealth.newMaxHealth;

                spriteRenderer.sprite = secondPhaseSprite;

                StartCoroutine(FlipAlpha(false, true));
            }
        }
    }

    //We spawn lots of trees around the boss and add them all into a list, after 2 seconds the player should realise its an attack with this we activate these trees. These trees position
    //are used to spawn in the ad enemy trees, once spawned we destroy the fake trees and add our ads into a differnt list then clear the fake tree list.
    private IEnumerator SpawnAds()
    {
        List<GameObject> fakeFolkTree = new List<GameObject>();

        for (int i = 0; i < adsToSpawn; i++)
        {
            float randomAngle = Random.Range(0f, 360f);
            float angleInRadians = randomAngle * Mathf.Deg2Rad;

            Vector3 randomSpawnPosition = transform.position + new Vector3(Mathf.Cos(angleInRadians) * adsSpawnRadius, Mathf.Sin(angleInRadians) * adsSpawnRadius, 0f);

            GameObject normalTree = Instantiate(smallTree, randomSpawnPosition, Quaternion.identity);

            fakeFolkTree.Add(normalTree);
        }

        yield return new WaitForSeconds(2f);

        foreach (GameObject normalTree in fakeFolkTree)
        {
            GameObject folkTree = Instantiate(treeFolk, normalTree.transform.position, Quaternion.identity);

            enemyAdsLeft.Add(folkTree);

            Destroy(normalTree);
        }

        fakeFolkTree.Clear();

        adsSpawned = true;
    }

    //This script is purely for visual aid that hides or shows the boss and the big trees around it over time. We show or hide them by changing the aplha of the sprite renderer and stop
    //doing it based on the numbers i set, which indicates im happy with the result.
    private IEnumerator FlipAlpha(bool hide, bool mainBossOnly)
    {
        if (!mainBossOnly)
        {
            while (hide && spriteRenderer.color.a > 0.6 || !hide && spriteRenderer.color.a < 1)
            {
                Color changedColour = spriteRenderer.color;

                if (hide)
                {
                    changedColour.a -= hideAlphaChangeRate * Time.deltaTime;
                }
                else
                {
                    changedColour.a += hideAlphaChangeRate * Time.deltaTime;
                }

                spriteRenderer.color = changedColour;

                foreach (SpriteRenderer renderer in bossTreesRenderer)
                {
                    changedColour = renderer.color;

                    if (hide)
                    {
                        changedColour.a += hideAlphaChangeRate * 2.5f * Time.deltaTime;
                    }
                    else
                    {
                        changedColour.a -= hideAlphaChangeRate * 2.5f * Time.deltaTime;
                    }

                    renderer.color = changedColour;
                }

                yield return null;
            }
        }
        else
        {
            while (hide && spriteRenderer.color.a > 0 || !hide && spriteRenderer.color.a < 1)
            {
                Color changedColour = spriteRenderer.color;

                if (hide)
                {
                    changedColour.a -= hideAlphaChangeRate * Time.deltaTime;
                }
                else
                {
                    changedColour.a += hideAlphaChangeRate * 1.5f * Time.deltaTime;
                }

                spriteRenderer.color = changedColour;

                yield return null;
            }
        }
       
        yield return null;
    }

    //Simple check collison
    private void OnTriggerStay2D(Collider2D other)
    {
        if (itsShowTime)
        {
            // Check if the trigger event is with the player.
            if (other.CompareTag("Player"))
            {
                if (playerHealth == null)
                {
                    // Get the PlayerHealth script from the player GameObject.
                    playerHealth = other.GetComponent<PlayerHealth>();
                }

                // Check if the playerHealth script is not null.
                if (playerHealth != null)
                {
                    // Inflict damage on the player.
                    playerHealth.TakeDamage(contactDamageAmount);
                }
            }
        }
    }
}
