using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

//We inherit from enemyHealth
public class BossHealth : EnemyHealth
{
    public Image leftHealthBar;
    public Image rightHealthBar;

    [Header("Level Boss General")]
    [SerializeField]
    private EnemyMovement enemyMovement;

    [SerializeField]
    private TreeBoss treeBoss;

    [SerializeField]
    private VampireBoss vampireBoss;

    [SerializeField]
    private int bossLevel;

    public bool invunrable;

    [Header("Level Boss Phase Two")]
    [SerializeField]
    private bool secondPhase;

    public bool inSecondPhase;

    public bool canDie;

    public float newMaxHealth;

    [Header("Vampire")]
    [SerializeField]
    private List<GameObject> vampireClones = new List<GameObject>();

    [Header("Victory")]
    [SerializeField]
    private GameObject victoryScreen;

    [SerializeField]
    private float victoryScreenPopup;

    private void Update()
    {
        // Update the health bars' fill amounts to match the boss's current health
        float fillAmount = currentHealth / maxHealth;
        leftHealthBar.fillAmount = fillAmount;
        rightHealthBar.fillAmount = fillAmount;
    }

    //Same as original except we check if we are invunrable or not
    protected override void TakeGunDamage(float damage)
    {
        if (!invunrable && !dead && !PlayerStats.Instance.paused)
        {
            base.TakeGunDamage(damage);
        }
    }

    //Same as original except we check if we are invunrable or not
    public override void TakeStatusDamage(float damagePercent)
    {
        if (!invunrable && !dead && !PlayerStats.Instance.paused)
        {
            base.TakeStatusDamage(damagePercent);
        }
    }

    //Same as original except we check if we are invunrable or not
    protected override void TakeEnemyExplosionDamage(float damage)
    {
        if (!invunrable && !dead && !PlayerStats.Instance.paused)
        {
            base.TakeEnemyExplosionDamage(damage);
        }
    }

    //We override the general Deadscript and follow it as normal except for each boss we stop its movement and attack upon death. We also destroy any lingering bullets and invoke the victory
    //Screen. If we have a second phase and we are not in it we set our new max health and set the in second phase bool to true.
    protected override void Dead()
    {
        if (!secondPhase || secondPhase && inSecondPhase && canDie)
        {
            dead = true;

            //Tree boss
            if (bossLevel == 1)
            {
                enemyMovement.dontMove = true;

                treeBoss.itsShowTime = false;

                animationManager.ChangeAnimationState("Death");
            }
            else if (bossLevel == 2)
            {
                vampireBoss.itsShowTime = false;

                if (vampireClones != null)
                {
                    foreach (GameObject clone in vampireClones)
                    {
                        clone.SetActive(false);

                        Destroy(clone);
                    }
                }

                animationManager.ChangeAnimationState("Vampire Hat");
            }
            else
            {

            }

            EnemyBullet[] enemyBullets = FindObjectsOfType<EnemyBullet>();

            foreach (EnemyBullet bullets in enemyBullets)
            {
                Destroy(bullets.gameObject);
            }

            //We gain gold on kill if the value is more than 0
            if (goldGainOnKill > 0)
            {
                PlayerStats.Instance.gold += goldGainOnKill;
            }

            Invoke("VictoryScreen", victoryScreenPopup);

            Destroy(gameObject, despawnTime);
        }
        else
        {
            maxHealth = newMaxHealth;

            inSecondPhase = true;
        }
    }
    private void VictoryScreen()
    {
        victoryScreen.SetActive(true);
    }
}
