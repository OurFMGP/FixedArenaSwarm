using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VampireMovement : MonoBehaviour
{
    [Header("References")]
    [SerializeField]
    private AnimationManager animationManager;

    [SerializeField]
    private VampireBoss vampireBoss;

    [SerializeField]
    private GameObject player;

    [SerializeField]
    private GameObject boss;

    [Header("General Fields")]
    public int movementType;

    [Header("Teliport")]
    [SerializeField]
    private float tpAroundPlayerRadiusMin;

    [SerializeField]
    private float tpAroundPlayerRadiusMax;

    private float tpAroundPlayerRadius;

    [SerializeField]
    private float teliportTime;

    [SerializeField]
    private float teliportCounter;

    [HideInInspector]
    public bool getNewTpPosition;

    private Vector3 randomTeliportPosition;

    [SerializeField]
    private float teliportSpeed;

    public bool freeTeliport;

    [SerializeField]
    private float teliportMoveCounter;

    public int teliportOrbUseCounter;

    private bool teliportAnimationComplete;

    //We use a counter to see when to teliport which when reached if null we call GetRandomTeilportPosition, then we call mainTeliport
    private void Update()
    {
        if (vampireBoss.itsShowTime && !PlayerStats.Instance.paused)
        {
            if (movementType == 1)
            {
                if (teliportCounter < teliportTime && !freeTeliport)
                {
                    teliportCounter += Time.deltaTime;
                }
                else
                {
                    if (getNewTpPosition)
                    {
                        GetRandomTeilportPosition();

                        getNewTpPosition = false;
                    }
                    else
                    {
                        if (teliportAnimationComplete)
                        {
                            MainTeliport();
                        }
                        else
                        {
                            TeliportAnimation();
                        }
                    }
                }
            }
        }
    }

    private void TeliportAnimation()
    {
        if (!vampireBoss.stage2Animations)
        {
            animationManager.ChangeAnimationState("Vampire Teliport");
        }
        else
        {
            animationManager.ChangeAnimationState("Vampire Teleport 2");
        }
    }

    private void TeliportAnimationComplete()
    {
        teliportAnimationComplete = true;
    }

    //We get the distance between this boss and the teliport location and with using an increment counter we can lerp to that position. Once we are close enough increase our orb teliport
    //counter, reset our other teliport time counters and allow the boss to get a free main attack without needing the counters.
    private void MainTeliport()
    {
        float distance = Vector2.Distance(boss.transform.position, randomTeliportPosition);

        teliportMoveCounter += Time.deltaTime;

        boss.transform.position = Vector3.Lerp(boss.transform.position, randomTeliportPosition, (teliportMoveCounter / distance * teliportSpeed));

        if (distance < 0.01)
        {
            if (!vampireBoss.stage2Animations)
            {
                animationManager.ChangeAnimationState("Vampire Teliport Reverse");
            }
            else
            {
                animationManager.ChangeAnimationState("Vampire Teliport Reverse 2");
            }

            if (!vampireBoss.stopOrbAttacks)
            {
                teliportOrbUseCounter++;
            }

            teliportCounter = 0;

            teliportAnimationComplete = false;

            teliportMoveCounter = 0;

            vampireBoss.freeMainAttack = true;

            getNewTpPosition = true;

            freeTeliport = false;
        }
    }

    //We pick a random area around the player in a circle based on how far we set the variable tpAroundPlayerRadius min and max. 
    private void GetRandomTeilportPosition()
    {
        float randomAngle = Random.Range(0f, 360f);
        float angleInRadians = randomAngle * Mathf.Deg2Rad;

        tpAroundPlayerRadius = Random.Range(tpAroundPlayerRadiusMin, tpAroundPlayerRadiusMax);

        randomTeliportPosition = player.transform.position + new Vector3(Mathf.Cos(angleInRadians) * tpAroundPlayerRadius, Mathf.Sin(angleInRadians) * tpAroundPlayerRadius, 0f);

        vampireBoss.freeMainAttack = false;
    }
}
