using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class BossManager : MonoBehaviour
{
    [SerializeField]
    private GameObject bossHpBar; // Reference to the boss HP bar

    [Header("References")]
    [SerializeField]
    private List<GameObject> objectsToHide = new List<GameObject>();

    [SerializeField]
    private GameObject player;

    [SerializeField]
    private PlayerMovement playerMovement;

    [SerializeField]
    private GameObject playerCamera;

    [SerializeField]
    private GameObject playerCineMachine;

    [SerializeField]
    private List<Sprite> playerDifferntSprites = new List<Sprite>();

    [SerializeField]
    private GameObject newPlayerPosition;

    [SerializeField]
    private GameObject projectiles;

    [SerializeField]
    private GameObject boss;

    [SerializeField]
    private GameObject bossCamera;

    [SerializeField]
    private GameObject plantBorder;

    [Header("Fields")]
    [SerializeField]
    private int bossThisLevel;

    [SerializeField]
    private float playerActivateTime;

    [SerializeField]
    private bool movePlayer;

    [SerializeField]
    private float movePlayerSpeed;

    [SerializeField]
    private float distance;

    [SerializeField]
    private float counter;

    [Header("Tree Boss")]
    [SerializeField]
    private SpriteRenderer treeBossRenderer;

    [SerializeField]
    private List<SpriteRenderer> bossTreesRenderer = new List<SpriteRenderer>();

    [SerializeField]
    private float alphaChangeRate;

    private bool alphaChangeBoss;
    public int bossCount; // The number of bosses in the scene
    public GameObject victoryScreen; // Reference to the victory screen


    //We move the player as well as the player camera to the boss this is so we can mimic a sort of simple cutscene later. we set move input to zero
    //as when the player does spawn in again the move input still has data that cause our player to continue to move without any input.
    public void BossTime()
    {
        DeactivateObjects();

        boss.SetActive(true);

        bossCamera.SetActive(true);

        player.transform.position = boss.transform.position;

        playerCamera.transform.position = boss.transform.position;

        playerMovement.moveInput = Vector2.zero;

        plantBorder.SetActive(true);

        //Based on the character chosen we use the apropiate weapon
        if (PlayerStats.Instance.selectedSpriteIndex == 0)
        {
            newPlayerPosition.GetComponent<SpriteRenderer>().sprite = playerDifferntSprites[0];
        }
        else if (PlayerStats.Instance.selectedSpriteIndex == 1)
        {
            newPlayerPosition.GetComponent<SpriteRenderer>().sprite = playerDifferntSprites[1];
        }
        else if (PlayerStats.Instance.selectedSpriteIndex == 2)
        {
            newPlayerPosition.GetComponent<SpriteRenderer>().sprite = playerDifferntSprites[2];
        }
        else
        {
            newPlayerPosition.GetComponent<SpriteRenderer>().sprite = playerDifferntSprites[3];
        }

        //If tree boss we ivoke PlayerSet otherwise we set alphaChangeBoss to true
        if (bossThisLevel != 1)
        {
            Invoke("PlayerSet", playerActivateTime);
        }
        else
        {
            alphaChangeBoss = true;
        }
    }

    //Deactivates trees and possible skills that could hide the boss or deal damage to it, we also deactivate objects like the player so we can manipulate them 
    private void DeactivateObjects()
    {
        foreach (GameObject environment in objectsToHide)
        {
            environment.SetActive(false);
        }

        EnemyHealth[] enemies = FindObjectsOfType<EnemyHealth>();

        foreach (EnemyHealth enemy in enemies)
        {
            Destroy(enemy.gameObject);
        }

        Turret[] turrets = FindObjectsOfType<Turret>();

        foreach (Turret turret in turrets)
        {
            Destroy(turret.gameObject);
        }

        FlameDamage[] flames = FindObjectsOfType<FlameDamage>();

        foreach (FlameDamage flame in flames)
        {
            Destroy(flame.gameObject);
        }

        EnemyBullet[] enemyBullets = FindObjectsOfType<EnemyBullet>();

        foreach (EnemyBullet bullets in enemyBullets)
        {
            Destroy(bullets.gameObject);
        }
    }

    //When move is true we move the invisable player to a fake sprite player using lerp, we do this because the player camera will still follow the invisable player giving the impression
    //of a simple cutscene. We dont use an actual cutscene as its easier to do this on this scale. once we are close enough to the fake sprite we hide it and show our player and continue 
    //the game as normal
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.B))
        {
           // BossTime();
        }

        // If the boss is active, show the boss HP bar
        if (boss.activeInHierarchy)
        {
            bossHpBar.SetActive(true);
        }
        else
        {
            bossHpBar.SetActive(false);
        }

        //When active this increases the bosses alpha and reduces the trees arounf that bosses alpha, when the boss is fully visable we call PlayerSet
        if (alphaChangeBoss)
        {
            Color changedColour = treeBossRenderer.color;

            changedColour.a += alphaChangeRate * Time.deltaTime;

            treeBossRenderer.color = changedColour;

            foreach (SpriteRenderer renderer in bossTreesRenderer)
            {
                changedColour = renderer.color;

                changedColour.a -= alphaChangeRate * Time.deltaTime;

                renderer.color = changedColour;
            }

            if (treeBossRenderer.color.a >= 1)
            {
                PlayerSet();

                alphaChangeBoss = false;
            }
        }

        if (movePlayer == true)
        {
            distance = Vector2.Distance(boss.transform.position, newPlayerPosition.transform.position);

            counter += Time.deltaTime;

            player.transform.position = Vector3.Lerp(boss.transform.position, newPlayerPosition.transform.position, (counter / distance * movePlayerSpeed));


            if (Vector2.Distance(player.transform.position, newPlayerPosition.transform.position) < 0.01)
            {
                newPlayerPosition.SetActive(false);

                player.SetActive(true);

                projectiles.SetActive(true);

                if (bossThisLevel == 1)
                {
                    boss.GetComponent<TreeBoss>().itsShowTime = true;
                }
                else if (bossThisLevel == 2)
                {
                    boss.GetComponent<VampireBoss>().itsShowTime = true;
                }
                else
                {
                   
                }

                movePlayer = false;
            }
        }
    }

    //Sets active some gameobjects we want to manipulate and hides the boss camera, we also set a bool to true
    private void PlayerSet()
    {
        bossCamera.SetActive(false);

        playerCamera.SetActive(true);

        playerCineMachine.SetActive(true);

        //ADD A WAY TO CHANGE THE SPRITE ONCE CHARACTER SELECT IS DONE AS THE FAKE SPRITE WILL NEED TO MATCH THE CHARACTER SPRITE
        newPlayerPosition.SetActive(true);

        movePlayer = true;
    }
}
